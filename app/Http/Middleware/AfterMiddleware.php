<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class AfterMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        Session::forget('api_token');

        return $response;
    }
}