<?php

namespace App\Http\Middleware;

use Closure;
use App\Libraries\StripTag;
use Session;

class BeforeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        StripTag::globalXssClean($request);

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS, DELETE, PUT');
        header("Access-Control-Allow-Headers: X-TOKEN, Origin, X-Requested-With, Cache-Control, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header('Access-Control-Allow-Credentials: true');

        $token = $request->header('X-TOKEN');
        if($token) {
            Session::put('api_token', $token);
        }
        return $next($request);
    }
}
