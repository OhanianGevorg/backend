<?php
/**
 * Created by PhpStorm.
 * User: hakob
 * Date: 5/19/16
 * Time: 7:34 PM
 */
namespace App\Http\Middleware;

use App\Models\AuthTokens;
use Closure;
use Illuminate\Support\Facades\Session;

class AuthorizedMiddleware
{

    /**
     * Check income request X-Auth-Token
     *
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('X-Auth-Token');
        if(!AuthTokens::tokenIsValid($token))
        {
            return response('Unauthorized.', 401);
        }else {
            AuthTokens::getTokenRow($token)->extendValidTo();
        }
        
        return $next($request);
    }
}