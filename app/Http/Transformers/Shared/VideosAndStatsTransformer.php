<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/15/16
 * Time: 1:48 PM
 */

namespace App\Http\Transformers\Shared;


use App\Http\Transformers\StatsTransformers\VideosStatsTransformer;
use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\Models\Users\UserVideos;
class VideosAndStatsTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model,$params=null)
    {
        if(!$model){
            return [];
        }

        return [
            'stats'=>VideosStatsTransformer::transformCollection($model->videosStats()),
            'title'=>$model->title,
            'id'=>$model->getKey(),
            'plays' => $model->getCount('plays',$params),
            'shares' => $model->getCount('shares',$params),
            'saves' => $model->getCount('saves',$params)
        ];
    }
}