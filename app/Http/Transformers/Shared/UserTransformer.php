<?php
namespace App\Http\Transformers\Shared;

use App\Models\User;
use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;


class UserTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;

    /**
     * @param User $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'email' => $model->email,
            'role' => $model->role_id,
            'facebook_id' => $model->facebook_id,
            'name' => $model->name,
            'surname' => $model->address,
            'address_details' => [
                'country' => $model->country,
                'state' => $model->state,
                'city' => $model->city,
                'latitude' => $model->latitude,
                'longitude' => $model->longitude
            ],
            'store_name' => $model->site_name,
            'contact_phone' => $model->contact_phone,
            'avatar' => $model->avatar ? asset('pics/' . $model->avatar) : ($model->fb_user_id ? "https://graph.facebook.com/" . $model->fb_user_id . "/picture?type=large&w‌​idth=500&height=500" : asset('img/no-avatar.png')),
            'avatar_file' => $model->avatar,
            'saved_products_count' => $model->favorite_products()->count(),
            'is_favorite' => $model->isFavorite(),
            'is_follow' => $model->isFollow(),
            'fb_user_id' => $model->fb_user_id,
            'timeLine' => TimeLineTransformer::transform($model->timeLine)
        ];
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return User
     */
    public static function prepare(array $raw, array $injector = [])
    {
    }
}