<?php
namespace App\Http\Transformers\Shared;

use App\Models\Users\UserGenres;
use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;

class UserGenresTransformer implements TransformerInterface
{
    use TransformCollection;

    /**
     * @param UserGenres $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id' => $model->genre->getKey(),
            'name' => $model->genre->name
        ];
    }
}