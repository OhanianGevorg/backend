<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/17/17
 * Time: 4:36 PM
 */

namespace App\Http\Transformers\Shared;


use App\Interfaces\TransformerInterface;

class UserXsTransformer implements TransformerInterface
{

    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'name'=>$model->name,
            'role_id'=>$model->role_id,
            'avatar'=>$model->avatar ? asset('pics/' . $model->avatar) : ($model->fb_user_id  ? "https://graph.facebook.com/".$model->fb_user_id."/picture?type=large&w‌​idth=500&height=500" : asset('img/no-avatar.png')),
            'fb_user_id' => $model->fb_user_id,
            'site_name'=>$model->site_name
        ];
    }
}