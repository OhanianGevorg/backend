<?php

namespace App\Http\Transformers\Shared;

use App\Models\Users\UserVideos;
use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;

class VideoTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param UserVideos $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'url' => $model->url,
            'title' => $model->title,
            'caption' => $model->caption,
            'is_favorite' => $model->isFavorite(),
            'user_name' => $model->user()->first()->name
        ];
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return array
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new UserVideos(array_merge([
            'user_id' => array_get($raw, 'user_id'),
            'url' => array_get($raw, 'url'),
            'title' => array_get($raw, 'title'),
            'caption' => array_get($raw, 'caption'),
        ], $injector));
    }
}