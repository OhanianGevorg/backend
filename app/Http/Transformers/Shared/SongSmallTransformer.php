<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/15/16
 * Time: 1:10 PM
 */

namespace App\Http\Transformers\Shared;


use App\Http\Transformers\StatsTransformers\SongsStatsTransformer;
use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\Models\Users\UserSongs;

class SongSmallTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model,$params=null)
    {
        if (!$model) {
            return [];
        }
        return [
            'stats' => SongsStatsTransformer::transformCollection($model->songsStats()),
            'id' => $model->getKey(),
            'name' => $model->song_name,
            'plays' => $model->getCount('plays',$params),
            'downloads' => $model->getCount('downloads',$params),
            'shares' => $model->getCount('shares',$params),
            'saves' => $model->getCount('saves',$params),
        ];
    }
}