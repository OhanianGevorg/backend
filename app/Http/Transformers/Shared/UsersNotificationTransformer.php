<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 12/22/16
 * Time: 5:47 PM
 */

namespace App\Http\Transformers\Shared;


use App\Http\Transformers\Notifications\NotificationTransformer;
use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Users\UserNotification;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;
use DateTime;

class UsersNotificationTransformer implements PrepareInterface, TransformerInterface
{

    use PrepareCollection;
    use TransformCollection;

    public static function transform($model)
    {
        return [
            'id' => $model->getKey(),
            'user_id' => $model->user_id,
            'date' => DateTime::createFromFormat('Y-m-d H:i:s', $model->created_at)->format('Y-m-d H:i:s'),
            'event_name' => $model->event_name,
            'notification_type' => $model->notification_type,
            'is_active' => $model->is_active,
//            'notifications'=>NotificationTransformer::transformCollection($model->notifications)
        ];
    }

    public static function prepare(array $raw, array $injector = [])
    {
        return new UserNotification(array_merge([
            'user_id' => array_get($raw, 'user_id'),
            'event_name' => array_get($raw, 'event_name'),
            'is_active' => array_get($raw, 'is_active'),
            'notification_type' => array_get($raw, 'notification_type')
        ], $injector));
    }
}