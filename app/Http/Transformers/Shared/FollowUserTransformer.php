<?php

namespace App\Http\Transformers\Shared;

use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\Models\User;

class FollowUserTransformer implements TransformerInterface
{
    use  TransformCollection;

    public static function transform($model)
    {
        if(!$model){
            return [];
        }

        return [
            'id' => $model->getKey(),
            'role' => $model->role_id,
            'name' => $model->name,
            'avatar' => $model->avatar ? asset('pics/' . $model->avatar) : ($model->fb_user_id  ? "https://graph.facebook.com/".$model->fb_user_id."/picture?type=large&w‌​idth=500&height=500" : asset('img/no-avatar.png')),
            'avatar_file' => $model->avatar,
            'is_favorite' => $model->isFavorite(),
            'is_follow' => $model->isFollow(),
            'fb_user_id' => $model->fb_user_id,
            'gigs_count'=>$model->gigsCount(),
            'site_name'=>$model->site_name
        ];
    }
}