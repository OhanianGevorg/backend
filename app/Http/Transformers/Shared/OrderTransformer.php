<?php
namespace App\Http\Transformers\Shared;

use App\Models\Order\Order;
use App\Models\Users\UserPictures;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;


class OrderTransformer implements  PrepareInterface
{
    use PrepareCollection;


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return UserPictures
     */
    public static function prepare(array $raw, array $injector = [])
    {

        return new Order(array_merge([
            'item_id' => array_get($raw, 'id'),
            'caption' => array_get($raw, 'caption'),
            'photo_credit' => array_get($raw, 'photo_credit')
        ], $injector));
    }
}