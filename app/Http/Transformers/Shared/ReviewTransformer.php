<?php
namespace App\Http\Transformers\Shared;

use App\Models\Users\UserReviews;

use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;

class ReviewTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param UserReviews $model
     *
     * @return array
     */
    public static function transform($model)
    {
        return [
            'id' => $model->getKey(),
            'review' => $model->review,
            'reviewed_by' => $model->reviewed_by,
            'review_date' => $model->review_date
        ];
    }

    /**
     * @param array $raw
     * @param array $injector
     *
     * @return UserReviews
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new UserReviews(array_merge([
            'user_id' => array_get($raw, 'user_id'),
            'review' => array_get($raw, 'review'),
            'reviewed_by' => array_get($raw, 'reviewed_by'),
            'review_date' => array_get($raw, 'review_date'),
        ], $injector));
    }
}