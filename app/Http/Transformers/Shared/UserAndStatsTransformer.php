<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/15/16
 * Time: 2:08 PM
 */

namespace App\Http\Transformers\Shared;


use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\Models\User;

class UserAndStatsTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model, $params = null)
    {

        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'stats' => $model->usersStats(),
            'impressions' => $model->getCount('impressions', $params),
            'views' => $model->getCount('views', $params),
            'shares' => $model->getCount('shares', $params),
            'saves' => $model->getCount('saves', $params)
        ];
    }
}