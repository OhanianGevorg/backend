<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 3/19/2017
 * Time: 11:30 PM
 */

namespace App\Http\Transformers\Shared;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Users\Post;
use App\Traits\TransformCollection;

class PostTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        return [
            'post_id'=>$model->getKey(),
            'user_id'=>$model->user_id,
            'time_line_id'=>$model->time_line_id,
            'content'=>$model->post_content,
            'post_type'=>$model->post_type

        ];
    }
    public static function prepare(array $raw, array $injector = [])
    {
        return new Post([
            'user_id'=>$raw['user_id'],
            'time_line_id'=>$raw['time_line_id'],
            'content'=>$raw['content'],
            'post_type'=>$raw['post_type']
        ]);
    }
}