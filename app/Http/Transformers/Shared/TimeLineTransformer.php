<?php
/**
 * Created by PhpStorm.
 * User: Gevorg
 * Date: 3/19/2017
 * Time: 11:08 PM
 */

namespace App\Http\Transformers\Shared;


use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\Users\TimeLine;

class TimeLineTransformer implements PrepareInterface, TransformerInterface
{
    public static function transform($model)
    {

        dd(PostTransformer::transformCollection($model->posts));
        return [
            'id' => $model->getKey(),
            'user_id' => $model->user_id,
            'posts' => PostTransformer::transformCollection($model->posts) + PostTransformer::transformCollection($model->getSharedPosts()),
//            'shared'=>ShareTransformer::transform($model->sharedPosts)
        ];
    }

    public static function prepare(array $raw, array $injector = [])
    {
        return new TimeLine([
            'user_id' => $raw['user_id'],
            'user_type' => $raw['user_type'],
            'user_name' => $raw['user_name']
        ]);
    }
}