<?php
namespace App\Http\Transformers\Shared;

use App\Models\Users\UserPictures;
use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;


class PictureTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param mixed $model
     *
     * @return array
     */
    public static function transform($model)
    {
        return [
            'id' => $model->getKey(),
            'file_id' => $model->file_id,
            'caption' => $model->caption,
            'photo_credit' => $model->photo_credit,
            'type' => $model->type,
            'url' => $model->file_id ? asset('pics/' . $model->file_id) : ""
        ];
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return UserPictures
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new UserPictures(array_merge([
            'file_id' => array_get($raw, 'file_id'),
            'caption' => array_get($raw, 'caption'),
            'photo_credit' => array_get($raw, 'photo_credit')
        ], $injector));
    }
}