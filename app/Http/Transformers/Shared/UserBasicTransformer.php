<?php
namespace App\Http\Transformers\Shared;

use App\Models\User;
use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;


class UserBasicTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;

    /**
     * @param User $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'role' => $model->role_id,
            'name' => $model->name,
            'address' => $model->address,
            'web_site' => $model->web_site,
            'address_details' => [
                'country' => $model->country,
                'state' => $model->state,
                'city' => $model->city,
                'latitude' => $model->latitude,
                'longitude' => $model->longitude
            ],
            'avatar' => $model->avatar ? asset('pics/' . $model->avatar) : ($model->fb_user_id ? "https://graph.facebook.com/" . $model->fb_user_id . "/picture?type=large&w‌​idth=500&height=500" : ''),
            'avatar_file' => $model->avatar,
            'is_favorite' => $model->isFavorite(),
            'is_follow' => $model->isFollow(),
            'fb_user_id' => $model->fb_user_id,
            'site_name' => $model->site_name
        ];
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return User
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new User([
            'role_id' => $raw['role_id'],
            'password' => bcrypt($raw['password']),
            'email' => (string)$raw['email'],
            'store_name' => isset($raw['store_name']) && $raw['role_id'] == User::STORE ? (string)$raw['store_name'] : null,
            'name' => isset($raw['name']) && $raw['role_id'] == User::USERS ? (string)$raw['name'] : null,
            'surname' => isset($raw['surname']) && $raw['role_id'] == User::USERS ? (string)$raw['surname'] : null,
        ]);
    }
}