<?php

namespace App\Http\Transformers\Shared;


use App\Interfaces\TransformerInterface;

class PosterTransformer implements TransformerInterface
{
    public static function transform($modelField)
    {
        if(!$modelField){
            return [];
        }
        return [
            'file_id'=>$modelField,
            'url'=>asset('posters/' . $modelField)
        ];
    }
}