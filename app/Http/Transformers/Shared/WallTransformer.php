<?php
namespace App\Http\Transformers\Shared;

use App\Models\Users\UserWall;

use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;
use DateTime;


class WallTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param UserWall $model
     *
     * @return array
     */
    public static function transform($model)
    {
        return [
            'id' => $model->getKey(),
            'user' => UserTransformer::transform($model->user),
            'sender' => UserTransformer::transform($model->sender),
            'message' => $model->message,
            'posted_date' => DateTime::createFromFormat('Y-m-d H:i:s', $model->created_at)->format('Y-m-d H:i:s')
        ];
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return UserWall
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new UserWall(array_merge([
            'user_id' => array_get($raw, 'user_id'),
            'sender_id' => array_get($raw, 'sender_user_id'),
            'message' => array_get($raw, 'message')
        ], $injector));
    }
}