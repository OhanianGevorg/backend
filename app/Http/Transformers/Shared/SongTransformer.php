<?php
namespace App\Http\Transformers\Shared;

use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;
use App\Models\Users\UserSongs;

class SongTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param UserSongs $model
     *
     * @return array
     */
    public static function transform($model)
    {


        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'title' => $model->song_name,
            'mp3' => $model->file_id ? asset('/audios/' . $model->file_id) : null,
            'artist' => UserTransformer::transform($model->artist),
            'file_id' => $model->file_id,
            'file_name' => $model->file_name,
            'song_name' => $model->song_name,
            'is_favorite' => $model->isFavorite(),
            'permission' => $model->permission
        ];
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return UserSongs
     */
    public static function prepare(array $raw, array $injector = [])
    {
//        dd(key_exists('permission',$raw)&&$raw['permission']===true);
        return new UserSongs(array_merge([
            'song_name' => array_get($raw, 'song_name'),
            'user_id' => array_get($raw, 'user_id'),
            'file_name' => array_get($raw, 'file_name'),
            'file_id' => array_get($raw, 'file_id'),
            'description' => array_get($raw, 'description'),
            'permission' => (key_exists('permission',$raw) && $raw['permission']==='allow')?'allow':'deny'
        ], $injector));
    }
}