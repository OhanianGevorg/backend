<?php
namespace App\Http\Transformers\Shared;

use App\Models\User;
use App\Models\Users\UserSearchSaves;
use App\Models\Users\UserWall;

use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;
use DateTime;
use Illuminate\Support\Facades\Auth;


class SearchSavesTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param UserSearchSaves $model
     *
     * @return array
     */
    public static function transform($model)
    {
//        dd($model->genres);
        return [
            'id' => $model->getKey(),
            'user_id' => $model->user_id,
            'name' => $model->name,
            'date' => $model->date,
            'type' => $model->type,
            'firstInitial' => $model->first_initial,
            'types' => unserialize($model->types),
            'location[city]' => $model->location_city,
            'location[radius]' => $model->location_radius,
            'location[longitude]' => $model->location_longitude,
            'location[latitude]' => $model->location_latitude,
            'minPrice' => $model->min_price ? $model->min_price : 0,
            'maxPrice' => $model->max_price ? $model->max_price : "",
            'genres' => unserialize($model->genres),
        ];
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return UserSearchSaves
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new UserSearchSaves([
            'user_id'=>User::auth()->getKey(),
            'name'=>array_get($raw,'name'),
            'date'=>array_get($raw,'date'),
            'type'=>array_get($raw,'type'),
            'types'=>serialize(array_get($raw,'types')),
            'location_city'=>(array_key_exists('location[city]',$raw))?array_get($raw,'location[city]'):'',
            'location_radius'=>(array_key_exists('location[radius]',$raw))?array_get($raw,'location[radius]'):'',
            'location_latitude'=>(array_key_exists('location[latitude]',$raw))?array_get($raw,'location[latitude]'):'',
            'location_longitude'=>(array_key_exists('location[longitude]',$raw))?array_get($raw,'location[longitude]'):'',
            'min_price'=>(array_key_exists('minPrice',$raw))?array_get($raw,'minPrice'):'',
            'max_price'=>(array_key_exists('maxPrice',$raw))?array_get($raw,'maxPrice'):'',
            'genres'=>serialize((!is_array(array_get($raw,'genres')))?explode(',',array_get($raw,'genres')):array_get($raw,'genres')),
        ]);
//        return [new UserWall(array_merge([
//            'user_id' => array_get($raw, 'user_id'),
//            'sender_id' => array_get($raw, 'sender_user_id'),
//            'message' => array_get($raw, 'message')
//        ], $injector))];
    }
}