<?php
namespace App\Http\Transformers\Shared;

use App\Models\Genre\Genre;
use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;

class GenreTransformer implements TransformerInterface
{
    use TransformCollection;

    /**
     * @param Genre $model
     *
     * @return array
     */
    public static function transform($model)
    {
        return [
            'id' => $model->getKey(),
            'name' => $model->name
        ];
    }
}