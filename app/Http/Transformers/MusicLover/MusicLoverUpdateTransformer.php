<?php
namespace App\Http\Transformers\MusicLover;

use App\Models\User;
use App\Interfaces\PrepareInterface;


class MusicLoverUpdateTransformer implements PrepareInterface
{
    /**
     * @param array $raw
     * @param array $injector
     *
     * @return User
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return [
            'name' => array_get($raw, 'name'),
            'avatar' => array_get($raw, 'avatar_file'),
            'about' => array_get($raw, 'about'),
            'contact_phone' => array_get($raw, 'contact_phone'),
            'contact_mobile' => array_get($raw, 'contact_mobile'),
            'contact_email' => array_get($raw, 'contact_email'),
            'city' => array_get($raw, 'address_details.city'),
            'state' => array_get($raw, 'address_details.state'),
            'country' => array_get($raw, 'address_details.country'),
            'address' => array_get($raw, 'address'),
            'facebook_id' => array_get($raw, 'facebook_id'),
        ];
    }
}

