<?php
namespace App\Http\Transformers\MusicLover;

use App\Http\Transformers\Artist\ArtistDirectoryTransformer;
use App\Http\Transformers\Shared\PictureTransformer;
use App\Http\Transformers\Shared\SongTransformer;
use App\Http\Transformers\Shared\UserBasicTransformer;
use App\Http\Transformers\Shared\UserGenresTransformer;
use App\Http\Transformers\Shared\VideoTransformer;
use App\Http\Transformers\Venue\VenueBasicTransformer;
use App\Models\User;

use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;

use App\Http\Transformers\Shared\UserTransformer;

class MusicLoverTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;

    /**
     * @param User $model
     *
     * @return array
     */
    public static function transform($model)
    {

        return array_merge(
            UserTransformer::transform($model), [
               'genres' => UserGenresTransformer::transformCollection($model->genres),
               'profile_pictures' => PictureTransformer::transformCollection($model->profile_pictures()->get()),
               'gig_pictures' => PictureTransformer::transformCollection($model->gig_pictures()->get()),
               'favorite_artists' => ArtistDirectoryTransformer::transformCollection($model->favorite_artists()),
               'favorite_venues' => VenueBasicTransformer::transformCollection($model->favorite_venues()),
               'favorite_songs' => SongTransformer::transformCollection($model->favorite_songs()),
               'favorite_videos' => VideoTransformer::transformCollection($model->favorite_videos()),
//               'followers' => UserBasicTransformer::transformCollection($model->followers()),
//               'followings' => UserBasicTransformer::transformCollection($model->followings()),
               'fb_user_id' => $model->fb_user_id
            ]
        );
    }
 

    /**
     * @param array $raw
     * @param array $injector
     *
     * @return User
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new User(array_merge([
            'role_id' => User::MUSIC_LOVER,
            'email' => array_get($raw, 'email'),
            'password' => bcrypt($raw['password']),
            'name' => array_get($raw, 'name'),
            'avatar' => array_get($raw, 'avatar_file'),
            'about' => array_get($raw, 'about'),
            'fb_user_id' => array_get($raw, 'fb_user_id'),
        ], $injector));
    }
}

