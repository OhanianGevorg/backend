<?php
namespace App\Http\Transformers\Venue;

use App\Http\Transformers\Gig\GigBasicTransformer;
use App\Http\Transformers\Shared\UserBasicTransformer;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;

use App\Http\Transformers\Shared\UserTransformer;
use App\Http\Transformers\Shared\UserGenresTransformer;

class VenueBasicTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;

    /**
     * @param User $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return array_merge(
            UserBasicTransformer::transform($model), [
                'genres' => UserGenresTransformer::transformCollection($model->genres),
                'gigs' => GigBasicTransformer::transformCollection($model->upcomingGigs())
            ]
        );
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return User
     */
    public static function prepare(array $raw, array $injector = [])
    {

    }
}