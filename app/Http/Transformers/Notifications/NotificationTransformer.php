<?php


namespace App\Http\Transformers\Notifications;


use App\Http\Transformers\Shared\UserBasicTransformer;
use App\Interfaces\TransformerInterface;
use App\Models\User;
use App\Traits\TransformCollection;
use DateTime;

class NotificationTransformer implements TransformerInterface
{

    use TransformCollection;

    public static function transform($model)
    {
        return [
            'id' => $model->getKey(),
            'notifier_id' => $model->notifier_user_id,
            'notified_id' => $model->notified_user_id,
            'action_id' => $model->action_id,
            'event_type' => $model->event_type,
            'is_active' => $model->is_active,
            'is_online' => $model->is_online,
            'user' => UserBasicTransformer::transform(User::where('id', '=', $model->notifier_user_id)->first()),
            'users_notification_id' => $model->users_notification_id,
            'date' => DateTime::createFromFormat('Y-m-d H:i:s', $model->created_at)->format('Y-m-d H:i:s')
        ];
    }

}