<?php

namespace App\Http\Transformers\Notifications;

use App\Http\Transformers\Shared\FollowUserTransformer;
use App\Http\Transformers\Shared\UserBasicTransformer;
use App\Http\Transformers\Shared\UsersNotificationTransformer;
use App\Http\Transformers\Shared\UserXsTransformer;
use App\Interfaces\PrepareInterface;
use App\Interfaces\TransformerInterface;
use App\Models\User;
use App\Models\Users\Notification;
use App\Traits\TransformCollection;

class GigViewNotificationTransformer implements PrepareInterface, TransformerInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        return [
            'id'=>$model->getKey(),
            'notified_id'=>$model->notified_user_id,
            'action_id'=>$model->action_id,
            'user' => UserXsTransformer::transform(User::where('id', '=', $model->notifier_user_id)->first()),
            'event' => UsersNotificationTransformer::transform($model->user_notification)
        ];
    }

    public static function prepare(array $raw, array $injector = [])
    {
        return new Notification(array_merge([
            'user_notification_id' => array_get($raw, 'user_notification_id'),
            'notifier_user_id' => array_get($raw, 'notifier_user_id'),
            'notified_user_id' => array_get($raw, 'notified_user_id'),
            'event_type' => array_get($raw, 'event_type'),
        ]), $injector);
    }
}