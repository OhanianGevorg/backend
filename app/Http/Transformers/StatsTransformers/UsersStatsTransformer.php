<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/15/16
 * Time: 11:57 AM
 */

namespace App\Http\Transformers\StatsTransformers;


use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;

class UsersStatsTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        if(!$model){
            return [];
        }

        return [
            'id'=>$model->getKey(),
            'ip'=>$model->ip,
            'owner_id'=>$model->owner_id,
            'type'=>$model->type
        ];
    }
}