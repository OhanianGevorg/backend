<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/15/16
 * Time: 11:56 AM
 */

namespace App\Http\Transformers\StatsTransformers;


use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;

class SongsStatsTransformer implements TransformerInterface
{
    use TransformCollection;

    public static function transform($model)
    {
        if(!$model){
            return [];
        }

        return [
            'id'=>$model->getKey(),
            'type'=>$model->type,
            'ip'=>$model->ip,
            'user_id'=>$model->user_id,
            'owner_id'=>$model->owner_id
        ];
    }
}