<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/14/16
 * Time: 7:09 PM
 */

namespace App\Http\Transformers\StatsTransformers;

use App\Models\User;
use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;

class StatTransformer implements TransformerInterface
{
    use TransformCollection;

    /**
     * @param User $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'email' => $model->email,
            'role' => $model->role_id,
            'facebook_id' => $model->facebook_id,
            'name' => $model->name,
            'address' => $model->address,
            'address_details' => [
                'country' => $model->country,
                'state' => $model->state,
                'city' => $model->city,
                'latitude' => $model->latitude,
                'longitude' => $model->longitude
            ],
            'site_name' => $model->site_name,
            'contact_phone' => $model->contact_phone,
            'contact_mobile' => $model->contact_mobile,
            'contact_email' => $model->contact_email,
            'web_site' => $model->web_site,
            'other_web_site' => $model->other_web_site,
            'about' => $model->about,
            'avatar' => $model->avatar ? asset('pics/' . $model->avatar) : ($model->fb_user_id  ? "https://graph.facebook.com/".$model->fb_user_id."/picture?type=large&w‌​idth=500&height=500" : asset('img/no-avatar.png')),
            'avatar_file' => $model->avatar,
//            'saved_gigs_count' => $model->favorite_gigs()->count(),
            'is_favorite' => $model->isFavorite(),
            'is_follow' => $model->isFollow(),
            'fb_user_id' => $model->fb_user_id
        ];
    }
}