<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/14/16
 * Time: 7:08 PM
 */

namespace App\Http\Transformers\Gig;

use App\Http\Transformers\StatsTransformers\GigsStatsTransformer;
use App\Models\Gig\Gig;
use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;

class GigSmallTransformer implements TransformerInterface
{

    use TransformCollection;

    /**
     * @param Gig $model
     *
     * @return array
     */
    public static function transform($model, $paramas = null)
    {
        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'stats' => GigsStatsTransformer::transformCollection($model->gigStats()),
            'name' => $model->name,
            'start' => $model->start,
            'venue' => $model->venue,
            'artist' => $model->artists()->first(),
            'impressions' => $model->getCount('impressions', $paramas),
            'views' => $model->getCount('views', $paramas),
            'shares' => $model->getCount('shares', $paramas),
            'saves' => $model->getCount('saves', $paramas)
        ];
    }
}