<?php
namespace App\Http\Transformers\Gig;

use App\Models\Gig\GigType;
use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;

class GigTypeTransformer implements TransformerInterface
{
    use TransformCollection;

    /**
     * @param GigType $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'name' => $model->name
        ];
    }
}