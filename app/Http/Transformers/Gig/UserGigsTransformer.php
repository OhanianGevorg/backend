<?php
namespace App\Http\Transformers\Gig;

use App\Http\Transformers\Shared\UserTransformer;
use App\Models\Users\UserWall;
use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;
use DateTime;


class UserGigsTransformer implements TransformerInterface
{
    use TransformCollection;

    /**
     * @param UserWall $model
     *
     * @return array
     */
    public static function transform($model)
    {

        return [
            'gigs' => GigTransformer::transformCollection($model->favorite_gigs()),
        ];
    }


}