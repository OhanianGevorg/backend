<?php
namespace App\Http\Transformers\Gig;

use App\Http\Transformers\Shared\UserTransformer;
use App\Models\Users\UserWall;
use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;
use DateTime;


class GigWallTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param UserWall $model
     *
     * @return array
     */
    public static function transform($model)
    {
        return [
            'id' => $model->getKey(),
            'gig_id' => $model->gig->getKey(),
            'sender' => UserTransformer::transform($model->sender),
            'message' => $model->message,
            'posted_date' =>  DateTime::createFromFormat('Y-m-d H:i:s', $model->created_at)->format('j F h:i A')

        ];
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return array
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new UserWall(array_merge([
            'gig_id' => array_get($raw, 'gig_id'),
            'sender_id' => array_get($raw, 'sender_id'),
            'message' => array_get($raw, 'message')
        ], $injector));
    }
}