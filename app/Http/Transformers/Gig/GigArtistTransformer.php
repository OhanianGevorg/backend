<?php
namespace App\Http\Transformers\Gig;


use App\Models\Gig\GigArtist;

use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\PrepareCollection;
use App\Traits\TransformCollection;

class GigArtistTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param GigArtist $model
     * @return array
     */
    public static function transform($model)
    {
        return [
            'id' => $model->getKey(),
            'name' => $model->name,
            'stage_time' => $model->stage_time,
        ];
    }

    
    /**
     * @param array $raw
     * @param array $injector
     *
     * @return GigArtist
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new GigArtist(array_merge([
            'artist_user_id' => array_get($raw, 'id'),
            'gig_id' => array_get($raw, 'gig_id'),
            'stage_time' => array_get($raw, 'stage_time'),
        ], $injector));
    }
}