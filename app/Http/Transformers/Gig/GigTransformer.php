<?php
namespace App\Http\Transformers\Gig;

use App\Http\Transformers\Shared\PosterTransformer;
use App\Http\Transformers\Shared\UserTransformer;
use App\Models\Gig\Gig;
use App\Models\Users\UserPictures;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;
use App\Http\Transformers\Artist\ArtistBasicTransformer;
use App\Http\Transformers\Venue\VenueBasicTransformer;

class GigTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param Gig $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

//        dd($model);
        return [
            'id' => $model->getKey(),
            'name' => $model->name,
            'description' => $model->description,
            'price' => (int)$model->price,
            'date' => $model->start,
            'poster' => $model->poster ? asset('posters/' . $model->poster) : '',
            'smallPoster'=>$model->poster ? asset('posters/' . UserPictures::$small_poster['name'].$model->poster):'',
            'middlePoster'=>$model->poster ? asset('posters/' . UserPictures::$middle_poster['name'].$model->poster):'',
            'posterFull' => PosterTransformer::transform($model->poster),
            'venue' => VenueBasicTransformer::transform($model->venue),
            'artists' => ArtistBasicTransformer::transformCollection($model->artists()),
            'types' => GigTypeTransformer::transformCollection($model->types()),
            'is_favorite' => $model->isFavorite(),
            'seller_site' => $model->ticket_seller_site,
            'wall' => GigWallTransformer::transformCollection($model->wall),
            'saved_by' => UserTransformer::transformCollection($model->saved_by()),
            'ages' => $model->ages,
            'start' => $model->start,
            'end' => $model->end,
            'user_id' => (int)$model->user_id,
            'venue_user_id' => (int)$model->venue_user_id
        ];
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return Gig
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new Gig(array_merge([
            'venue_user_id' => array_get($raw, 'venue_user_id'),
            'user_id' => array_get($raw, 'venue_id'),
            'start' => array_get($raw, 'start_time'),
            'end' => array_get($raw, 'end_time'),
            'poster' => array_get($raw, 'poster.file_id'),
            'price' => array_get($raw, 'price'),
            'description' => array_get($raw, 'description'),
            'name' => array_get($raw, 'name'),
            'ages' => array_get($raw, 'ages'),
            'ticket_seller_site' => array_get($raw, 'ticket_seller_site'),
        ], $injector));
    }
}