<?php
/**
 * Created by PhpStorm.
 * User: Grigor
 * Date: 27.07.2016
 * Time: 15:45
 */

namespace App\Http\Transformers\Gig;


use App\Interfaces\PrepareInterface;

class GigUpdateTransformer implements PrepareInterface
{
    /**
     * @param array $raw
     * @param array $injector
     *
     * @return array
     */

    public static function prepare(array $raw, array $injector = [])
    {
        return [
            'venue_user_id' => $raw['venue_user_id'],
            'poster' => $raw['posterFull']['file_id'],
            'price' => $raw['price'],
            'name' => $raw['name'],
            'description' => $raw['description'],
            'ticket_seller_site' => $raw['seller_site'],
            'ages' => $raw['ages'],
            'start' => $raw['start'],
            'end' => $raw['end'],
        ];
    }
}