<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/17/17
 * Time: 4:28 PM
 */

namespace App\Http\Transformers\Gig;


use App\Http\Transformers\Shared\UserXsTransformer;
use App\Interfaces\TransformerInterface;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class FavoritGigNotificationTransformer implements TransformerInterface
{
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'gig' =>GigXsTransformer::transform($model->gig),
            'user'=>UserXsTransformer::transform($model->user)
        ];
    }
}