<?php
namespace App\Http\Transformers\Gig;

use App\Http\Transformers\Artist\ArtistSongTransformer;
use App\Http\Transformers\Shared\PosterTransformer;
use App\Http\Transformers\Shared\UserBasicTransformer;
use App\Http\Transformers\Shared\UserTransformer;
use App\Models\Gig\Gig;
use App\Models\Users\UserPictures;
use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;
use App\Http\Transformers\Artist\ArtistBasicTransformer;
use App\Http\Transformers\Venue\VenueBasicTransformer;

class GigDirectoryTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param Gig $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

//        dd($model);
        return [
            'id' => $model->getKey(),
            'name' => $model->name,
            'date' => $model->start,
            'smallPoster'=>$model->poster ? asset('posters/' . UserPictures::$small_poster['name'].$model->poster):'',
            'middlePoster'=>$model->poster ? asset('posters/' . UserPictures::$middle_poster['name'].$model->poster):'',
            'venue' => UserBasicTransformer::transform($model->venue),
            'artists' => ArtistSongTransformer::transformCollection($model->artists()),
            'is_favorite' => $model->isFavorite(),
            'saved_by' => UserBasicTransformer::transformCollection($model->saved_by()),
            'start' => $model->start,
            'user_id' => (int)$model->user_id,
            'venue_user_id' => (int)$model->venue_user_id
        ];
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return Gig
     */
    public static function prepare(array $raw, array $injector = [])
    {

    }
}