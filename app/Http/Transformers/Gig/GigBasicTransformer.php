<?php
namespace App\Http\Transformers\Gig;

use App\Http\Transformers\Artist\ArtistBasicTransformer;
use App\Http\Transformers\Venue\VenueBasicTransformer;
use App\Models\Gig\Gig;
use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\Traits\PrepareCollection;

class GigBasicTransformer implements TransformerInterface
{
    use TransformCollection;
    use PrepareCollection;

    /**
     * @param Gig $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return [
            'id' => $model->getKey(),
            'name' => $model->name,
            'start' => $model->start,
            'venue' => $model->venue,
            'artists' => $model->artists(),
        ];
    }

}