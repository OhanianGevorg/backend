<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/17/17
 * Time: 4:32 PM
 */

namespace App\Http\Transformers\Gig;


use App\Interfaces\TransformerInterface;

class GigXsTransformer implements TransformerInterface
{
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return [
            'id' => $model->getKey(),
            'name' => $model->name,
            'start' => $model->start
        ];
    }

}