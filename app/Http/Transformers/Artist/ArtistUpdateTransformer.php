<?php
namespace App\Http\Transformers\Artist;

use App\Interfaces\PrepareInterface;

class ArtistUpdateTransformer implements PrepareInterface
{
    /**
     * @param array $raw
     * @param array $injector
     *
     * @return array
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return [
            'avatar' => array_get($raw, 'avatar_file'),
            'name' => array_get($raw, 'name'),
            'about' => array_get($raw, 'about'),
            'city' => array_get($raw, 'address_details.city'),
            'state' => array_get($raw, 'address_details.state'),
            'country' => array_get($raw, 'address_details.country'),
            'latitude' => array_get($raw, 'address_details.latitude'),
            'longitude' => array_get($raw, 'address_details.longitude'),
            'address' => array_get($raw, 'address'),
            'web_site' => array_get($raw, 'web_site'),
            'facebook_id' => array_get($raw, 'facebook_id'),
            'contact_email' => array_get($raw, 'contact_email'),
            'contact_phone' => array_get($raw, 'contact_phone'),
            'contact_mobile' => array_get($raw, 'contact_mobile'),
            'hear_about' => array_get($raw, 'hear_about'),
        ];
    }

}

