<?php
namespace App\Http\Transformers\Artist;

use App\Http\Transformers\Gig\GigDirectoryTransformer;
use App\Http\Transformers\Shared\PictureTransformer;
use App\Http\Transformers\Shared\UserBasicTransformer;
use App\Models\User;
use App\Interfaces\TransformerInterface;
use App\Interfaces\PrepareInterface;
use App\Traits\TransformCollection;
use App\Http\Transformers\Shared\UserTransformer;
use App\Http\Transformers\Shared\UserGenresTransformer;
use App\Http\Transformers\Shared\ReviewTransformer;
use App\Http\Transformers\Shared\SongTransformer;
use App\Http\Transformers\Shared\VideoTransformer;
use App\Http\Transformers\Shared\WallTransformer;
use App\Http\Transformers\Gig\GigTransformer;

class ArtistTransformer implements TransformerInterface, PrepareInterface
{
    use TransformCollection;

    /**
     * @param User $model
     *
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }
        return array_merge(
            UserTransformer::transform($model),
            [
                'genres' => UserGenresTransformer::transformCollection($model->genres),
                'reviews' => ReviewTransformer::transformCollection($model->reviews),
                'songs' => SongTransformer::transformCollection($model->songs()->get()),
                'videos' => VideoTransformer::transformCollection($model->videos),
                'studio_pictures' => PictureTransformer::transformCollection($model->profile_pictures()->get()),
                'gig_pictures' => PictureTransformer::transformCollection($model->gig_pictures()->get()),
                'wall' => WallTransformer::transformCollection($model->wall),
                'first_gig' => GigDirectoryTransformer::transform($model->gigs()->first()),

//                'followers' => UserBasicTransformer::transformCollection($model->followers()),
//                'followings' => UserBasicTransformer::transformCollection($model->followings()),
//                'favorite_gigs' => GigTransformer::transformCollection($model->favorite_gigs())

            ]
        );
    }


    /**
     * @param array $raw
     * @param array $injector
     *
     * @return User
     */
    public static function prepare(array $raw, array $injector = [])
    {
        return new User([
            'role_id' => User::ARTIST,
            'email' => array_get($raw, 'email'),
            'site_name' => array_get($raw, 'site_name'),
            'password' => bcrypt(array_get($raw, 'password')),
            'avatar' => array_get($raw, 'avatar_file'),
            'name' => array_get($raw, 'name'),
            'about' => array_get($raw, 'about'),
            'city' => array_get($raw, 'address_details.city'),
            'state' => array_get($raw, 'address_details.state'),
            'country' => array_get($raw, 'address_details.country'),
            'latitude' => array_get($raw, 'address_details.latitude'),
            'longitude' => array_get($raw, 'address_details.longitude'),
            'address' => array_get($raw, 'address'),
            'web_site' => array_get($raw, 'web_site'),
            'facebook_id' => array_get($raw, 'facebook_id'),
            'contact_email' => array_get($raw, 'contact_email'),
            'contact_phone' => array_get($raw, 'contact_phone'),
            'contact_mobile' => array_get($raw, 'contact_mobile'),
            'hear_about' => array_get($raw, 'hear_about'),
        ]);
    }
}

