<?php
namespace App\Http\Transformers\Artist;

use App\Http\Transformers\Gig\GigBasicTransformer;
use App\Http\Transformers\Shared\UserBasicTransformer;
use App\Http\Transformers\Shared\UserGenresTransformer;
use App\Models\User;
use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\Http\Transformers\Shared\UserTransformer;
use App\Http\Transformers\Shared\SongTransformer;
use App\Http\Transformers\Shared\PictureTransformer;
use App\Http\Transformers\Shared\VideoTransformer;

class ArtistSongTransformer implements TransformerInterface
{
    use TransformCollection;

    /**
     * @param User $model
     * @return array
     */
    public static function transform($model)
    {
        if (!$model) {
            return [];
        }

        return array_merge(
            UserBasicTransformer::transform($model), [
                'songs' => SongTransformer::transformCollection($model->songs()->get()),
            ]
        );
    }
}