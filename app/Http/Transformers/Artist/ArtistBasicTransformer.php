<?php
namespace App\Http\Transformers\Artist;

use App\Http\Transformers\Gig\GigBasicTransformer;
use App\Http\Transformers\Shared\UserGenresTransformer;
use App\Models\User;
use App\Interfaces\TransformerInterface;
use App\Traits\TransformCollection;
use App\Http\Transformers\Shared\UserTransformer;
use App\Http\Transformers\Shared\SongTransformer;
use App\Http\Transformers\Shared\PictureTransformer;
use App\Http\Transformers\Shared\VideoTransformer;

class ArtistBasicTransformer implements TransformerInterface
{
    use TransformCollection;

    /**
     * @param User $model
     * @return array
     */
    public static function transform($model)
    {

        if (!$model) {
            return [];
        }

        return array_merge(
            UserTransformer::transform($model), [
                'songs' => SongTransformer::transformCollection($model->songs()->get()),
                'studio_pictures' => PictureTransformer::transformCollection($model->profile_pictures()->get()),
                'gig_pictures' => PictureTransformer::transformCollection($model->gig_pictures()->get()),
                'videos' => VideoTransformer::transformCollection($model->videos),
                'genres' => UserGenresTransformer::transformCollection($model->genres),
                'stage_time' => $model->stage_time ?  $model->stage_time : '',
                'gigs' => GigBasicTransformer::transformCollection($model->gigs()),
            ]
        );
    }
}