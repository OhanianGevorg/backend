<?php
namespace App\Http;

use App\Libraries\Enum;
use Illuminate\Validation\Validator;
use Mockery\CountValidator\Exception;

class TGValidator extends Validator
{
    /**
     * @param string $attribute
     * @param string $value
     * @param array $parameters
     *
     * @return mixed
     */
    protected function validateEnum($attribute, $value, $parameters)
    {
        $className = $parameters[0];

        if (!class_exists($className) || !is_subclass_of($className, Enum::class)) {
            throw new Exception("Enum class [{$className}] not found.");
        }

        return $className::has($value);
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param array $parameters
     *
     * @return mixed
     */
    protected function validateArrayOrRule($attribute, $value, $parameters)
    {
        if (!in_array($value, $parameters)) {
            foreach ($parameters as $parameter) {
                $exp = explode('or_', $parameter, 2);
                if (count($exp) === 2) {
                    $pieces = explode("_", $exp[1]);
                    $method = 'validate' . implode('', array_map('ucfirst', $pieces));

                    if(method_exists(parent::class,  $method)) {
                        if(!call_user_func(array(parent::class, $method),$attribute, $value )) {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        return true;
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     */
    protected function validateFileExists($attribute, $value, $parameters)
    {
        $dir = $parameters[0];

        return file_exists(public_path() . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . $value);
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     */
    protected function validateExistsSoftDeleted($attribute, $value, $parameters)
    {
        $parameters[] = 'deleted_at';
        $parameters[] = 'NULL';

        return parent::validateExists($attribute, $value, $parameters);
    }

    /**
     * @param $attribute
     * @param $value
     * @param $parameters
     *
     * @return bool
     */
    protected function validateUniqueSoftDeleted($attribute, $value, $parameters)
    {
        $parameters[] = 'NULL';
        $parameters[] = 'id';
        $parameters[] = 'deleted_at';
        $parameters[] = 'NULL';

        return parent::validateUnique($attribute, $value, $parameters);
    }

    /**
     * @param $message
     * @param $attribute
     * @param $rule
     * @param $parameters
     *
     * @return mixed
     */
    protected function replaceEnum($message, $attribute, $rule, $parameters)
    {
        $className = $parameters[0];
        $values = implode(',', $className::getAll());

        return str_replace(':enumlist', $values, $message);
    }

    /**
     * @param string $attribute
     * @param string $value
     * @param array $parameters
     *
     * @return mixed
     */
    protected function validateTimestamp($attribute, $value, $parameters)
    {
        return ((string)(int)$value === $value) && ($value <= PHP_INT_MAX) && ($value >= ~PHP_INT_MAX);
    }
}