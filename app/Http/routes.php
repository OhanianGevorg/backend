<?php

Route::get('/', function () {
    return view('welcome');
});

/**
 *  Require authentication routs
 */
Route::group(['middleware' => ['before', 'auth:api', 'after']], function () {
    //MusicLover
    Route::put('musiclover/{id}', 'MusicLoverController@update');
    Route::get('musiclover/{id}/edit', 'MusicLoverController@edit');

    //Artist
    Route::put('artists/{id}', 'ArtistController@update');
    Route::get('artists/{id}/edit', 'ArtistController@edit');

    //Venue
    Route::put('venues/{id}', 'VenueController@update');
    Route::get('venues/{id}/edit', 'VenueController@edit');

    //Wall
    Route::post('/wall', 'WallController@store');
    Route::post('/wall/gig', 'WallController@gigWall');

    // make songs/artists/venues/videos/gig as favorite
    Route::get('/favorite/{entityType}/{entityId}', 'FavoritesController@store');
    Route::delete('/favorite/{entityType}/{entityId}', 'FavoritesController@destroy');

    // follow users
    Route::post('/follow', 'FollowController@store');
    Route::delete('/follow/{user_id}', 'FollowController@destroy');
    Route::get('/followers/{user_id}', 'FollowController@getFollowers');
    Route::get('/followings/{user_id}', 'FollowController@getFollowings');
    Route::get('/follow/search/{user_id}', 'FollowController@searchFollows');

    //Log out
    Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);

    //User Gigs
    Route::get('/user/gigs', 'GigController@userGigs');

    //User Searches Save
    Route::resource('/search/save', 'SearchSaveController');

    //User Searches Save
    Route::resource('/user/videos', 'VideoController');

    //Statistics
    Route::post('/stat/saveShare','StatController@saveShare');
    Route::post('/stat/savePlay','StatController@savePlay');
    Route::get('/stat','StatController@getStats');

    //notifications
    Route::get('users/notification/getNotification','UserNotificationController@getNotifications');
    Route::get('users/notification/getUserNotification','UserNotificationController@getUserNotifications');
    Route::get('users/notification/getActiveNotification','UserNotificationController@getActiveNotifications');
    Route::get('users/notification/resetActiveNotifications','UserNotificationController@resetActiveNotifications');
    Route::post('/test/test-channel','Auth\AuthController@pusherAuth');


    // AUTH USER AVATAR
    Route::get('authavatar/{id}', function($id) {
        return Image::make('pics/'.\App\Models\User::find($id)->avatar)->response('jpg');;
    });

});

/**
 *  Not authenticated routs
 */
Route::group(['middleware' => ['before']], function () {
    //User
    Route::post('user','UserController@store');
    //MusicLover
    Route::post('musiclover', 'MusicLoverController@store');
    Route::get('musiclover', 'MusicLoverController@index');
    Route::get('musiclover/{id}', 'MusicLoverController@show');

    //Artist
    Route::post('artists', 'ArtistController@store');
    Route::get('artists', 'ArtistController@index');
    Route::get('artists/directory', 'ArtistController@getDirectory');
    Route::get('artists/{id}', 'ArtistController@show');

    //Venue
    Route::post('venues', 'VenueController@store');
    Route::get('venues', 'VenueController@index');
    Route::get('venues/directory', 'VenueController@getDirectory');
    Route::get('venues/{id}', 'VenueController@show');

    //Facebook
    Route::resource('search', 'SearchController');

    //Facebook
    Route::resource('auth/facebook', 'FacebookController');

    //Gig
    Route::get('gigs/user', 'GigController@showUserGigs');
    Route::get('gigs/directory', 'GigController@getDirectory');
    Route::resource('gigs', 'GigController');
    Route::resource('gigtypes', 'GigTypeController');
    Route::resource('genres', 'GenresController');


    Route::post('/login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);

    //Upload
    Route::post('/upload', 'UploadController@upload');
    Route::post('/order', 'UploadController@order');

    // Autocomplete
    Route::get('/autocomplete/venues', 'AutocompleteController@venues');
    Route::get('/autocomplete/artists', 'AutocompleteController@artists');
    Route::get('/autocomplete/users', 'AutocompleteController@users');

    Route::get('/artistSongs/{id}', 'SongsController@artistSongs');
    Route::get('/songs/download/{id}','SongsController@download');
    Route::resource('artistsongs', 'SongsController');
    Route::get('/autocomplete/getID/{siteName}', 'AutocompleteController@getID');
    Route::get('/autocomplete/getRoleID/{siteName}', 'AutocompleteController@getRoleID');
});