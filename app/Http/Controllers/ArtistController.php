<?php

namespace App\Http\Controllers;

use App\Http\Transformers\Artist\ArtistDirectoryTransformer;
use App\Http\Transformers\Artist\ArtistUpdateTransformer;
use App\Http\Transformers\Gig\GigArtistTransformer;
use App\Http\Transformers\Shared\UsersNotificationTransformer;
use App\Http\Transformers\Shared\OrderTransformer;
use App\Models\Favorites\FavoriteSongs;
use App\Models\Gig\GigArtist;
use App\Models\Stats\BaseStats;
use App\Models\Stats\UsersStats;
use App\Models\Users\UserNotification;
use App\Models\Users\UserSongs;
use App\Models\Users\UserVideos;
use Carbon\Carbon;
use DB;

use App\Models\User;
use App\Models\Users\UserGenres;
use App\Models\Users\UserPictures;
use App\Models\Gig\GigType;
use App\Http\Requests\Filters\Artist\ArtistFilterRequest;
use App\Http\Requests\Forms\Artist\ArtistStoreRequest;
use App\Http\Requests\Forms\Artist\ArtistUpdateRequest;
use App\Http\Transformers\Shared\SongTransformer;
use App\Http\Transformers\Shared\ReviewTransformer;
use App\Http\Transformers\Shared\VideoTransformer;
use App\Http\Transformers\Shared\PictureTransformer;
use App\Http\Transformers\Artist\ArtistTransformer;
use App\Http\Transformers\Gig\GigTransformer;
use Illuminate\Http\Request;
use App\Models\Order\Order;

use League\Flysystem\Exception;


class ArtistController extends Controller
{
    /**
     * @param ArtistFilterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ArtistFilterRequest $request)
    {
        $artists = ArtistTransformer::transformCollection(User::artists($request)->paginate(30));
        $artists_count = User::artists($request)->count();

        return response()->json([
            'artists' => $artists,
            'artists_count' => $artists_count
        ], 200);
    }


    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {

        $ip = $request->ip();
        $type = BaseStats::action('views');

        $artist = User::where('role_id', '=', User::ARTIST)->find($id);
        if (!$artist) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        $auth_id = (User::auth())?User::auth()->getKey():User::auth();

        if ($type && (int)$id !== $auth_id) {
            $inserted_array = [
                'type' => $type,
                'owner_id' => $artist->getKey(),
                'ip' => $ip,
                'created_at' => Carbon::now()->toDateTimeString()
            ];
            try {
                UsersStats::create($inserted_array);
            } catch (\Exception $exception) {

            }
        }

        return response()->json([
            'artist' => ArtistTransformer::transform($artist)
        ], 200);
    }


    /**
     * @param ArtistStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ArtistStoreRequest $request)
    {
        DB::beginTransaction();

        try {

            $artist = ArtistTransformer::prepare($request->input());
            $artist->save();

            UserGenres::insert(array_map(function ($genre) use ($artist) {
                return [
                    'genre_id' => $genre['id'],
                    'user_id' => $artist->getKey(),
                    'created_at' => Carbon::now()
                ];
            }, $request->input('genres')));

            if ($request->has('studio_pictures')) {
                $artist->pictures()->saveMany(PictureTransformer::prepareCollection($request->input('studio_pictures'), ['type' => UserPictures::PROFILE]));
            }

            if ($request->has('gig_pictures')) {
                $artist->pictures()->saveMany(PictureTransformer::prepareCollection($request->input('gig_pictures'), ['type' => UserPictures::GIG]));
            }

            if ($request->has('songs')) {
                $artist->user_songs()->saveMany(SongTransformer::prepareCollection($request->input('songs')));
            }

            if ($request->has('reviews')) {
                $artist->reviews()->saveMany(ReviewTransformer::prepareCollection($request->input('reviews')));
            }

            if ($request->has('videos')) {
                $artist->videos()->saveMany(VideoTransformer::prepareCollection($request->input('videos')));
            }

            if ($request->has('gig.date') && !empty($request->input('gig.date'))) {
                $gig = GigTransformer::prepare($request->input('gig'), ['user_id' => $artist->getKey()]);
                $gig->save();

                GigType::insert(array_map(function ($type) use ($gig) {
                    return [
                        'gig_system_type_id' => $type['id'],
                        'gig_id' => $gig->getKey(),
                        'created_at' => Carbon::now()
                    ];
                }, $request->input('gig')['gig_types']));

                //Add this artist to gig by default
                $_artist = new GigArtist;
                $_artist->artist_user_id = $artist->getKey();
                $_artist->gig_id = $gig->getKey();
                $_artist->stage_time = $request->input('first_artist_stage_time');
                $_artist->save();

                //Add another added artists
                $gig->gig_artists()->saveMany(GigArtistTransformer::prepareCollection($request->input('gig')['artists']));

            }

            //notification first call
            $notifications = UserNotification::getEventRules($artist->role_id,$artist->getKey());
            $artist->notifications()->saveMany(UsersNotificationTransformer::prepareCollection($notifications));

            DB::commit();

            return response()->json([
                'artist' => ArtistTransformer::transform($artist)
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'error' => $e->getMessage() . $e->getLine() . $e->getFile(),//'Something goes wrong'
            ], 422);
        }
    }


    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $artist = User::auth();

        if (!$artist || $artist->id != $id || !$artist->is(User::ARTIST)) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        return response()->json([
            'artist' => ArtistTransformer::transform($artist)
        ], 200);
    }


    /**
     * @param ArtistUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ArtistUpdateRequest $request)
    {
        $artist = User::auth();
        if (!$artist || !$artist->is(User::ARTIST)) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        DB::beginTransaction();

        try {
            $artist->update(ArtistUpdateTransformer::prepare($request->input()));

            //Remove Genres and insert new ones
            $artist->genres()->delete();
            UserGenres::insert(array_map(function ($genre) use ($artist) {
                return [
                    'genre_id' => $genre['id'],
                    'user_id' => $artist->getKey(),
                    'created_at' => Carbon::now()
                ];
            }, $request->input('genres')));

            if ($request->has('reviews')) {
                $artist->reviews()->delete();
                $artist->reviews()->saveMany(ReviewTransformer::prepareCollection($request->input('reviews')));
            }

            if ($request->has('studio_pictures')) {
                $studio_pictures = $artist->profile_pictures()->get()->lists(['id'])->toArray();
                $exists_list = [];
                //If picture exists check if picture belongs to user
                //If no then save new picture
                foreach ($request->input('studio_pictures') as $picture) {
                    if (isset($picture['id'])) {
                        $_picture = UserPictures::find($picture['id']);
                        if ($_picture) {
                            if ($artist->isOwn($_picture->user_id)) {
                                $_picture->update(array_merge($picture, ['type' => UserPictures::PROFILE]));
                                $exists_list[] = $_picture->getKey();
                            }
                        }
                    } else {
                        $_picture = PictureTransformer::prepare($picture, ['user_id' => $artist->getKey(), 'type' => UserPictures::PROFILE]);
                        $_picture->save();

                        //Save Order Studio Pic (temp)

                        $orderPic = $_picture->toArray();

                        $order = OrderTransformer::prepare($orderPic, ['user_id' => $artist->getKey(), 'item_type' => Order::PROFILE, 'order_number' => '9999']);
                        $order->save();


                    }
                }
                //Get difference of arrays for figure out which pictures have been removed
                $removable_list = array_diff($studio_pictures, $exists_list);
                UserPictures::whereIn('id', $removable_list)->delete();
                Order::whereIn('item_id',$removable_list)->delete();
            }

            if ($request->has('gig_pictures')) {
                $studio_pictures = $artist->gig_pictures()->get()->lists(['id'])->toArray();
                $exists_list = [];
                //If picture exists check if picture belongs to user
                //If no then save new picture
                foreach ($request->input('gig_pictures') as $picture) {
                    if (isset($picture['id'])) {
                        $_picture = UserPictures::find($picture['id']);
                        if ($_picture) {
                            if ($artist->isOwn($_picture->user_id)) {
                                $_picture->update(array_merge($picture, ['type' => UserPictures::GIG]));
                                $exists_list[] = $_picture->getKey();
                            }
                        }
                    } else {
                        $_picture = PictureTransformer::prepare($picture, ['user_id' => $artist->getKey(), 'type' => UserPictures::GIG]);
                        $_picture->save();

                        //Save Order Gig Pic (temp)

                        $orderPic = $_picture->toArray();

                        $order = OrderTransformer::prepare($orderPic, ['user_id' => $artist->getKey(), 'item_type' => UserPictures::GIG, 'order_number' => '9999']);
                        $order->save();
                    }
                }
                //Get difference of arrays for figure out which pictures have been removed
                $removable_list = array_diff($studio_pictures, $exists_list);
                UserPictures::whereIn('id', $removable_list)->delete();
                Order::whereIn('item_id',$removable_list)->delete();
            }

            if ($request->has('songs')) {
                $songs = $artist->songs()->get()->lists(['id'])->toArray();
                $exists_list = [];
                //If song exists check if picture belongs to user
                //If no then save new sons
                foreach ($request->input('songs') as $song) {
                    if (isset($song['id'])) {
                        $_song = UserSongs::find($song['id']);
                        if ($_song) {
                            if ($artist->isOwn($_song->user_id)) {
                                $_song->update($song);
                                $exists_list[] = $_song->getKey();
                            }
                        }
                    } else {
                        $_song = SongTransformer::prepare($song, ['user_id' => $artist->getKey()]);
                        $_song->save();

                        //Save Order Song (temp)

                        $orderSong = $_song->toArray();

                        $order = OrderTransformer::prepare($orderSong, ['user_id' => $artist->getKey(), 'item_type' => Order::SONG, 'order_number' => '9999']);
                        $order->save();
                    }
                }
                //Get difference of arrays for figure out which pictures have been removed
                $removable_list = array_diff($songs, $exists_list);
                UserSongs::whereIn('id', $removable_list)->delete();
                FavoriteSongs::whereIn('song_id', $removable_list)->delete();
                Order::whereIn('item_id',$removable_list)->delete();
            }

            if ($request->has('videos')) {
                $videos = $artist->videos()->get()->lists(['id'])->toArray();
                $exists_list = [];
                //If video exists check if picture belongs to user
                //If no then save new video
                foreach ($request->input('videos') as $video) {
                    if (isset($video['id'])) {
                        $_video = UserVideos::find($video['id']);
                        if ($_video) {
                            if ($artist->isOwn($_video->user_id)) {
                                $_video->update($video);
                                $exists_list[] = $_video->getKey();
                            }
                        }
                    } else {
                        $_video = VideoTransformer::prepare($video, ['user_id' => $artist->getKey()]);
                        $_video->save();
                    }
                }
                //Get difference of arrays for figure out which pictures have been removed
                $removable_list = array_diff($videos, $exists_list);
                UserVideos::whereIn('id', $removable_list)->delete();
            }

            DB::commit();

            return response()->json([
                'status' => 'Successfully updated'
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'error' => $e->getMessage() . $e->getLine() . $e->getFile()//'Something goes wrong'
            ], 422);
        }
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $artist = ArtistModel::find($id);
        if (!$artist) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        DB::beginTransaction();

        try {
            $artist->videos()->delete();
            $artist->reviews()->delete();
            $artist->songs()->delete();
            $artist->data()->delete();
            ArtistGenreModel::where('artist_id', $artist->getKey())->delete();
            GigArtistModel::where('artist_id', $artist->getKey())->delete();
            $artist->delete();

            DB::commit();

            return response()->json([
                'status' => 'Successfully deleted'
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'error' => 'Something goes wrong'
            ], 422);
        }
    }


    /**
     * @param ArtistFilterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDirectory(ArtistFilterRequest $request)
    {
        $inserted_array = [];
        $ip = $request->ip();

        $filtered_artists = User::artists($request);
        User::setDate($request->get('localTime'));
        $artists = ArtistDirectoryTransformer::transformCollection($results = $filtered_artists->paginate(30));
        $artists_count = $results->total();

        //impression statistic block
        $type = BaseStats::action('impressions');

        $auth_id = (User::auth())?User::auth()->getKey():User::auth();
        if ($type) {
            foreach ($artists as $key => $value) {
                if ($auth_id !== (int)$value['id']) {
                    $inserted_array[] = array(
                        'type' => $type,
                        'owner_id' => $value['id'],
                        'ip' => $ip,
                        'created_at' => Carbon::now()

                    );
                }
            }
            UsersStats::insert($inserted_array);
        }


        return response()->json([
            'artists' => $artists,
            'artists_count' => $artists_count
        ], 200);
    }
}