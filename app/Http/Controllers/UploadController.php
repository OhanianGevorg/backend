<?php

namespace App\Http\Controllers;

use App\Http\Transformers\Shared\OrderTransformer;
use App\Models\User;
use App\Models\Users\UserPictures;
use App\Models\Order\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
class UploadController extends Controller
{
    private $pic_rules = [
        'file' => 'image|max:5120'
    ];
    private $audio_rules = [

        'file' => 'max:5120'
    ];
    private $poster_rules = [
        'file' => 'image|dimensions:min_width=383,min_height=541|max:2048'
    ];
    private $poster_messages = [
        'file.dimensions' => 'Min width 383 Min height 541'
    ];

    public function upload(Request $request)
    {

        if ($request->hasFile('file')) {
            $data = $request->all();

            if (isset($data['type']) && !empty($data['type'])) {

                $file = $request->file('file');

                $extension = $file->getClientOriginalExtension();

                if (!$extension) {
                    $extension = 'jpeg';
                }

                switch ($data['type']) {
                    case 'poster':

                        $v = Validator::make($data, $this->poster_rules, $this->poster_messages);

                        if (!$v->fails()) {

                            $file_id = uniqid() . '.' . $extension;

                            $small_file_id = UserPictures::$small_poster['name'] . $file_id;
                            $middle_file_id = UserPictures::$middle_poster['name'] . $file_id;

                            $small_file = Image::make($file)->resize(UserPictures::$small_poster['width'], UserPictures::$small_poster['height'])->encode('jpg',50);
                            $middle_file = Image::make($file)->resize(UserPictures::$middle_poster['width'], UserPictures::$middle_poster['height'])->encode('jpg',50);

                            $file->move(public_path() . '/posters', $file_id);

                            $small_file->save(public_path() . '/posters/' . $small_file_id);

                            $middle_file->save(public_path() . '/posters/' . $middle_file_id);

                            return response()->json([
                                'file_id' => $file_id,
                                'small_file_id' => $small_file_id], 200);

                        } else {

                            $errors = $v->messages()->toArray();

                            return response()->json($errors, 422);
                        }

                        break;
                    case 'audio':

                        $v = Validator::make($data, $this->audio_rules);

                        if (!$v->fails()) {
                            $file_id = uniqid() . '.' . $extension;

                            $file->move(public_path() . '/audios', $file_id);

                            return response()->json(['file_id' => $file_id], 200);

                        } else {
                            $errors = $v->messages()->toArray();

                            return response()->json($errors, 422);
                        }

                        break;
                    case 'pic':

                        $v = Validator::make($data, $this->pic_rules);

                        if (!$v->fails()) {

                            $file_id = uniqid() . '.' . $extension;

                            $file->move(public_path() . '/pics', $file_id);

                            return response()->json([
                                'file_id' => $file_id
                            ], 200);

                        } else {

                            $errors = $v->messages()->toArray();

                            return response()->json($errors, 400);
                        }
                        break;
                    default:
                        return response()->json('Something went wrong', 400);
                }
            } else {
                return response()->json('Something went wrong', 400);
            }
        }

        return response()->json('Upload failed', 400);
    }


    public function order(Request $request)
    {
        $user = User::auth();
        $type = $request->input('type');
        $items = $request->input('items');
        foreach ($items as $key => $value) {
            $user_order = Order::where('user_id', $user->id)->where('item_type', $type)->where('item_id', (int)$value)->first();
            if (count($user_order) == 0) {
                $model = new Order;
                $model->user_id = $user->id;
                $model->order_number = $key;
                $model->item_id = $value;
                $model->item_type = $type;
                $model->save();

            } else {
                $model = Order::where('user_id', $user->id)->where('item_type', $type)->where('item_id', (int)$value);
                $model->update([
                    'order_number' => $key,
                    ]);
            }
        }


    }

}
