<?php

namespace App\Http\Controllers;

use App\Models\Genre\Genre;
use App\Http\Transformers\Shared\GenreTransformer;

class GenresController extends Controller
{
    /**
     * Get system all "genres".
     *
     * @return array
     */
    public function index()
    {
        $genres = Genre::all();

        return response()->json([
            'genres' => GenreTransformer::transformCollection($genres)
        ], 200);
    }
}
