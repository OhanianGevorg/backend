<?php

namespace App\Http\Controllers;

use App\Http\Requests\Filters\Gig\GigFilterRequest;
use App\Http\Requests\Forms\Gig\GigStoreRequest;
use App\Http\Requests\Forms\Gig\GigUpdateRequest;
use App\Http\Requests;
use App\Http\Transformers\Gig\GigArtistTransformer;
use App\Http\Transformers\Gig\GigDirectoryTransformer;
use App\Http\Transformers\Gig\GigTransformer;
use App\Http\Transformers\Gig\GigUpdateTransformer;
use App\Http\Transformers\Gig\UserGigsTransformer;
use App\Http\Transformers\Venue\VenueTransformer;
use App\Models\Gig\Gig;
use App\Http\Controllers\GigPostersModel;

use App\Models\Gig\GigArtist;
use App\Models\Gig\GigType;
use App\Models\Stats\BaseStats;
use App\Models\Stats\GigsStats;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;

class GigController extends Controller
{
    /**
     * @param GigFilterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GigFilterRequest $request)
    {
        $gigs = GigTransformer::transformCollection(Gig::getAll($request)->paginate(30));
        $gigs_count = Gig::getAll($request)->count();
        return response()->json([
            'gigs' => $gigs,
            'gigs_count' => $gigs_count
        ], 200);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $ip = $request->ip();
        $type = BaseStats::action('views');

        $gig = GigTransformer::transform(Gig::find($id));

        if (!$gig) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        $ids = [];
        foreach ($gig['artists'] as $artist => $art) {
            $ids[] = array_get($art, 'id');
        }
        $auth_id = (User::auth()) ? User::auth()->getKey() : User::auth();
        if ($type && !in_array($auth_id, $ids)) {

            $inserted_array = [
                'type' => $type,
                'owner_id' => $gig['id'],
                'ip' => $ip,
                'user_id' => $gig['user_id'],
                'created_at' => Carbon::now()
            ];

            GigsStats::create($inserted_array);

        }

        return response()->json([
            'gig' => $gig
        ], 200);
    }

    /**
     * @param GigStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GigStoreRequest $request)
    {


        DB::beginTransaction();

        try {
            $user = User::auth();
            $userId = $user->getKey();

            if (!$user) {
                return response('Unauthorized.', 401);
            };


//            $decode_image = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->input('poster.file_id')));
//
//            $destinationPath = public_path() . "/posters/";
//            $file_id = uniqid() . '.png';
//
//            if (!file_exists($destinationPath))
//            {
//                mkdir($destinationPath,0777,true);
//            }
//            $file = $destinationPath.$file_id;
//            file_put_contents($file, $decode_image);


            $gig = GigTransformer::prepare($request->all(), ['user_id' => $userId]);
            $gig->save();

            GigType::insert(array_map(function ($type) use ($gig) {
                return ['gig_system_type_id' => $type['id'], 'gig_id' => $gig->getKey(), 'created_at' => Carbon::now()];
            }, $request->input('gig_types')));


            //Add another added artists
            if ($request->has('artists') && !empty($request->input('artists'))) {
                $gig->gig_artists()->saveMany(GigArtistTransformer::prepareCollection($request->input('artists')));
            }
            DB::commit();

            return response()->json(['gig' => GigTransformer::transform($gig),]);

        } catch (\Exception $e) {
            DB::rollback();

            return response()->json([
                'error' => $e->getMessage() . $e->getLine() . $e->getFile(),//'Something goes wrong'
            ], 422);
        }
    }


    /**
     * @param GigUpdateRequest $request
     * @param int $gigId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(GigUpdateRequest $request, $gigId)
    {
        $gig = Gig::find($gigId);

        $user = User::auth();

        //check if user has permissions to update this gig
        if (!$gig || $user->getKey() != $gig->user_id) {
            return response()->json([
                'error' => 'You Can not edit this Gig'
            ], 404);
        }

        DB::beginTransaction();

        try {
            $gig->update(GigUpdateTransformer::prepare($request->input()));

            $gig->gigTypes()->delete();

            //Remove Gig Types related table's values and insert new ones

            GigType::insert(array_map(function ($gigType) use ($gig) {
                return [
                    'gig_system_type_id' => $gigType['id'],
                    'gig_id' => $gig->getKey(),
                    'created_at' => Carbon::now()
                ];
            }, $request->input('types')));

            $gig->gig_artists()->delete();

            //Remove Gig Artist related table's values and insert new ones

            GigArtist::insert(array_map(function ($gigArtist) use ($gig) {
                return [
                    'gig_id' => $gig->getKey(),
                    'artist_user_id' => $gigArtist['id'],
                    'stage_time' => $gigArtist['stage_time'],
                    'created_at' => Carbon::now()
                ];
            }, $request->input('artists')));

            DB::commit();

            return response()->json([
                'status' => 'Successfully updated'
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'error' => $e->getMessage() . $e->getLine() . $e->getFile()//'Something goes wrong'
            ], 422);
        }

    }


    public function userGigs()
    {
        $user = User::auth();
        if (!$user) {
            return response('Unauthorized.', 401);
        };

        return response()->json(UserGigsTransformer::transform($user));
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $user = User::auth();

        $gig = Gig::find($id);

        if (!$gig || $user->getKey() != $gig->user_id) {
            return response()->json([
                'error' => 'You have not access to this action'
            ], 404);
        }
        DB::beginTransaction();
        try {
            $gig->gig_artists()->delete();
            $gig->delete();

            DB::commit();

            return response()->json([
                'success' => 'Gig successfully deleted'
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'error' => 'Something goes wrong!'
            ], 422);
        }
    }

    public function showUserGigs(GigFilterRequest $request)
    {
        $user = null;
        if ($request->has('id')) {

            $user = User::find($request->get('id'));

        }
        if (!$user) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }
        $filtered_gigs = $user->userGigsFilter($request);
        $gigs = GigTransformer::transformCollection($results = $filtered_gigs->paginate(3));

        return response()->json([
            'gigs' => $gigs,
            'count' => $results->total()
        ],200);
    }


    /**
     * @param GigFilterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDirectory(GigFilterRequest $request)
    {
        $inserted_array = [];
        $ip = $request->ip();
        $map_view = $request->input('map_view');
//        dd($map_view);

        $filtered_gigs = Gig::getAll($request);
        if ($map_view == 'false') {
            $gigs = GigDirectoryTransformer::transformCollection($results = $filtered_gigs->paginate(36));
        } else {
            $gigs = GigDirectoryTransformer::transformCollection($results = $filtered_gigs->paginate(85)); //temp
        }

        //impression statistic block
        $type = BaseStats::action('impressions');

        $auth_id = (User::auth()) ? User::auth()->getKey() : User::auth();

        if ($type) {
            foreach ($gigs as $key => $value) {
                $ids = [];
                foreach ($value['artists'] as $artist => $art) {
                    $ids[] = array_get($art, 'id');
                }

                if ((int)$value['venue']['id'] !== $auth_id && !in_array($auth_id, $ids)) {
                    $inserted_array[] = array(
                        'type' => $type,
                        'owner_id' => $value['id'],
                        'ip' => $ip,
                        'user_id' => $value['user_id'],
                        'created_at' => Carbon::now()
                    );
                }
            }
            GigsStats::insert($inserted_array);
        }

        $gigs_count = $results->total();

        return response()->json([
            'gigs' => $gigs,
            'gigs_count' => $gigs_count
        ], 200);
    }
}