<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Http\Transformers\SongTransformer;
use App\Models\Favorites\FavoriteSongs;
use App\Models\User;
use App\Models\Users\ArtistModel;
use App\Models\Users\UserSongs;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class SongsController extends Controller
{
    use SoftDeletes;

    /**
     * @param int $artistId
     * @return \Illuminate\Http\JsonResponse
     */
    public function artistSongs($artistId)
    {
        $artist = ArtistModel::find($artistId);
        if (!$artist) {
            abort(404);
        }

        return response()->json(['songs' => SongTransformer::transformCollection($artist->songs()->get())]);
    }

    public function destroy($id)
    {
        $user = User::auth();

        $song = UserSongs::find($id);


        if (!$song || $user->getKey() != $song->user_id) {
            return response()->json([
                'error' => 'You have not access to this action'
            ], 404);
        }
        DB::beginTransaction();
        try {
            // File::delete(public_path() . '/audios', $song->file_id);
            $faves_song = FavoriteSongs::where('song_id', $song->id);
            $faves_song->delete();
            $song->delete();

            DB::commit();

            return response()->json([
                'success' => 'Song successfully deleted'
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'error' => 'Something goes wrong!'
            ], 422);
        }
    }

    public function download(\Illuminate\Http\Request $request, $id)
    {
        $song = UserSongs::find($id);

        $ip = $request->ip();

        if ($song->permission !== 'allow') {
            return response()->view('errors.downloadError', [
                'error' => 'Permission Denied'
            ]);
        }

        $file = public_path('/audios/' . $song->file_id);

        if (!file_exists($file)) {
            return response()->view('errors.downloadError', [
                'error' => 'File Does not Exists'
            ]);
        }

        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="' . basename($file) . '"',
            'Expires' => 0,
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public',
            'Content-Length: ' => filesize($file)
        ];

        $auth_id = (User::auth())?User::auth()->getKey():User::auth();

        if ((int)$song->user_id !== $auth_id) {
            $stat = new StatController();
            $stat->downloadStat($ip, $song);
        }
        return response()->download($file, $song->file_name, $headers);
    }

}