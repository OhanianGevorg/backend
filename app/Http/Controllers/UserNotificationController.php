<?php

namespace App\Http\Controllers;

use App\Http\Transformers\Notifications\ActiveNotificationTransformer;
use App\Http\Transformers\Notifications\NotificationTransformer;
use App\Http\Transformers\Shared\UsersNotificationTransformer;
use App\Models\User;
use App\Models\Users\Notification;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class UserNotificationController extends Controller
{
    //
    public function getNotifications()
    {
        $user = User::auth();
        if (!$user) {
            return response()->json([
                'error' => 'You have not permission to continue'
            ]);
        }
        return response()->json([
            'notifications_list' => UsersNotificationTransformer::transformCollection($user->notifications)
        ]);
    }

    public function getUserNotifications(Request $request)
    {

        $user = User::auth();
        if (!$user) {
            return response()->json([
                'error' => 'You have not permission to continue'
            ]);
        }
        return response()->json([
            'user_notifications_list' => NotificationTransformer::transformCollection($user->getUserNotifications)
        ]);
    }

    public function getActiveNotifications()
    {
        $user = User::auth();

        if (!$user) {
            return response()->json([
                'error' => 'You have not permission to continue'
            ]);
        }
        return response()->json([
            'notifications_list' =>   ActiveNotificationTransformer::transformCollection($user->getUserNotifications()->where('is_online','=','no')->get())
        ]);
    }

    public function ResetActiveNotifications()
    {
        $user = User::auth();

       $model = Notification::where('notified_user_id',$user->getKey())->where('is_online','no');
       $model->update(['is_online' => 'yes']);


        if (!$user) {
            return response()->json([
                'error' => 'You have not permission to continue'
            ]);
        }
        return response()->json([
            'status' =>   'reseted'
        ]);
    }
}



