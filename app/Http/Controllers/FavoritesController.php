<?php
namespace App\Http\Controllers;

use App\Models\Gig\Gig;
use App\Models\Stats\BaseStats;
use App\Models\Stats\GigsStats;
use App\Models\Stats\SongsStats;
use App\Models\Stats\UsersStats;
use App\Models\Stats\VideosStats;
use App\Models\User;
use App\Models\Users\UserFollows;
use App\Models\Users\UserSongs;
use App\Models\Users\UserVideos;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use DB;

class FavoritesController extends Controller
{

    private static $entityToClassName = [
        'artist' => [
            'baseClassName' => 'FavoriteUsers',
            'entityFieldName' => 'entity_user_id',
            'validationRules' => ['int', 'exists:users,id'],
            'additionalRules' => ['unique:user_favorite_users,entity_user_id,user_id'],
        ],
        'venue' => [
            'baseClassName' => 'FavoriteUsers',
            'entityFieldName' => 'entity_user_id',
            'validationRules' => ['int', 'exists:users,id'],
            'additionalRules' => ['unique:user_favorite_users,entity_user_id,user_id'],
        ],
        'gig' => [
            'baseClassName' => 'FavoriteGigs',
            'entityFieldName' => 'gig_id',
            'validationRules' => ['int', 'exists:gigs,id'],
            'additionalRules' => ['unique:user_favorite_gigs,gig_id,user_id'],
        ],
        'song' => [
            'baseClassName' => 'FavoriteSongs',
            'entityFieldName' => 'song_id',
            'validationRules' => ['int', 'exists:user_songs,id'],
            'additionalRules' => ['unique:user_favorite_songs,song_id,user_id'],
        ],
        'video' => [
            'baseClassName' => 'FavoriteVideos',
            'entityFieldName' => 'video_id',
            'validationRules' => ['int', 'exists:user_videos,id'],
            'additionalRules' => ['unique:user_favorite_videos,video_id,user_id'],
        ],
    ];


    /**
     * @param string $entityType
     * @param int $entityId
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request,$entityType, $entityId)
    {

        $ip = $request->ip();
        $type = BaseStats::action('saves');
        $statsClass = '';

        if (!array_key_exists($entityType, self::$entityToClassName)) {
            return response()->json([
                'error' => 'Access Denied'
            ], 403);
        }

        $insert_array = [
            'type' => $type,
            'owner_id' => $entityId,
            'ip' => $ip,
            'created_at' => Carbon::now()
        ];

        switch ($entityType){

            case('artist'):
            case ('venue'):
                $statsClass = UsersStats::class;
                break;
            case('gig'):
                $gig_owner = Gig::find($entityId);
                $insert_array['user_id'] = ($gig_owner)?$gig_owner->user_id:'';
                $statsClass = GigsStats::class;
                break;
            case ('song'):
                $song_owner = UserSongs::find($entityId);
                $insert_array['user_id'] = ($song_owner)?$song_owner->user_id:'';
                $statsClass = SongsStats::class;
                break;
            case ('video'):
                $video_owner = UserVideos::find($entityId);
                $insert_array['user_id'] = ($video_owner)?$video_owner->user_id:'';
                $statsClass = VideosStats::class;
                break;
            default :
                $insert_array['user_id'] = null;
        }

        $user = User::auth();
        if (!$user) {
            return response()->json([
                'error' => 'Access Denied'
            ], 403);
        }


        $user_id = $user->getKey();

        $fieldName = self::$entityToClassName[$entityType]['entityFieldName'];

        $favoriteData = [
            $fieldName => $entityId,
            'user_id' => $user_id,
        ];

        if ($entityType == 'artist' || $entityType == 'venue') {
            $favoriteData['entity_type'] = $entityType;
        }

        $className = '\\App\\Models\\Favorites\\' . self::$entityToClassName[$entityType]['baseClassName'];

        if (!$className::where($favoriteData)->first()) {

            DB::beginTransaction();

            try {
                $model = new $className;
                $model->fill($favoriteData);
                $model->save();
                if ($entityType == 'artist' || $entityType == 'venue') {
                    if(!UserFollows::isFollowed($user->getKey(),$entityId)){
                        $model = new UserFollows();
                        $model->follower_user_id = $user->getKey();
                        $model->follow_user_id = $entityId;
                        $model->save();
                    }
                }
                if($type){
                    $statsClass::insert($insert_array);
                }

                DB::commit();
                return response()->json([
                    'favorite' => $model,
                    'success' => 'Successfully created'
                ], 200);

            } catch (\Exception $e) {


                DB::rollback();

                return response()->json([
                    'error' => $e->getMessage() . $e->getLine() . $e->getFile(),//'Something goes wrong'
                ], 422);
            }

        } else {
            return response()->json([
                'error' => 'Already exist'
            ], 422);
        }

    }

    /**
     * @param $entityType
     * @param $entityId
     *
     * @return Response
     */
    public function destroy($entityType, $entityId)
    {
        if (!array_key_exists($entityType, self::$entityToClassName)) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        $user = User::auth();

        if (!$user) {
            return response()->json([
                'error' => 'Access Denied'
            ], 403);
        }

        $user_id = $user->getKey();

        $fieldName = self::$entityToClassName[$entityType]['entityFieldName'];
        $favoriteData = [
            $fieldName => $entityId,
            'user_id' => $user_id,
        ];

        $className = '\\App\\Models\\Favorites\\' . self::$entityToClassName[$entityType]['baseClassName'];

        $model = new $className;


        try {
            $favorite = $model->where($favoriteData)->first();
            $favorite->delete();
            if ($entityType == 'artist' || $entityType == 'venue') {
                $model = UserFollows::where('follower_user_id', '=', $user->getKey())->where('follow_user_id', '=', $entityId);
                $model->delete();
            }
            return response()->json([
                'success' => 'successfully deleted'
            ], 200);
        } catch (\Exception $e) {

            return response()->json([
                'error' => $e->getMessage()
            ], 403);

        }

    }
}