<?php

namespace App\Http\Controllers;

use App\Http\Transformers\Shared\SearchSavesTransformer;
use App\Models\User;
use App\Models\Users\UserSearchSaves;
use Illuminate\Http\Request;

use App\Http\Requests;

class SearchSaveController extends Controller
{

    public function index(Request $request)
    {
        $user = User::auth();
        if (!$user) {
            return response()->json([
                'error' => 'Access Denied'
            ], 403);
        }
        if ($request->has('type'))
        {
            $searchSaves = SearchSavesTransformer::transformCollection(UserSearchSaves::where('user_id','=',$user->getKey())->where('type','=',$request->input('type'))->get());

            return response()->json(['searchSaves' => $searchSaves]);
        }

    }

    //need to put in request file
    public function store(Request $request)
    {
        $user = User::auth();
        if (!$user) {
            return response()->json([
                'error' => 'Access Denied'
            ], 403);
        }

        $searchSave = SearchSavesTransformer::prepare($request->input());
        $save = $searchSave->save();

        if(!$save){
            return response()->json([
               'error'=>'Something goes wrong'
            ],403);
        }

        return response()->json([
            'saves' => SearchSavesTransformer::transform($searchSave),
            'success'=>'Save successfully '
        ], 200);
    }

    public function destroy($id)
    {
        $user = User::auth();
        $UserSearchSaves = UserSearchSaves::find($id);

        if(!$UserSearchSaves||$user->getKey()!=$UserSearchSaves->user_id){
        return response()->json([
            'error'=>'You have not access to this action'
        ],404);
    }
        $UserSearchSaves->delete();

        if(!$UserSearchSaves){
            return response()->json([
                'error'=>'Something goes wrong!'
            ],422);
        }
        return response()->json([
            'success'=>'Save successfully deleted'
        ],200);
    }
}
