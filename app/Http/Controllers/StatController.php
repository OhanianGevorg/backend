<?php

namespace App\Http\Controllers;

use App\Http\Requests\Filters\Statistic\StatisticFilterRequest;
use App\Http\Transformers\Gig\GigSmallTransformer;
use App\Http\Transformers\Shared\SongSmallTransformer;
use App\Http\Transformers\Shared\UserAndStatsTransformer;
use App\Http\Transformers\Shared\VideosAndStatsTransformer;
use App\Models\Gig\Gig;
use App\Models\Stats\BaseStats;
use App\Models\Stats\GigsStats;
use App\Models\Stats\SongsStats;
use App\Models\Stats\UsersStats;
use App\Models\Stats\VideosStats;
use App\Models\User;
use App\Models\Users\UserSongs;
use App\Models\Users\UserVideos;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StatController extends Controller
{
    //

    public function getStats(StatisticFilterRequest $request)
    {

        $id = $request->get('id');
        $user = User::auth();
        if (!$user || $user->getKey() !== (int)$id) {
            return response()->json([
                'error' => 'Permission denied'
            ], 403);
        }

        return response()->json([
            'success' => [
                'songsStats' => SongSmallTransformer::transformCollection(UserSongs::where('user_songs.user_id', '=', $user->getKey())->get(), $request),
                'videosStats' => VideosAndStatsTransformer::transformCollection(UserVideos::where('user_videos.user_id', '=', $user->getKey())->get(), $request),
                'gigsStats' => GigSmallTransformer::transformCollection($user->gig_artist()->get(), $request),
                'profileStats' => UserAndStatsTransformer::transform($user, $request),
                'followers' => $user->followersQuery($request)
            ]
        ]);
    }

    public function saveShare(Request $request)
    {
        $ip = $request->ip();

        $userClass = '';
        $statsClass = '';

        switch ($request->get('type')) {
            case('gigs'):
                $userClass = Gig::class;
                $statsClass = GigsStats::class;
                break;
            case ('songs'):
                $userClass = UserSongs::class;
                $statsClass = SongsStats::class;
                break;
            case ('videos'):
                $userClass = UserVideos::class;
                $statsClass = VideosStats::class;
                break;
            case ('users'):
                $statsClass = UsersStats::class;
                break;
            default:
                $statsClass = '';
                $userClass = '';
        }


        try {
            $inserted_data = [
                'type' => BaseStats::action('shares'),
                'owner_id' => $request->get('id'),
                'created_at' => Carbon::now(),
                'ip' => $ip
            ];

            if ($request->get('type') !== 'users') {
                $owner = $userClass::find($request->get('id'));
                $inserted_data['user_id'] = ($owner) ? $owner->user_id : null;
            }

            $statsClass::insert($inserted_data);

            return response()->json([
                'success' => 'Statistic created'
            ], 200);

        } catch (\Exception $exception) {
            return response()->json([
                'error' => 'Something goes wrong' . $exception->getMessage() . '\n' . $exception->getLine()
            ], 403);
        }

    }

    public function savePlay(Request $request)
    {
        $ip = $request->ip();

        if (!$request->get('id') || !$request->get('type')) {
            return response()->json([
                'error' => 'Something goes wrong'
            ], 403);
        }
        switch ($request->get('type')) {
            case('songs'):
                $ownerClass = UserSongs::class;
                $statsClass = SongsStats::class;
                break;
            case ('videos'):
                $ownerClass = UserVideos::class;
                $statsClass = VideosStats::class;
                break;
            default:
                return response()->json([
                    'error' => 'Something goes wrong'
                ], 403);
        }

        try {

            $owner = $ownerClass::find($request->get('id'));

            $regiteredUserId = (User::auth()) ? User::auth()->getKey() : null;


            if($owner->user_id != $regiteredUserId){
                $inserted_data = [
                    'type' => BaseStats::action('plays'),
                    'owner_id' => $request->get('id'),
                    'ip' => $ip,
                    'user_id' => $owner->user_id,
                    'created_at' => Carbon::now(),
                ];

                $statsClass::insert($inserted_data);

                return response()->json([
                    'success' => 'Statistic created'
                ], 200);
            }

        } catch (\Exception $exception) {
            return response()->json([
                'error' => 'Something goes wrong' . $exception->getMessage() . '\n' . $exception->getLine()
            ], 403);
        }
    }

    public function downloadStat($ip, UserSongs $song)
    {
        try {
            $inserted_array = [
                'type' => BaseStats::action('downloads'),
                'owner_id' => $song->id,
                'ip' => $ip,
                'user_id' => $song->user_id,
                'created_at' => Carbon::now(),
            ];

            SongsStats::insert($inserted_array);

            return response()->json([
                'success' => 'Statistic is created'
            ], 200);

        } catch (\Exception $exception) {

            return response()->json([
                'error' => 'Something goes wrong' . $exception->getMessage() . '\n' . $exception->getLine()
            ], 403);
        }
    }
}
