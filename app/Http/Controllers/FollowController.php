<?php

namespace App\Http\Controllers;

use App\Events\FollowNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Transformers\Shared\FollowUserTransformer;
use App\Models\Users\UserFollows;
use App\Models\User;
use App\Http\Requests\Filters\Search\FollowFilterRequest;
use Illuminate\Support\Facades\Event;

class FollowController extends Controller
{
    /**
     * Get system all "genres".
     *
     * @return array
     */
    public function store(Request $request)
    {
        $user = User::auth();

        if (!$user) {
            return response()->json([
                'error' => 'Access Denied'
            ], 403);
        }

        $model = new UserFollows;
        $model->follower_user_id = $user->getKey();
        $model->follow_user_id = $request->input('id');
        $model->save();

//        Event::fire(new FollowNotification($model));

        return response()->json([
            'status' => 'ok'
        ], 200);
    }

    public function destroy($user_id)
    {
        $user = User::auth();
        if (!$user) {
            return response()->json([
                'error' => 'Access Denied'
            ], 403);
        }

        $model = new UserFollows;
        $follow = $model->where(['follower_user_id' => $user->getKey(), 'follow_user_id' => $user_id])->first();

        if ($follow) {
            if ($follow->delete()) {
                return response()->json([
                    'status' => 'ok'
                ]);
            }
        }

        return response()->json([
            'error' => 'Something goes wrong'
        ], 403);
    }

    public function getFollowers(Request $request,$user_id)
    {
        $time = Carbon::now();

        if($request->has('time')){
            $time = $request->get('time');
        }

        $user = User::find($user_id);

        $user->setDate($time);

        if (!$user || $user->getKey() != $user_id) {
            return response()->json([
                'error' => 'User is not found'
            ], 403);
        }
        return response()->json([
            'followers' => FollowUserTransformer::transformCollection($user->followers())
        ], 200);
    }

    public function getFollowings(Request $request,$user_id)
    {
        $time = Carbon::now();

        if($request->has('time')){
            $time = $request->get('time');
        }

        $user = User::find($user_id);

        $user->setDate($time);

        if (!$user || $user->getKey() != $user_id) {
            return response()->json([
                'error' => 'User is not found'
            ], 403);
        }
        return response()->json([
            'followings' => FollowUserTransformer::transformCollection($user->followings())
        ], 200);
    }

    public function searchFollows(FollowFilterRequest $request,$id)
    {

        $follows = User::follows($request,$id)->pluck('users.id');

        $user = User::whereIn('users.id',$follows)->get();

        return response()->json([
           'follows' =>FollowUserTransformer::transformCollection($user)
        ],200);

    }
}
