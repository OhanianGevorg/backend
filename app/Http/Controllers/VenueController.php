<?php

namespace App\Http\Controllers;


use App\Http\Transformers\Gig\GigArtistTransformer;
use App\Http\Transformers\Shared\UsersNotificationTransformer;
use App\Http\Transformers\Shared\OrderTransformer;
use App\Http\Transformers\Venue\VenueBasicTransformer;
use App\Http\Transformers\Venue\VenueUpdateTransformer;
use App\Models\Order\Order;
use App\Models\Stats\BaseStats;
use App\Models\Stats\UsersStats;
use App\Models\Users\UserNotification;
use Carbon\Carbon;
use DB;

use App\Models\User;
use App\Models\Users\UserGenres;
use App\Models\Users\UserPictures;
use App\Models\Gig\GigType;
use App\Models\Users\UserVideos;
use App\Http\Requests\Filters\Venue\VenueFilterRequest;
use App\Http\Requests\Forms\Venue\VenueStoreRequest;
use App\Http\Requests\Forms\Venue\VenueUpdateRequest;
use App\Http\Transformers\Shared\ReviewTransformer;
use App\Http\Transformers\Shared\VideoTransformer;
use App\Http\Transformers\Shared\PictureTransformer;
use App\Http\Transformers\Venue\VenueTransformer;
use App\Http\Transformers\Gig\GigTransformer;
use Illuminate\Http\Request;

class VenueController extends Controller
{
    /**
     * @param VenueFilterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(VenueFilterRequest $request)
    {
        $venues = VenueTransformer::transformCollection(User::venues($request)->paginate(30));
        $venues_count = User::venues($request)->count();

        return response()->json([
            'venues' => $venues,
            'venues_count' => $venues_count
        ], 200);
    }


    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        $ip = $request->ip();
        $type = BaseStats::action('views');

        $venue = User::where('role_id', '=', User::VENUE)->find($id);

        if (!$venue) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        $auth_id = (User::auth())?User::auth()->getKey():User::auth();

        if ($type && (int)$id !== $auth_id) {
            $inserted_array = [
                'type' => $type,
                'owner_id' => $venue['id'],
                'ip' => $ip,
                'created_at' => Carbon::now()
            ];
            UsersStats::create($inserted_array);
        }

        return response()->json([
            'venue' => VenueTransformer::transform($venue)
        ], 200);
    }


    /**
     * @param VenueStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VenueStoreRequest $request)
    {
        DB::beginTransaction();

        try {
            $venue = VenueTransformer::prepare($request->input());
            $venue->save();

            UserGenres::insert(array_map(function ($genre) use ($venue) {
                return [
                    'genre_id' => $genre['id'],
                    'user_id' => $venue->getKey(),
                    'created_at' => Carbon::now()
                ];
            }, $request->input('genres')));

            if ($request->has('venue_pictures')) {
                $venue->pictures()->saveMany(PictureTransformer::prepareCollection($request->input('venue_pictures'), ['type' => UserPictures::PROFILE]));
            }

            if ($request->has('gig_pictures')) {
                $venue->pictures()->saveMany(PictureTransformer::prepareCollection($request->input('gig_pictures'), ['type' => UserPictures::GIG]));
            }

            if ($request->has('reviews')) {
                $venue->reviews()->saveMany(ReviewTransformer::prepareCollection($request->input('reviews')));
            }

            if ($request->has('videos')) {
                $venue->videos()->saveMany(VideoTransformer::prepareCollection($request->input('videos')));
            }

            if ($request->has('gig.date') && !empty($request->input('gig.date'))) {
                $gig = GigTransformer::prepare($request->input('gig'), ['user_id' => $venue->getKey(), 'venue_user_id' => $venue->getKey()]);
                $gig->save();

                //Add  artists
                $gig->gig_artists()->saveMany(GigArtistTransformer::prepareCollection($request->input('gig')['artists']));

                GigType::insert(array_map(function ($type) use ($gig) {
                    return [
                        'gig_system_type_id' => $type['id'],
                        'gig_id' => $gig->getKey(),
                        'created_at' => Carbon::now()
                    ];
                }, $request->input('gig')['gig_types']));

            }

            //add notifications
            $notifications = UserNotification::getEventRules($venue->role_id,$venue->getKey());
            $venue->notifications()->saveMany(UsersNotificationTransformer::prepareCollection($notifications));

            DB::commit();

            return response()->json([
                'venue' => VenueTransformer::transform($venue)
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'error' => $e->getMessage() . $e->getLine() . $e->getFile(),//'Something goes wrong'
            ], 422);
        }
    }


    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $venue = User::auth();

        if (!$venue || $venue->id != $id || !$venue->is(User::VENUE)) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }


        return response()->json([
            'venue' => VenueTransformer::transform($venue)
        ], 200);
    }


    /**
     * @param VenueUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VenueUpdateRequest $request)
    {
        $venue = User::auth();
        if (!$venue || !$venue->is(User::VENUE)) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        DB::beginTransaction();

        try {
            $venue->update(VenueUpdateTransformer::prepare($request->input()));

            //Remove Genres and insert new ones
            $venue->genres()->delete();
            UserGenres::insert(array_map(function ($genre) use ($venue) {
                return [
                    'genre_id' => $genre['id'],
                    'user_id' => $venue->getKey(),
                    'created_at' => Carbon::now()
                ];
            }, $request->input('genres')));

            if ($request->has('reviews')) {
                $venue->reviews()->delete();
                $venue->reviews()->saveMany(ReviewTransformer::prepareCollection($request->input('reviews')));
            }

            if ($request->has('venue_pictures')) {
                $studio_pictures = $venue->profile_pictures()->get()->lists(['id'])->toArray();
                $exists_list = [];
                //If picture exists check if picture belongs to user
                //If no then save new picture
                foreach ($request->input('venue_pictures') as $picture) {
                    if (isset($picture['id'])) {
                        $_picture = UserPictures::find($picture['id']);
                        if ($_picture) {
                            if ($venue->isOwn($_picture->user_id)) {
                                $_picture->update(array_merge($picture, ['type' => UserPictures::PROFILE]));
                                $exists_list[] = $_picture->getKey();
                            }
                        }
                    } else {
                        $_picture = PictureTransformer::prepare($picture, ['user_id' => $venue->getKey(), 'type' => UserPictures::PROFILE]);
                        $_picture->save();

                        //Save Order Studio Pic (temp)

                        $orderPic = $_picture->toArray();

                        $order = OrderTransformer::prepare($orderPic, ['user_id' => $venue->getKey(), 'item_type' => UserPictures::PROFILE, 'order_number' => '9999']);
                        $order->save();
                    }
                }
                //Get difference of arrays for figure out which pictures have been removed
                $removable_list = array_diff($studio_pictures, $exists_list);
                UserPictures::whereIn('id', $removable_list)->delete();
                Order::whereIn('item_id',$removable_list)->delete();
            }

            if ($request->has('gig_pictures')) {
                $studio_pictures = $venue->gig_pictures()->get()->lists(['id'])->toArray();
                $exists_list = [];
                //If picture exists check if picture belongs to user
                //If no then save new picture
                foreach ($request->input('gig_pictures') as $picture) {
                    if (isset($picture['id'])) {
                        $_picture = UserPictures::find($picture['id']);
                        if ($_picture) {
                            if ($venue->isOwn($_picture->user_id)) {
                                $_picture->update(array_merge($picture, ['type' => UserPictures::GIG]));
                                $exists_list[] = $_picture->getKey();
                            }
                        }
                    } else {
                        $_picture = PictureTransformer::prepare($picture, ['user_id' => $venue->getKey(), 'type' => UserPictures::GIG]);
                        $_picture->save();

                        //Save Order Gig Pic (temp)

                        $orderPic = $_picture->toArray();

                        $order = OrderTransformer::prepare($orderPic, ['user_id' => $venue->getKey(), 'item_type' => UserPictures::GIG, 'order_number' => '9999']);
                        $order->save();
                    }
                }
                //Get difference of arrays for figure out which pictures have been removed
                $removable_list = array_diff($studio_pictures, $exists_list);
                UserPictures::whereIn('id', $removable_list)->delete();
                Order::whereIn('item_id',$removable_list)->delete();
            }

            if ($request->has('videos')) {
                $videos = $venue->videos()->get()->lists(['id'])->toArray();
                $exists_list = [];
                //If video exists check if picture belongs to user
                //If no then save new video
                foreach ($request->input('videos') as $video) {
                    if (isset($video['id'])) {
                        $_video = UserVideos::find($video['id']);
                        if ($_video) {
                            if ($venue->isOwn($_video->user_id)) {
                                $_video->update($video);
                                $exists_list[] = $_video->getKey();
                            }
                        }
                    } else {
                        $_video = VideoTransformer::prepare($video, ['user_id' => $venue->getKey()]);
                        $_video->save();
                    }
                }
                //Get difference of arrays for figure out which pictures have been removed
                $removable_list = array_diff($videos, $exists_list);
                UserVideos::whereIn('id', $removable_list)->delete();
            }

            DB::commit();

            return response()->json([
                'status' => 'Successfully updated'
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'error' => $e->getMessage() . $e->getLine() . $e->getFile()//'Something goes wrong'
            ], 422);
        }
    }


    /**
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $venue = VenueModel::find($id);
        if (!$venue) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        $venue->reviews()->delete();
        $venue->data()->delete();
        VenueGenreModel::where('venue_id', $venue->getKey())->delete();
        GigModel::where('venue_id', $venue->getKey())->delete();
        //TODO: delete other stuff related to deleted gigs
        $venue->delete();
    }

    /**
     * @param VenueFilterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDirectory(VenueFilterRequest $request)
    {

        $inserted_array = [];
        $ip = $request->ip();

        $filtered_venues = User::venues($request);
        User::setDate($request->get('localTime'));
        $venues = VenueBasicTransformer::transformCollection($results = $filtered_venues->paginate(30));
        $venues_count = $results->total();
//        $all_venues = $results->total();
        //impression statistic block
        $type = BaseStats::action('impressions');

        $auth_id = (User::auth())?User::auth()->getKey():User::auth();

        if ($type) {
            foreach ($venues as $key => $value) {
                if ($auth_id !== (int)$value['id']) {
                    $inserted_array[] = array(
                        'type' => $type,
                        'owner_id' => $value['id'],
                        'ip' => $ip,
                        'created_at' => Carbon::now()

                    );
                }
            }

            UsersStats::insert($inserted_array);
        }

        return response()->json([
            'venues' => $venues,
            'venues_count' => $venues_count
        ], 200);
    }

}