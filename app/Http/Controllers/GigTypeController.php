<?php
namespace App\Http\Controllers;

use App\Models\Gig\GigSystemTypes;
use App\Http\Transformers\Gig\GigTypeTransformer;

class GigTypeController extends Controller
{
    /**
     * Get system all "gig types"
     *
     * @return array
     */
    public function index()
    {
        $gig_types = GigTypeTransformer::transformCollection(GigSystemTypes::all());

        return response()->json(['gig_types' => $gig_types]);
    }
}
