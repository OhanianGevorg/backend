<?php

namespace App\Http\Controllers;

use App\Http\Requests\Forms\UserFormRequest;
use App\Http\Transformers\Shared\TimeLineTransformer;
use App\Http\Transformers\Shared\UserBasicTransformer;
use App\Http\Transformers\Shared\UsersNotificationTransformer;
use App\Http\Transformers\Shared\UserTransformer;
use App\Models\User;
use App\Models\Users\UserNotification;
use DB;

class UserController extends Controller
{
    //
    public function index()
    {
//        if (!User::auth()){
//            return response()->json([
//               'error'=>'You have not permissions'
//            ]);
//        }
//
//        return response()
//        return response()->json([
//           'user'=>UserTransformer::transform()
//        ]);
    }

    public function store(UserFormRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = UserBasicTransformer::prepare($request->input());
            $user->save();
            $timelineArray = [
                'user_id' => $user->getKey(),
                'user_type' => $user->role_id,
                'user_name' => ($user->store_name) ? $user->store_name : $user->name . ' ' . $user->surname
            ];
            $notifications = UserNotification::getEventRules($user->role_id, $user->getKey());
            $user->notifications()->saveMany(UsersNotificationTransformer::prepareCollection($notifications));
            $timeLine = TimeLineTransformer::prepare($timelineArray);
            $timeLine->save();
            DB::commit();
            return response()->json([
                'success' => 'Your registration process has done successfully.We have send you an activation email'
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'error' => $e->getMessage() . $e->getLine() . $e->getFile()
            ], 422);
        }
    }
}
