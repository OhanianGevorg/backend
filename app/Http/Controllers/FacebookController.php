<?php

namespace App\Http\Controllers;

use App\Http\Transformers\MusicLover\MusicLoverTransformer;
use App\Http\Transformers\Shared\PictureTransformer;
use App\Http\Transformers\Shared\UserTransformer;
use App\Models\User;
use App\Models\Users\UserPictures;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class FacebookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new User;
        $type = $request->input('type');
        $user = User::where(['fb_user_id' => $request->input('id')])->first();
        $token = md5(Carbon::now());
        if (!$user) {
            $model->role_id = User::MUSIC_LOVER;
            $model->name = html_entity_decode(html_entity_decode($request->input('name')));
            $model->fb_user_id = $request->input('id');
            $model->email = $request->input('id').'@facebook.com';
            $model->api_token = $token;
            $model->save();

//            $picture = "https://graph.facebook.com/".$request->input('id')."/picture";

            if ($model->save()) {
                return response()->json([
                    'user' => UserTransformer::transform($model),
                    'api_token' => $token
                ], 200);
            }

            return response()->json([
                'error' => 'Something goes wrong'
            ], 422);
        } else {
            $user->api_token = $token;
            $user->save();

            return response()->json([
                'user' => UserTransformer::transform($user),
                'api_token' => $token,
                'auth_type' => $type
            ], 200);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
