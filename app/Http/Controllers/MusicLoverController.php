<?php

namespace App\Http\Controllers;

use App\Http\Requests\Forms\MusicLover\MusicLoverStoreRequest;
use App\Http\Requests\Forms\MusicLover\MusicLoverUpdateRequest;
use App\Http\Transformers\MusicLover\MusicLoverTransformer;
use App\Http\Transformers\MusicLover\MusicLoverUpdateTransformer;
use App\Http\Transformers\Shared\UsersNotificationTransformer;
use App\Http\Transformers\Shared\OrderTransformer;
use App\Http\Transformers\Shared\PictureTransformer;
use App\Models\Order\Order;
use App\Models\User;
use App\Models\Users\UserGenres;
use App\Models\Users\UserNotification;
use App\Models\Users\UserPictures;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MusicLoverController extends Controller
{
    /**
     * @param
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        //$musicLovers = User::music_lovers();
        //return response()->json(['music_lovers' => MusicLoverTransformer::transformCollection($musicLovers)]);
    }


    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $musicLover = User::where('role_id','=',User::MUSIC_LOVER)->find($id);

        if (!$musicLover) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        return response()->json([
            'music_lover' => MusicLoverTransformer::transform($musicLover)
        ], 200);
    }


    /**
     * @param MusicLoverStoreRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MusicLoverStoreRequest $request)
    {
        $musicLover = MusicLoverTransformer::prepare($request->input());

        if ($musicLover->save()) {
            //add notification
            $notifications = UserNotification::getEventRules($musicLover->role_id,$musicLover->getKey());
            $musicLover->notifications()->saveMany(UsersNotificationTransformer::prepareCollection($notifications));
            return response()->json([
                'musiclover' => MusicLoverTransformer::transform($musicLover)
            ], 200);
        }

        return response()->json([
            'error' => 'Something goes wrong'
        ], 422);
    }


    /**
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $musicLover = User::auth();

        if (!$musicLover || $musicLover->id != $id||!$musicLover->is(User::MUSIC_LOVER)) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        return response()->json([
            'music_lover' => MusicLoverTransformer::transform($musicLover)
        ], 200);
    }


    /**
     * @param MusicLoverUpdateRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(MusicLoverUpdateRequest $request)
    {
        $musicLover = User::auth();

        if (!$musicLover||!$musicLover->is(User::MUSIC_LOVER)) {
            return response()->json([
                'error' => 'The page you are looking for not found'
            ], 404);
        }

        DB::beginTransaction();

        try {
            $musicLover->update(MusicLoverUpdateTransformer::prepare($request->input()));

            //Remove Genres and insert new ones
            if ($request->has('genres')) {
                $musicLover->genres()->delete();
                UserGenres::insert(array_map(function ($genre) use ($musicLover) {
                    return [
                        'genre_id' => $genre['id'],
                        'user_id' => $musicLover->getKey(),
                        'created_at' => Carbon::now()
                    ];
                }, $request->input('genres')));

            }

            if ($request->has('profile_pictures')) {
                $studio_pictures = $musicLover->profile_pictures()->get()->lists(['id'])->toArray();
                $exists_list = [];
                //If picture exists check if picture belongs to user
                //If no then save new picture
                foreach ($request->input('profile_pictures') as $picture) {
                    if (isset($picture['id'])) {
                        $_picture = UserPictures::find($picture['id']);
                        if ($_picture) {
                            if ($musicLover->isOwn($_picture->user_id)) {
                                $_picture->update(array_merge($picture, ['type' => UserPictures::PROFILE]));
                                $exists_list[] = $_picture->getKey();
                            }
                        }
                    } else {
                        $_picture = PictureTransformer::prepare($picture, ['user_id' => $musicLover->getKey(), 'type' => UserPictures::PROFILE]);
                        $_picture->save();

                        //Save Order Studio Pic (temp)

                        $orderPic = $_picture->toArray();

                        $order = OrderTransformer::prepare($orderPic, ['user_id' => $musicLover->getKey(), 'item_type' => UserPictures::PROFILE, 'order_number' => '9999']);
                        $order->save();
                    }
                }
                //Get difference of arrays for figure out which pictures have been removed
                $removable_list = array_diff($studio_pictures, $exists_list);
                UserPictures::whereIn('id', $removable_list)->delete();
                Order::whereIn('item_id',$removable_list)->delete();
            }

            if ($request->has('gig_pictures')) {
                $studio_pictures = $musicLover->gig_pictures()->get()->lists(['id'])->toArray();
                $exists_list = [];
                //If picture exists check if picture belongs to user
                //If no then save new picture
                foreach ($request->input('gig_pictures') as $picture) {
                    if (isset($picture['id'])) {
                        $_picture = UserPictures::find($picture['id']);
                        if ($_picture) {
                            if ($musicLover->isOwn($_picture->user_id)) {
                                $_picture->update(array_merge($picture, ['type' => UserPictures::GIG]));
                                $exists_list[] = $_picture->getKey();
                            }
                        }
                    } else {
                        $_picture = PictureTransformer::prepare($picture, ['user_id' => $musicLover->getKey(), 'type' => UserPictures::GIG]);
                        $_picture->save();

                        //Save Order Gig Pic (temp)

                        $orderPic = $_picture->toArray();


                        $order = OrderTransformer::prepare($orderPic, ['user_id' => $musicLover->getKey(), 'item_type' => UserPictures::GIG, 'order_number' => '9999']);
                        $order->save();

                    }
                }
                //Get difference of arrays for figure out which pictures have been removed
                $removable_list = array_diff($studio_pictures, $exists_list);
                UserPictures::whereIn('id', $removable_list)->delete();
                Order::whereIn('item_id',$removable_list)->delete();
            }

            DB::commit();

            return response()->json([
                'status' => 'Successfully updated'
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'error' => $e->getMessage() . '  ' . $e->getLine() //'Something goes wrong',
            ], 422);
        }
    }


    /**
     * @param $id
     */
    public function destroy($id)
    {

    }
}