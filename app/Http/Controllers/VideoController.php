<?php

namespace App\Http\Controllers;

use App\Models\Favorites\FavoriteVideos;
use App\Models\User;
use App\Models\Users\UserVideos;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::auth();
        $user_videos = UserVideos::where('user_id',$user->getKey());
        $video = $user_videos->find($id);
        $faves_video = FavoriteVideos::where('video_id',$video->id);

        if (!$video || $user->getKey() != $video->user_id) {
            return response()->json([
                'error' => 'You have not access to this action'
            ], 404);
        }

        DB::beginTransaction();
        try {
            $faves_video->delete();
            $video->delete();

            DB::commit();

            return response()->json([
                'success' => 'Video successfully deleted'
            ], 200);

        } catch (\Exception $e) {

            DB::rollback();

            return response()->json([
                'error' => 'Something goes wrong!'
            ], 422);
        }
    }
}
