<?php

namespace App\Http\Controllers;

use App\Http\Transformers\Shared\WallTransformer;
use App\Models\Users\UserWall;
use App\Models\Gig\GigWall;
use App\Models\User;
use Illuminate\Http\Request;

class WallController extends Controller
{
    /**
     *@param Request $request
     *
     * @return array
     */
    public function store(Request $request)
    {

        $user = User::auth();
        if (!$user) {
            return response()->json([
                'error' => 'Access Denied'
            ], 403);
        }

        if ($request->input('message') == '' ){
            return response()->json([
                'errors' => ['message' => 'Please fill message']
            ], 422);
        }

        $model = new UserWall;
        $model->sender_user_id =  $user->getKey();
        $model->user_id = $request->input('user_id');
        $model->message = $request->input('message');
        $model->save();

        return response()->json([
            'wall' => WallTransformer::transform($model)
        ], 200);
    }


    /**
     *@param Request $request
     *
     * @return array
     */
    public function gigWall(Request $request)
    {
        $user = User::auth();
        if (!$user) {
            return response()->json([
                'error' => 'Access Denied'
            ], 403);
        }

        if ($request->input('message') == '' ){
            return response()->json([
                'errors' => ['message' => 'Please fill message']
            ], 422);
        }

        $model = new GigWall;
        $model->sender_user_id =  $user->getKey();
        $model->gig_id = $request->input('gig_id');
        $model->message = $request->input('message');
        $model->save();

        return response()->json([
            'wall' => WallTransformer::transform($model)
        ], 200);
    }

    public function destroy($user_id)
    {

    }
}
