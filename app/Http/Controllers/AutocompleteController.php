<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests;
use App\Http\Requests\Filters\Venue\VenueFilterRequest;
use App\Http\Requests\Filters\Artist\ArtistFilterRequest;
use App\Http\Requests\Filters\Search\UserFilterRequest;


class AutocompleteController extends Controller
{
    /**
     * @param VenueFilterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function venues(VenueFilterRequest $request)
    {
        $venues = User::venues($request)->get();

        $results = [];

        foreach ($venues as $venue) {
            $results[] = [
                'model' => $venue->name,
                'label' => $venue->name,
                'value' => $venue->getKey(),
                'avatar' => $venue->avatar ? asset('pics/' . $venue->avatar) : asset('img/no-avatar.png'),
                'site_name'=>$venue->site_name
            ];
        }

        return response()->json([
            'errors' => [],
            'results' => $results
        ], 200);
    }


    /**
     * @param ArtistFilterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function artists(ArtistFilterRequest $request)
    {
        $artists = User::artists($request)->get();

        $results = [];

        foreach ($artists as $artist) {
            $results[] = [
                'model' => $artist->name,
                'label' => $artist->name,
                'value' => $artist->getKey(),
                'avatar' => $artist->avatar ? asset('pics/' . $artist->avatar) : asset('img/no-avatar.png'),
                'site_name'=>$artist->site_name
            ];
        }

        return response()->json([
            'errors' => [],
            'results' => $results
        ], 200);
    }


    public function users(UserFilterRequest $request)
    {
        $users = User::users($request)->paginate(70);

        $results = [];

        foreach ($users as $user) {
            $results[] = [
                'model' => $user->name,
                'avatar' => $user->avatar ? asset('pics/' . $user->avatar) : ($user->fb_user_id ? "https://graph.facebook.com/" . $user->fb_user_id . "/picture?type=large&w‌​idth=500&height=500" : asset('img/no-avatar.png')),
                'value' => $user->name,
                'id' => $user->getKey(),
                'role' => $user->role_id,
                'site_name'=>$user->has('site_name')
            ];
        }

        return response()->json([
            'results' => $results,
        ], 200);
    }

    public function getID($siteName)
    {
        $user = User::where('site_name', '=', $siteName)->first();
        if (!$user) {
            return response()->json([
                'error' => 'This user does not exists'
            ], 403);
        }

        return response()->json([

            'id' => $user->getKey()
        ], 200);
    }


    public function getRoleID($siteName)
    {
        $user = User::where('site_name', '=', $siteName)->first();
        if (!$user) {
            return response()->json([
                'error' => 'This user does not exists'
            ], 403);
        }

        return response()->json([

            'role_id' => $user->role_id
        ], 200);
    }
}
