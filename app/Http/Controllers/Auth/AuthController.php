<?php

namespace App\Http\Controllers\Auth;

use App\Http\Transformers\Shared\UsersNotificationTransformer;
use App\Models\User;
use App\Http\Transformers\Shared\UserTransformer;
use App\Models\Users\UserNotification;
use App\Providers\TGEventsProvider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    private $rules = [
        'email' => 'required|email|max:255',
        'password' => 'required|min:3',
    ];

    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    public function postLogin(Request $request)
    {
        $data = $request->all();

        $v = Validator::make($data, $this->rules);
//        dd($request->all());

        if (!$v->fails()) {
            if (Auth::guard('web')->attempt(['email' => $data['email'], 'password' => $data['password']], false, false)) {
                $user = Auth::guard('web')->getLastAttempted(); // get hold of the user
//                dd($user);
                if (!$user->api_token) {
                    $token = md5(Carbon::now());
                    $user->api_token = $token;
                };

                $user->save();

                if (!$user->notifications->first()) {
                    $notifications = UserNotification::getEventRules($user->role_id, $user->getKey());
                    $user->notifications()->saveMany(UsersNotificationTransformer::prepareCollection($notifications));
                }
                return response()->json([
                    'user' => UserTransformer::transform($user),
                    'api_token' => $user->api_token
                ], 200);
            } else {
                return response()->json(['errors' => ['attemp' => 'Wrong credentials']], 200);
            }

        } else {
            return response()->json(['errors' => $v->messages()->toArray()], 200);
        }
    }

    public function logout(Request $request)
    {

        $user = User::auth();
        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json([
            'success' => 'Logged Out!'
        ], 200);
    }

    public function pusherAuth(Request $request)
    {
        $user = User::auth();
        if ($user) {
            TGEventsProvider::setChannelName($request->get('channel_name'));
            TGEventsProvider::setSocket_id($request->get('socket_id'));
            $pusher = new \Pusher(config('broadcasting.connections.pusher.key'), config('broadcasting.connections.pusher.secret'), config('broadcasting.connections.pusher.app_id'));
            $connection_auth = $pusher->socket_auth($request->get('channel_name'), $request->get('socket_id'));
            echo $connection_auth;
        }
    }
}
