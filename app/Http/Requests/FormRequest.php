<?php
namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;

abstract class FormRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    abstract public function rules();
}