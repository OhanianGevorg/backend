<?php
namespace App\Http\Requests;

abstract class FilterRequest extends Request
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    abstract public function rules();

    abstract public function fields();

    public function filters()
    {
        return parent::only($this->fields());
    }
}