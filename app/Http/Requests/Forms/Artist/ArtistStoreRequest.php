<?php

namespace App\Http\Requests\Forms\Artist;

use App\Http\Requests\FormRequest;

class ArtistStoreRequest extends FormRequest
{
    /**
     * Define validation rules
     *
     * @return array
     */
    public function rules()
    {
        $customRules = [];
        if ($this->has('videos')) {
            foreach ($this->input('videos') as $index => $value) {
                $customRules['videos.' . ($index) . '.url'] = 'url|required_with:videos.' . ($index);
            }
        }

        if ($this->has('reviews')) {
            foreach ($this->input('reviews') as $index => $value) {
                $customRules['reviews.' . ($index) . '.review'] = 'required_with:reviews.' . ($index);
                $customRules['reviews.' . ($index) . '.review_date'] = 'date';
            }
        }

        if ($this->has('gig_pictures')) {
            foreach ($this->input('gig_pictures') as $index => $value) {
                $customRules['gig_pictures.' . ($index) . '.file_id'] = 'required|file_exists:pics';
            }
        }

        if ($this->has('studio_pictures')) {
            foreach ($this->input('studio_pictures') as $index => $value) {
                $customRules['studio_pictures.' . ($index) . '.file_id'] = 'required|file_exists:pics';
            }
        }

        if ($this->has('songs')) {
            foreach ($this->input('songs') as $index => $value) {
                $customRules['songs.' . ($index) . '.song_name'] = 'required_with:songs.' . ($index);
                $customRules['songs.' . ($index) . '.file_id'] = 'file_exists:audios|required_with:songs.' . ($index);
            }
        }

        if ($this->has('genres')) {
            foreach ($this->input('genres') as $index => $value) {
                $customRules['genres.' . ($index) . '.id'] = 'required|exists:genres,id';
            }
        }
        if ($this->has('gig') && $this->has('gig.gig_types')) {
            foreach ($this->input('gig.gig_types') as $index => $value) {
                $customRules['gig.gig_types.' . ($index) . '.id'] = 'required_with:gig.date|exists:gig_system_types,id';
            }
        }

        if ($this->has('gig') && $this->has('gig.artists')) {
            foreach ($this->input('gig.artists') as $index => $value) {
                $customRules['gig.artists.' . ($index) . '.id'] = 'exists_soft_deleted:users,id|required_with:gig.date';
                $customRules['gig.artists.' . ($index) . '.stage_time'] = 'required_with:gig.date|date';
            }
        }

        return array_merge([
            'name' => 'required',
            'address' => 'required',
            'address_details.city' => 'required',
            'address_details.state' => 'required',
            'address_details.country' => 'required',
            'address_details.longitude' => 'required',
            'address_details.latitude' => 'required',
            'about' => 'string',
            'web_site' => array('regex:/^((http|https|ftp):\/\/)?([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i'),
            'contact_email' => 'email',
            'contact_phone' => array('regex:/^[\'+\']{0,1}[^A-Za-z]+$/'),
            'contact_mobile' => array('regex:/^[\'+\']{0,1}[^A-Za-z]+$/'),
            'email' => 'required|email|unique_soft_deleted:users,email',
            'site_name' => 'required|unique_soft_deleted:users,site_name',
            'password' => 'required|confirmed|regex:/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/',

            'genres' => 'required|array',

            'gig.date' => 'date',
            'gig.price' => 'required_with:gig.date|numeric',
            'gig.name' => 'alpha_num',
            'gig.poster.file_id' => 'required_with:gig.date|file_exists:posters',
            'gig.venue_user_id' => 'required_with:gig.date|exists_soft_deleted:users,id',
            'gig.gig_types' => 'required_with:gig.date|array',
            'gig.start_time' => 'required_with:gig.date|date',
            'gig.end_time' => 'date',
            'gig.ticket_seller_site' => array('regex:/^((http|https|ftp):\/\/)?([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i')

        ], $customRules);
    }


    /**
     * Define validation messages
     *
     * @return array
     */
    public function messages()
    {
        $customMessages = [];

        if ($this->has('videos')) {
            foreach ($this->input('videos') as $index => $value) {
                $customMessages['videos.' . ($index) . '.url.required_with'] = 'The video url is required';
            }
        }

        if ($this->has('reviews')) {
            foreach ($this->input('reviews') as $index => $value) {
                $customMessages['reviews.' . ($index) . '.review.required_with'] = 'The review text is required';
                $customMessages['reviews.' . ($index) . '.review_date.date'] = 'The review date is not valid';
            }
        }

        if ($this->has('songs')) {
            foreach ($this->input('songs') as $index => $value) {
                $customMessages['songs.' . ($index) . '.song_name.required_with'] = 'The Song Name is required';
            }
        }

        if ($this->has('genres')) {
            foreach ($this->input('genres') as $index => $value) {
                $customMessages['genres.' . ($index) . '.id.required'] = 'The Genre is required';
                $customMessages['genres.' . ($index) . '.id.exists'] = 'The Genre should exists in the system';
            }
        }

        if ($this->has('gig_pictures')) {
            foreach ($this->input('gig_pictures') as $index => $value) {
                $customMessages['gig_pictures.' . ($index) . '.file_id.required'] = 'The File can not be empty';
                $customMessages['gig_pictures.' . ($index) . '.file_id.exists'] = 'The File not exist in the system';
            }
        }

        if ($this->has('studio_pictures')) {
            foreach ($this->input('studio_pictures') as $index => $value) {
                $customMessages['studio_pictures.' . ($index) . '.file_id.required'] = 'The File can not be empty';
                $customMessages['studio_pictures.' . ($index) . '.file_id.exists'] = 'The File not exist in the system';
            }
        }

        if ($this->has('gig') && $this->has('gig.artists')) {
            foreach ($this->input('gig.artists') as $index => $value) {
                $customMessages['gig.artists.' . ($index) . '.id.exists'] = 'The Artist should exists in the system';
                $customMessages['gig.artists.' . ($index) . '.id.required_with'] = 'Artist required for creating gig';
                $customMessages['gig.artists.' . ($index) . '.stage_time.date'] = 'Stage Time should be valid date';
            }
        }

        if ($this->has('gig') && $this->has('gig.gig_types')) {
            foreach ($this->input('gig.gig_types') as $index => $value) {
                $customMessages['gig.gig_types.' . ($index) . '.id.exists'] = 'The Type should exists in the system';
                $customMessages['gig.gig_types.' . ($index) . '.id.required_with'] = 'At last one Type is require for creating gig';

            }
        }

        return array_merge([
            'name.required' => 'The Name is required',
            'address.required' => 'The City is required',
            'address_details.city.required' => 'The City is required',
            'address_details.state.required' => 'The State is required',
            'address_details.country.required' => 'The Country is required',
            'web_site.regex' => 'The Web Site name is not valid',
            'contact_email.email' => 'The Email must be valid email',
            'contact_phone.regex' => 'The Contact phone must not contain letters',
            'contact_mobile.regex' => 'The Mobile phone must not contain letters',
            'about.string' => 'The about must be string',
            'email.required' => 'The Email is required',
            'email.email' => 'The Email must be valid email',
            'email.unique_soft_deleted' => 'The Email already taken',
            'site_name.required' => 'The Site Name is required',
            'site_name.unique_soft_deleted' => 'The Site Name is already taken',
            'password.required' => 'The Password is required',
            'password.confirmed' => 'The Password confirmation does not match',
            'password.regex' => 'Password should  be at least 6 characters, contain at least 1 uppercase letter, at least 1 lowercase letter and numeric characters',

            'genres.required' => 'The Genres is required',

            'gig.price.required_with' => 'The Price is required',
            'gig.date.date' => 'The Gig Date should be valid date',
            'gig.name.alpha_num' => 'Name can not contain only numbers',
            'gig.name.string' => 'The Gig name can not contains only numbers',
            'gig.poster.file_id.required_with' => 'The Poster is required',
            'gig.gig_types.required_with' => 'At least one Gig type',
            'gig.start_time.required_with' => 'The Start time is required',
            'gig.start_time.date' => 'The Start time should be valid date',
            'gig.venue_user_id.required_with' => 'The Venue is required',
            'gig.venue_user_id.exists' => 'The Venue should exists in the system',
            'gig.artists.required_with' => 'Artist required for creating gig.',
            'gig.ticket_seller_site.regex' => 'Site should have valid url'
        ], $customMessages);
    }
}