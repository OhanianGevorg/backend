<?php
namespace App\Http\Requests\Forms\Venue;

use App\Http\Requests\FormRequest;

class VenueUpdateRequest extends FormRequest
{
    /**
     * Define validation rules
     *
     * @return array
     */
    public function rules()
    {
        $customRules = [];
        if ($this->has('videos')) {
            foreach ($this->input('videos') as $index => $value) {
                $customRules['videos.' . ($index) . '.url'] = 'url|required_with:videos.' . ($index);
            }
        }

        if ($this->has('reviews')) {
            foreach ($this->input('reviews') as $index => $value) {
                $customRules['reviews.' . ($index) . '.review'] = 'required_with:reviews.' . ($index);
                $customRules['reviews.' . ($index) . '.review_date'] = 'date';
            }
        }

        if ($this->has('gig_pictures')) {
            foreach ($this->input('gig_pictures') as $index => $value) {
                $customRules['gig_pictures.' . ($index) . '.file_id'] = 'required|file_exists:pics';
            }
        }

        if ($this->has('venue_pictures')) {
            foreach ($this->input('venue_pictures') as $index => $value) {
                $customRules['venue_pictures.' . ($index) . '.file_id'] = 'required|file_exists:pics';
            }
        }

        if ($this->has('genres')) {
            foreach ($this->input('genres') as $index => $value) {
                $customRules['genres.' . ($index) . '.id'] = 'required|exists:genres,id';
            }
        }

        return array_merge([
            'name'=>'required|max:255',
            'address' => 'required',
            'about'=>'string',
            'address_details.city' => 'required',
            'address_details.state' => 'required',
            'address_details.country' => 'required',
            'address_details.latitude'=>'required',
            'address_details.longitude'=>'required',
            'web_site' => array('regex:/^((http|https|ftp):\/\/)?([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i'),
            'other_web_site' => array('regex:/^((http|https|ftp):\/\/)?([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i'),
            'contact_email' => 'email',
            'contact_phone' => array('regex:/^[\'+\']{0,1}[^A-Za-z]+$/'),
            'contact_mobile' => array('regex:/^[\'+\']{0,1}[^A-Za-z]+$/'),
            'genres' => 'required|array'
        ], $customRules);
    }


    /**
     * Define validation messages
     *
     * @return array
     */
    public function messages()
    {
        $customMessages = [];
        if ($this->has('genres')) {
            foreach ($this->input('genres') as $index => $value) {
                $customMessages['genres.' . ($index) . '.id.required'] = 'The Genre is required';
                $customMessages['genres.' . ($index) . '.id.exists'] = 'The Genre should exists in the system';
            }
        }

        $customMessages = [];
        if ($this->has('gig_pictures')) {
            foreach ($this->input('gig_pictures') as $index => $value) {
                $customMessages['gig_pictures.' . ($index) . '.file_id.required'] = 'The File can not be empty';
                $customMessages['gig_pictures.' . ($index) . '.file_id.exists'] = 'The File not exist in the system';
            }
        }

        if ($this->has('videos')) {
            foreach ($this->input('videos') as $index => $value) {
                $customMessages['videos.' . ($index) . '.url.required_with'] = 'The video url is required';
            }
        }

        if ($this->has('reviews')) {
            foreach ($this->input('reviews') as $index => $value) {
                $customMessages['reviews.' . ($index) . '.review.required_with'] = 'The review text is required';
                $customMessages['reviews.' . ($index) . '.review_date.date'] = 'The review date is not valid';
            }
        }

        if ($this->has('venue_pictures')) {
            foreach ($this->input('venue_pictures') as $index => $value) {
                $customMessages['venue_pictures.' . ($index) . '.file_id.required'] = 'The File can not be empty';
                $customMessages['venue_pictures.' . ($index) . '.file_id.exists'] = 'The File not exist in the system';
            }
        }

        return array_merge([
            'name.required'=>'Name is required',
            'address.required' => 'The City is required',
            'about.string'=>'The about must be string',
            'address_details.city.required' => 'The City is required',
            'address_details.state.required' => 'The State is required',
            'address_details.country.required' => 'The Country is required',
            'web_site.regex' => 'The Web Site name is not valid',
            'other_web_site.regex' => 'The Web Site name is not valid',
            'contact_email.email' => 'The Email must be valid email',
            'contact_phone.numeric' => 'The Contact phone must be numeric',
            'contact_mobile.numeric' => 'The Mobile phone must be numeric',
            'genres.required' => 'The Genres is required'
        ],$customMessages);
    }
}