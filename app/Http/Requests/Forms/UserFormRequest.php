<?php
namespace App\Http\Requests\Forms;

use App\Http\Requests\FormRequest;

class UserFormRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email'     => 'required|unique:users,email|email|max:255',
            'role_id'=>'required|max:255',
            'store_name' => 'unique:users,store_name|required_if:role_id,2|max:255',
            'name' => 'required_if:role_id,1|max:255',
            'surname' => 'required_if:role_id,1|max:255',
            'password'  => 'required|confirmed',
        ];
    }

    public function messages()
    {
        return [
          'email.required'=>"The email is required",
          'email.unique'=>"This email is already exists",
          'email.email'=>"The Email must be valid email",
          'store_name.required_if'=>"The Store Name is required",
          'store_name.unique'=>"This Store Name is already exists",
          'name.required_if'=>"The name is required",
          'surname.required_if'=>"The surname is required",
          'password.required'=>"The password is required",
          'password.confirmed'=>"The password confirmation does not match",
        ];
    }
}