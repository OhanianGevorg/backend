<?php
namespace App\Http\Requests\Forms\Gig;

use App\Http\Requests\FormRequest;

class GigStoreRequest extends FormRequest
{
    public function rules()
    {


        $custom_rules =[];

        if($this->has('artists')){
            foreach ($this->input('artists') as $index=>$value){
                $custom_rules['artists.'.($index).'.id']='required|exists_soft_deleted:users,id';
            }
        }

        if ($this->has('gig_types')) {
            foreach ($this->input('gig_types') as $index => $value) {
                $customRules['gig_types.' . ($index) . '.id'] = 'required_with:gig.date|exists:gig_system_types,id';
            }
        }

        return array_merge([
            'date' => 'required',
            'description' => 'string',
            'poster.file_id' => 'required|file_exists:posters',
            'venue_user_id' => 'required|integer',
            'name' => 'string',
            'ages' => 'string',
            'gig_types' => 'required|array',
            'price' => 'required|integer',
            'start_time' => 'required|date',
            'end_time' => 'date',
            'artists' => 'required|array',
            'artists.0.stage_time'=>'required|date',
            'ticket_seller_site'=>array('regex:/^((http|https|ftp):\/\/)?([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i')
        ],$custom_rules);

    }


    public function messages()
    {
        $customMessages = [];

        if ($this->has('artists')) {
            foreach ($this->input('artists') as $index => $value) {
                $customMessages['artists.' . ($index) . '.id.exists_soft_deleted'] = 'This artist does not exist';
            }
        }
        if ($this->has('gig_types')) {
            foreach ($this->input('gig_types') as $index => $value) {
                $customMessages['gig_types.' . ($index) . '.id.exists'] = 'The Type should exists in the system';
                $customMessages['gig_types.' . ($index) . '.id.required_with'] = 'At last one Type is require for creating gig';

            }
        }
        return array_merge([
            'price.required' => 'The Price is required',
            'date.date' => 'The Gig Date should be valid date',
            'poster.file_id.required' => 'The Poster is required',
//            'gig_types.required' => 'At least one Gig type',
            'start_time.required' => 'The Start time is required',
            'start_time.date' => 'The Start time should be valid date',
            'venue_user_id.required' => 'The Venue is required',
            'venue_user_id.exists' => 'The Venue should exists in the system',
            'artists.required' => 'Artist required for creating gig.',
            'artists.0.stage.required'=>'First Artist stage time is required',
            'ticket_seller_site.regex'=>'Site should have valid url'
        ], $customMessages);
    }
}