<?php

namespace App\Http\Requests\Forms\Gig;

use App\Http\Requests\FormRequest;

class GigUpdateRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $custom_rules = [];

        if ($this->has('types')) {
            foreach ($this->input('types') as $index => $value) {
                $custom_rules['types.' . ($index) . '.id'] = 'required|exists:gig_system_types,id';
            }
        }
        if ($this->has('artists')) {
            foreach ($this->input('artists') as $index => $value) {
                $custom_rules['artists.' . ($index) . '.id'] = 'required|exists_soft_deleted:users,id';
            }
        }

        return array_merge([
            'date' => 'required|date',
            'description' => 'string',
            'posterFull.file_id' => 'required|file_exists:posters',
            'venue_user_id' => 'required|integer',
            'name' => 'string',
            'ages' => 'string',
            'types' => 'required|array',
            'price' => 'required|integer',
            'start' => 'required|date',
            'end' => 'date',
            'artists'=>'required|array',
            'artists.0.stage_time'=>'required|date',
            'seller_site' => array('regex:/^((http|https|ftp):\/\/)?([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i')
        ], $custom_rules);

    }

    /**
     * Define validation messages
     *
     * @return array
     */
    public function messages()
    {
        $customMessages = [];

        if ($this->has('types')) {
            foreach ($this->input('types') as $index => $value) {
                $customMessages['types.' . ($index) . '.id.required'] = "Type is required";
//                $customMessages['types.' . ($index) . '.name.string'] = "Type must be string";
                $customMessages['types.' . ($index) . '.id.exists'] = 'The Type should exists in the system';
            }
        }
        if ($this->has('artists')) {
            foreach ($this->input('artists') as $index => $value) {
                $customMessages['artists.' . ($index) . '.id.exists_soft_deleted'] = 'This artist does not exist';
            }
        }

        return array_merge([
            'date.required' => 'The date is required',
            'description.string' => 'Description should contain only letters',
            'posterFull.file_id.required' => 'Poster is required',
            'venue_user_id' => 'Venue is required',
            'name.string' => 'The name should contain only letters',
            'ages.string' => 'The name should contain only letters',
            'price.required' => 'The price is required',
            'start_time.required' => 'The start time is required',
            'artists.required' => 'Artist required for creating gig.If the artist is not in the system please ask them to create an Artist Profile.',
            'artists.0.stage_time.required'=>'Artist stage time is required',
            'seller_site.regex' => 'Site name is not valid site name',
//            'types'=>'required|array'

        ], $customMessages);
    }
}
