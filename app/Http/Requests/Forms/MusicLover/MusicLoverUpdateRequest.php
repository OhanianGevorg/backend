<?php

namespace App\Http\Requests\Forms\MusicLover;

use App\Http\Requests\FormRequest;

class MusicLoverUpdateRequest extends FormRequest
{
    /**
     * Define validation rules
     *
     * @return array
     */
    public function rules()
    {
        $customRules = [];
        if ($this->has('gig_pictures')) {
            foreach ($this->input('gig_pictures') as $index => $value) {
                $customRules['gig_pictures.' . ($index) . '.file_id'] = 'required|file_exists:pics';
            }
        }

        if ($this->has('profile_pictures')) {
            foreach ($this->input('profile_pictures') as $index => $value) {
                $customRules['profile_pictures.' . ($index) . '.file_id'] = 'required|file_exists:pics';
            }
        }

        if ($this->has('genres')) {
            foreach ($this->input('genres') as $index => $value) {
                $customRules['genres.' . ($index) . '.id'] = 'required|exists:genres,id';
            }
        }

        return array_merge([
            'name' => 'required|string',
            'contact_email' => 'email',
            'contact_phone' => array('regex:/^[\'+\']{0,1}[^A-Za-z]+$/'),
            'contact_mobile' => array('regex:/^[\'+\']{0,1}[^A-Za-z]+$/'),
            'genres' => 'array'
        ], $customRules);
    }


    /**
     * Define validation messages
     *
     * @return array
     */
    public function messages()
    {
        $customMessages = [];
        if ($this->has('genres')) {
            foreach ($this->input('genres') as $index => $value) {
                $customMessages['genres.' . ($index) . '.id.required'] = 'The Genre is required';
                $customMessages['genres.' . ($index) . '.id.exists'] = 'The Genre should exists in the system';
            }
        }

        if ($this->has('gig_pictures')) {
            foreach ($this->input('gig_pictures') as $index => $value) {
                $customMessages['gig_pictures.' . ($index) . '.file_id.required'] = 'The File can not be empty';
                $customMessages['gig_pictures.' . ($index) . '.file_id.exists'] = 'The File not exist in the system';
            }
        }

        if ($this->has('studio_pictures')) {
            foreach ($this->input('studio_pictures') as $index => $value) {
                $customMessages['studio_pictures.' . ($index) . '.file_id.required'] = 'The File can not be empty';
                $customMessages['studio_pictures.' . ($index) . '.file_id.exists'] = 'The File not exist in the system';
            }
        }

        return array_merge([
            'name.required' => 'The Name is required',
            'contact_email.email' => 'The Email must be valid email',
            'contact_phone.regex' => 'The Contact phone must not contain letters',
            'contact_mobile.regex' => 'The Mobile phone must not contain letters',
            'genres.required' => 'The Genres is required'
        ], $customMessages);
    }
}