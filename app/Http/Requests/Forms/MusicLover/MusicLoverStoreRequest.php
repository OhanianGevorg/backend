<?php

namespace App\Http\Requests\Forms\MusicLover;

use App\Http\Requests\FormRequest;

class MusicLoverStoreRequest extends FormRequest
{
    /**
     * Define validation rules
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique_soft_deleted:users,email',
            'password' => 'required|confirmed|regex:/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/'
        ];
    }


    /**
     * Define validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The Name is required',
            'email.required' => 'The Email is required',
            'email.email' => 'The Email must be valid email',
            'email.unique_soft_deleted' => 'The Email already taken',
            'password.required' => 'The Password is required',
            'password.confirmed' => 'The Password confirmation does not match',
            'password.regex' => 'Password should  be at least 6 characters, contain at least 1 uppercase letter, at least 1 lowercase letter and numeric characters'
        ];
    }
}