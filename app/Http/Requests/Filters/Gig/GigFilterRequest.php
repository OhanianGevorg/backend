<?php
namespace App\Http\Requests\Filters\Gig;

use App\Enums\DateIntervals;
use App\Http\Requests\FilterRequest;

class GigFilterRequest extends FilterRequest
{
    /**
     * Define fields for filter
     *
     * @return array
     */
    public function fields()
    {
        return [
            'timezone',
            'date',
            'location',
            'genres',
            'types',
            'minPrice',
            'maxPrice',
            'radius',
            'favorite',
            'rangeDate',
            'ages'
        ];
    }


    /**
     * Define rules for filter
     *
     * @return array
     */
    public function rules()
    {

        return [
//            'date' => 'array_or_rule:' . implode(',', DateIntervals::getAll()) . ',or_date',
            'city' => 'string',
            'state' => 'string',
            'country' => 'string',
            'genre' => 'string',
            'types' => 'string',
            'minPrice' => 'integer',
            'maxPrice' => 'integer',
            'favorite' => 'string',
            'rangeDate' => 'array',
            'ages' => 'string',
        ];
    }

    /**
     * Define rules for filter
     *
     * @return array
     */
    public function messages()
    {
        return [
            'date.array_or_rule' => 'Not valid date',
        ];
    }
}