<?php

namespace App\Http\Requests\Filters\Statistic;

use App\Http\Requests\FilterRequest;

class StatisticFilterRequest extends FilterRequest
{
    /**
     * Define fields for filter
     *
     * @return array
     */
    public function fields()
    {
        return [
//            'id',
            'time',
            'type',
        ];
    }

    /**
     * Define rules for filter
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'id' => 'integer',
            'time' => 'string',
            'type' => 'string',
        ];
    }
}