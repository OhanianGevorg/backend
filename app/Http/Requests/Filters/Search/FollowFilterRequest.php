<?php

namespace App\Http\Requests\Filters\Search;

use App\Http\Requests\FilterRequest;
use App\Http\Requests\Request;

class FollowFilterRequest extends FilterRequest
{
    public function fields()
    {
        return [
            'type',
            'name'
        ];
    }

    /**
     * Define rules for filter
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'type'=>'string|required'
        ];
    }
}
