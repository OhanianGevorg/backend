<?php

namespace App\Http\Requests\Filters\Search;

use App\Http\Requests\FilterRequest;
use App\Enums\DateIntervals;

class UserFilterRequest extends FilterRequest
{
    /**
     * Define fields for filter
     *
     * @return array
     */
    public function fields()
    {
        return [
            'name',
        ];
    }

    /**
     * Define rules for filter
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
        ];
    }
}