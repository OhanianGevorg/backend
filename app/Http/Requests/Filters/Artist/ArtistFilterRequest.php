<?php

namespace App\Http\Requests\Filters\Artist;

use App\Http\Requests\FilterRequest;
use App\Enums\DateIntervals;

class ArtistFilterRequest extends FilterRequest
{
    /**
     * Define fields for filter
     *
     * @return array
     */
    public function fields()
    {
        return [
            'timezone',
            'name',
            'firstInitial',
            'date',
            'genres',
            'type',
            'types',
            'maxPrice',
            'minPrice',
            'location',
        ];
    }

    /**
     * Define rules for filter
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'firstInitial' => 'string',
            'date' => 'array_or_rule:' . implode(',', DateIntervals::getAll()) . ',or_date',
            'genres' => 'string',
            'type' => 'string|in:my_favorites,friends_favorites',
            'minPrice' => 'integer',
            'maxPrice' => 'integer',
        ];
    }
}