<?php
namespace App\Http\Requests\Filters\Venue;

use App\Http\Requests\FilterRequest;
use App\Enums\DateIntervals;

class VenueFilterRequest extends FilterRequest
{
    /**
     * Define fields for filter
     *
     * @return array
     */
    public function fields()
    {
        return [
            'timezone',
            'name',
            'firstInitial',
            'date',
            'location',
            'genres',
            'type',
            'types',
            'maxPrice',
            'minPrice',
        ];
    }


    /**
     * Define rules for filter
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string',
            'firstInitial' => 'string',
            'date' => 'array_or_rule:' . implode(',', DateIntervals::getAll()) . ',or_date',
            'genres' => 'string',
            'type' => 'string|in:my_favorites,friends_favorites',
            'minPrice' => 'integer',
            'maxPrice' => 'integer',
        ];
    }
}