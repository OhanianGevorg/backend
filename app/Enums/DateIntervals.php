<?php
namespace App\Enums;

use App\Libraries\Enum;

class DateIntervals extends Enum
{
    const TODAY = 'today';
    const TONIGHT = 'tonight';
    const WEEKEND = 'weekend';
    const WEEK = 'week';
    const MONTH = 'month';
}