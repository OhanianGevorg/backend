<?php
namespace App\Libraries;

use Exception;
use ReflectionClass;

abstract class Enum
{
    /**
     * @param string $str
     * @return bool
     */
    public static function has($str = '')
    {
        return in_array($str, array_values(static::getAll()));
    }

    /**
     * @return array
     */
    public static function getAll()
    {
        return (new ReflectionClass(static::class))->getConstants();
    }
}
