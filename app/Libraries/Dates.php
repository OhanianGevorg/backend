<?php
/**
 * Created by PhpStorm.
 * User: Zvezdan
 * Date: 15.06.2016
 * Time: 4:06
 */

namespace App\Libraries;

class Dates
{
    public static function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp) && ($timestamp <= PHP_INT_MAX) && ($timestamp >= ~PHP_INT_MAX);
    }

    public static function isValidTime($time)
    {
        return preg_match("/(1[012]|0[0-9]):([0-5][0-9])/", self::getTimeString($time));
    }

    public static function getTimeString($time)
    {
        $time_string = '';

        if(isset($time['hour'])){
            $time_string .= $time['hour'];
        }
        if(isset($time['minute'])){
            $time_string .= ':'.$time['minute'];
        }
        if(isset($time['format'])){
            $time_string .= ' '.$time['format'];
        }

        return $time_string;
    }
}