<?php

namespace App\Listeners;

use App\Events\GigStarringNotification;
use App\Events\ViewNotification;
use App\Http\Transformers\Notifications\GigStarringNotificationTransformer;
use App\Models\User;
use App\Models\Users\Notification;
use App\Models\Users\UserNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class GigStarringNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */

    private $favorite;

    public function __construct()
    {

    }


    /**
     * Handle the event.
     *
     * @param  GigStarringNotification $event
     * @return void
     */
    public function handle(GigStarringNotification $event)
    {

        $this->favorite = $event->getFavorite();
        $this->sendNotification($event->getModels());
    }

    private function sendNotification($models)
    {
        $pusher = new \Pusher(config('broadcasting.connections.pusher.key'), config('broadcasting.connections.pusher.secret'), config('broadcasting.connections.pusher.app_id'));

        if ($pusher) {

            $models->each(function ($model) use ($pusher) {

                $channelName = 'private-test-channel' . $model->user_id;
                $notification = $this->saveNotification($model,$this->favorite);
                $data = GigStarringNotificationTransformer::transform($notification);
                $pusher->trigger($channelName, UserNotification::GIG_STARING_NOTIFICATION, ['user' => $data]);
            });
        }
    }

    private function saveNotification($notificationModel,$favoriteModel)
    {

        $userNotification = $favoriteModel->user->getNotificationByType(UserNotification::GIG_STARING_NOTIFICATION);
        $data['notifier_user_id'] = $favoriteModel->user_id;
        $data['notified_user_id'] = $notificationModel->user_id;
        $data['users_notification_id'] = $userNotification->getKey();
        $data['event_type'] = $userNotification->getNotificationType();
        $data['action_id'] = $favoriteModel->gig_id;
        $notification = new Notification($data);
        $notification->save();
        return $notification;
    }
}
