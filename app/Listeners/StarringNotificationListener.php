<?php

namespace App\Listeners;

use App\Events\StarringNotification;
use App\Events\ViewNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class StarringNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Handle the event.
     *
     * @param  StarringNotification  $event
     * @return void
     */
    public function handle(StarringNotification $event)
    {
        //
//        dd($event);
    }
}
