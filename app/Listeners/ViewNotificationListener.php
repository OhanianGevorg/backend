<?php

namespace App\Listeners;

use App\Events\ViewNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class ViewNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Handle the event.
     *
     * @param  ViewNotification  $event
     * @return void
     */
    public function handle(ViewNotification $event)
    {
        //
//        dd($event);
    }
}
