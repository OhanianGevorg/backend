<?php

namespace App\Console\Commands;

use App\Http\Transformers\Gig\GigArtistTransformer;
use App\Http\Transformers\Gig\GigTransformer;
use App\Models\Gig\GigArtist;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use DB;
use League\Flysystem\Exception;

class CreateGigs extends Command
{
    private $fields = [
        'venue_user_id' => '',
        'user_id' => '',
        'start_time' => '',
        'price' => 20,
        'name' => '',
        'ages' => '18+',
        'poster.file_id' => '57ab372bc2102.jpg'
    ];

    private $gigArtistFields = [
        'id' => '',
        'gig_id' => '',
        'stage_time' => ''
    ];

    private $gigName = '';

    private $count = 0;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:gigs {--count=} {--eventName=?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('eventName') !== '?') {
            $this->gigName = $this->option('eventName');
        }

        $this->checkCount((int)$this->option('count'));
        $this->createGigs();
        $this->line('Gigs are successfully added');

    }

    private function checkCount($count)
    {
        if ($count == 0) {
            $count = $this->ask('How many gigs you want create?(set number greater than zero)');
            $this->checkCount($count);
        } else {
            $this->count = $count;
        }
    }

    private function getUser()
    {
        return User::where('role_id', '<>', User::MUSIC_LOVER)->pluck('id')->random(1);
    }

    private function getVenueUser()
    {
        return User::where('role_id', '=', User::VENUE)->pluck('id')->random(1);
    }

    private function getArtist()
    {
        return User::where('role_id', '=', User::ARTIST)->pluck('id')->random(1);
    }

    private function setDate($count)
    {
        return Carbon::now()->addDay($count);
    }

    private function setStageTime($count)
    {
        return $this->setDate($count)->addHour($count);
    }

    private function createGigArtists()
    {
        $gigArtist = GigArtistTransformer::prepare($this->gigArtistFields);

        $gigArtist->save();
    }

    private function createGigs()
    {

        for ($i = 0; $i < $this->count; $i++) {

            DB::beginTransaction();

            try {
                $this->fields['venue_user_id'] = $this->getVenueUser();
                $this->fields['start_time'] = $this->setDate($i);

                if ($this->gigName) {
                    $this->fields['name'] = $this->gigName;
                }

                $gig = GigTransformer::prepare($this->fields, ['user_id' => $this->getUser()]);
                $gig->save();

                $this->gigArtistFields['id'] = $this->getArtist();
                $this->gigArtistFields['gig_id'] = $gig->getKey();
                $this->gigArtistFields['stage_time'] = $this->setStageTime($i);

                $this->createGigArtists();

                DB::commit();

            } catch (\Exception $e) {

                DB::rollback();

                $this->error($e->getMessage());
            }

        }
    }
}
