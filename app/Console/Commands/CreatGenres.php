<?php

namespace App\Console\Commands;

use App\Models\Genre\Genre;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CreatGenres extends Command
{
    private $genres = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:genres {--genres=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create genres';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $options = $this->option('genres');
        $this->checkGenres($options);

    }

    private function checkGenres($genresList)
    {
        if (!$genresList) {
            $options = $this->ask('Set at last one genre. More that one divide with comma');
            $this->checkGenres($options);
        } else {
            $this->genres = explode(',', $genresList);
            $this->insertGenres($this->genres);
        }
    }

    private function insertGenres($genres)
    {
        try {
            Genre::insert(array_map(function ($genre) {
                return [
                    'name' => $genre,
                    'created_at' => Carbon::now()
                ];
            }, $genres));
            $this->line('Genres are successfully created');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
