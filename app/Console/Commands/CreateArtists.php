<?php

namespace App\Console\Commands;

use App\Models\Genre\Genre;
use App\Models\User;
use App\Models\Users\UserGenres;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Transformers\Venue\VenueTransformer;
use App\Http\Transformers\MusicLover\MusicLoverTransformer;
use App\Http\Transformers\Artist\ArtistTransformer;
use DB;

class CreateArtists extends Command
{
    private $count = 0;
    private $userName = '';
    private $userType = '';
    private $entity = [
        'artist' => [
            'transformer' => 'App\Http\Transformers\Artist\ArtistTransformer',
            'fields' => [
                'email' => '',
                'site_name' => '',
                'password' => '',
                'name' => '',
                'address_details.city' => 'Melbourne',
                'address_details.state' => 'Victoria',
                'address_details.country' => 'Australia',
                'address_details.latitude' =>  -37.813611,
                'address_details.longitude' => 144.963056,
                'address' => 'Melbourne, Victoria, Australia',
            ],
            'hasGenre' => true
        ],
        'venue' => [
            'transformer' => 'App\Http\Transformers\Venue\VenueTransformer',
            'fields' => [
                'email' => '',
                'site_name' => '',
                'password' => '',
                'name' => '',
                'address_details.city' => 'Melbourne',
                'address_details.state' => 'Victoria',
                'address_details.country' => 'Australia',
                'address_details.latitude' =>  -37.813611,
                'address_details.longitude' => 144.963056,
                'address' => 'Melbourne, Victoria, Australia',
            ],
            'hasGenre' => true
        ],
        'musiclover' => [
            'transformer' => 'App\Http\Transformers\MusicLover\MusicLoverTransformer',
            'fields' => [
                'email' => '',
                'password' => '',
                'name' => ''
            ],
            'hasGenre' => false
        ]
    ];
    private $userTypes = [
        'artist',
        'venue',
        'musiclover'
    ];
    private $password = 'asdASD123';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:users {--count= :set artists count}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */


    public function handle()
    {
        $this->checkCount((int)$this->option('count'));

        $this->userType = $this->ask('Set user type: artist, venue, or musiclover');
        $this->checkUserType($this->userType);

        $this->userName = $this->ask('Set just one user name. Created user name contain name + count');
        $count = $this->selectUser($this->userName);
        $this->checkUserName($count);


    }

    private function checkCount($count)
    {
        if ($count == 0) {
            $count = $this->ask('How many users you want create?(set number greater than zero)');
            $this->checkCount($count);
        } else {
            $this->count = $count;
        }
    }

    private function checkUserType($userType)
    {
        if (!in_array($userType, $this->userTypes)) {
            $type = $this->ask('Set right user type: artist, venue, or musiclover');
            $this->checkUserType($type);
        } else {
            $this->userType = $userType;
        }
    }

    private function checkUserName($namesCount)
    {
        if ($namesCount > 0) {
            $this->userName = $this->ask('User with the given name is already exists.Choose another name');
            $count = $this->selectUser($this->userName);
            $this->checkUserName($count);
        } else {
            $this->insertUsers($this->userName);
            $this->line('Users are successfully created Your password is asdASD123');
        }
    }

    private function selectUser($name)
    {
        $count = User::where('name', 'RLike', $name . '*[0-9]')->orWhere('name', '=', $name)->count();
        return $count;
    }

    private function insertUsers($name)
    {
        if ($this->count > 0) {

            DB::beginTransaction();
            try {

                for ($i = 0; $i < $this->count; $i++) {

                    $this->entity[$this->userType]['fields']['email'] = $name . $i . '@gmail.com';
                    $this->entity[$this->userType]['fields']['name'] = $name.$i;
                    $this->entity[$this->userType]['fields']['password'] = $this->password;

                    if (array_key_exists('site_name', $this->entity[$this->userType]['fields'])) {
                        $this->entity[$this->userType]['fields']['site_name'] = $name . $i;
                    }

                    $transformer = $this->entity[$this->userType]['transformer'];
                    $user = $transformer::prepare($this->entity[$this->userType]['fields']);
//                    $user->activated = 'true';
                    $user->save();

                    if ($this->entity[$this->userType]['hasGenre']) {

                        $genres = Genre::select('genres.id')->get()->pluck('id')->random(2)->toArray();

                        UserGenres::insert(array_map(function ($genre) use ($user) {
                            return [
                                'genre_id' => $genre,
                                'user_id' => $user->getKey(),
                                'created_at' => Carbon::now()
                            ];
                        }, $genres));
                    }

                    DB::commit();
                }

            } catch (\Exception $e) {

                DB::rollback();
                $this->error($e->getMessage());
            }
        }
    }
}
