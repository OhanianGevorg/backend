<?php

namespace App\Console\Commands;

use App\Models\Gig\GigSystemTypes;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CreateGigSystemTypes extends Command
{
    private $types = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:gigtypes {--types=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Gig System Types';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $options = $this->option('types');
        $this->checkGenres($options);
    }

    private function checkGenres($genresList)
    {
        if (!$genresList) {
            $options = $this->ask('Set at last one type. More that one divide with comma.');
            $this->checkGenres($options);
        } else {
            $this->types = explode(',', $genresList);
            $this->insertGenres($this->types);
        }
    }

    private function insertGenres($genres)
    {
        try {
            GigSystemTypes::insert(array_map(function ($genre) {
                return [
                    'name' => $genre,
                    'created_at' => Carbon::now()
                ];
            }, $genres));
            $this->line('Types are successfully created');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }
}
