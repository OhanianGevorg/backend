<?php

namespace App\Events;

use App\Http\Transformers\Notifications\StarringNotificationTransformer;
use App\Models\Favorites\FavoriteUsers;
use App\Models\User;
use App\Models\Users\Notification;
use App\Models\Users\UserNotification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StarringNotification extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    private $favorite;

    public function __construct(FavoriteUsers $model)
    {
        $this->favorite = $model;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [
            'private-test-channel'.$this->favorite->entity_user_id
        ];
    }

//
    public function broadcastWith()
    {
        $notification = $this->saveNotification();

        return [
            'user' => StarringNotificationTransformer::transform($notification)
        ];
    }

    public function broadcastAs()
    {

        return UserNotification::STARING_NOTIFICATION;
    }

    private function saveNotification()
    {

        $userNotification = $this->favorite->user->getNotificationByType(UserNotification::STARING_NOTIFICATION);
        $data['notifier_user_id'] = User::auth()->getKey();
        $data['notified_user_id'] = $this->favorite->entity_user_id;
        $data['users_notification_id'] = $userNotification->getKey();
        $data['event_type'] = $userNotification->getNotificationType();


        $notification = new Notification($data);
        $notification->save();

        return $notification;
    }

}
