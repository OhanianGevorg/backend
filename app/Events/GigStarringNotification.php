<?php

namespace App\Events;

use App\Http\Transformers\Gig\FavoritGigNotificationTransformer;
use App\Http\Transformers\Notifications\GigStarringNotificationTransformer;
use App\Models\Favorites\FavoriteProducts;
use App\Models\User;
use App\Models\Users\Notification;
use App\Models\Users\UserNotification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GigStarringNotification extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    private $favorite;

    public function __construct(FavoriteProducts $model)
    {
        $this->favorite = $model;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */


    public function broadcastAs()
    {
        return UserNotification::GIG_STARING_NOTIFICATION;
    }


    public function getModels()
    {
        //Notifiers collection
        $liked_users = FavoriteProducts::where('gig_id', '=', $this->favorite->gig_id)->where('user_id', '<>', $this->favorite->user_id)->get();
        return $liked_users;
    }


    public function getFavorite()
    {
        return $this->favorite;
    }
}
