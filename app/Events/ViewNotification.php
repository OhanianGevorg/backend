<?php

namespace App\Events;

use App\Http\Transformers\Notifications\ViewNotificationTransformer;
use App\Models\Stats\UsersStats;
use App\Models\User;
use App\Models\Users\Notification;
use App\Models\Users\UserNotification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ViewNotification extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    private $view;

    public function __construct(UsersStats $model)
    {
        $this->view = $model;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [
            'private-test-channel'.$this->view->owner_id
        ];
    }

//
    public function broadcastWith()
    {
        $notification = $this->saveNotification();

        return [
            'user' => ViewNotificationTransformer::transform($notification)
        ];
    }

    public function broadcastAs()
    {

//        dd($this->follow);

        return UserNotification::PROFILE_VIEW_NOTIFICATION;
    }

    private function saveNotification()
    {

        $auth_user_id = User::auth()->getKey();

        $userNotification = $this->view->user->getNotificationByType(UserNotification::PROFILE_VIEW_NOTIFICATION);
        $data['notifier_user_id'] = $auth_user_id;
        $data['notified_user_id'] = $this->view->owner_id;
        $data['users_notification_id'] = $userNotification->getKey();
        $data['event_type'] = $userNotification->getNotificationType();


        $notification = new Notification($data);
        $notification->save();

        return $notification;
    }

}
