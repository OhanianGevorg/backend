<?php

namespace App\Events;

use App\Http\Transformers\Notifications\GigViewNotificationTransformer;
use App\Models\Gig\Gig;
use App\Models\Stats\GigsStats;
use App\Models\User;
use App\Models\Users\Notification;
use App\Models\Users\UserNotification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GigViewNotification extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    private $view;

    public function __construct(GigsStats $model)
    {
        $this->view = $model;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [
            'private-test-channel'.Gig::find($this->view->owner_id)->user_id
        ];
    }

//
    public function broadcastWith()
    {
        $notification = $this->saveNotification();

        return [
            'user' => GigViewNotificationTransformer::transform($notification)
        ];
    }

    public function broadcastAs()
    {
        return UserNotification::GIG_PROFILE_VIEW_NOTIFICATION;
    }

    private function saveNotification()
    {

        $userNotification = $this->view->user->getNotificationByType(UserNotification::GIG_PROFILE_VIEW_NOTIFICATION);

        $data['notifier_user_id'] = User::auth()->getKey();
        $data['notified_user_id'] = $this->view->user_id;
        $data['users_notification_id'] = $userNotification->getKey();
        $data['event_type'] = $userNotification->getNotificationType();
        $data['action_id'] = $this->view->owner_id;
        $notification = new Notification($data);
        $notification->save();
        return $notification;
    }

}
