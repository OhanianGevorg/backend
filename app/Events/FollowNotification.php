<?php

namespace App\Events;

use App\Http\Transformers\Notifications\FollowNotificationTransformer;
use App\Http\Transformers\Shared\UsersNotificationTransformer;
use App\Models\User;
use App\Models\Users\Notification;
use App\Models\Users\UserFollows;
use App\Models\Users\UserNotification;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class FollowNotification extends Event implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    private $follow;

    public function __construct(UserFollows $model)
    {
        $this->follow = $model;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [
            'private-test-channel'.$this->follow->follow_user_id
        ];
    }

    public function broadcastWith()
    {
        $notification = $this->saveNotification();

        return [
            'user' => FollowNotificationTransformer::transform($notification)
        ];
    }

    public function broadcastAs()
    {

        return UserNotification::FOLLOW_NOTIFICATION;
    }

    private function saveNotification()
    {
        $userNotification = $this->follow->follow->getNotificationByType(UserNotification::FOLLOW_NOTIFICATION);

        $data['notifier_user_id'] = $this->follow->follower_user_id;
        $data['notified_user_id'] = $this->follow->follow_user_id;
        $data['users_notification_id'] = $userNotification->getKey();
        $data['event_type'] = $userNotification->getNotificationType();

        $notification = new Notification($data);
        $notification->save();

        return $notification;
    }

}
