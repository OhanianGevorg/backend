<?php

namespace App\Providers;

use App\Events\FollowNotification;
use App\Events\GigStarringNotification;
use App\Events\GigViewNotification;
use App\Events\StarringNotification;
use App\Events\ViewNotification;
use App\Models\Favorites\FavoriteProducts;
use App\Models\Favorites\FavoriteUsers;
use App\Models\Stats\GigsStats;
use App\Models\Stats\UsersStats;
use App\Models\User;
use App\Models\Users\UserFollows;
use App\Models\Users\UserNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class TGEventsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    private static $channel_name;
    private static $socket_id;

    public function boot()
    {
            UserFollows::created(function ($model) {

                if (User::auth() && $model->follow->getNotificationByType(UserNotification::FOLLOW_NOTIFICATION)
                    && $model->follow->getNotificationByType(UserNotification::FOLLOW_NOTIFICATION)->is_active())
                {
                    Event::fire(new FollowNotification($model));
                }
            });

            UsersStats::created(function ($model) {

                if ($model->user->getNotificationByType(UserNotification::FOLLOW_NOTIFICATION)
                    && $model->user->getNotificationByType(UserNotification::PROFILE_VIEW_NOTIFICATION)->is_active()
                    && $model->getByType('views') && User::auth())
                {
                    Event::fire(new ViewNotification($model));
                }
            });

            FavoriteUsers::created(function ($model) {

                if (User::auth() && $model->user->getNotificationByType(UserNotification::STARING_NOTIFICATION)
                    && $model->user->getNotificationByType(UserNotification::STARING_NOTIFICATION)->is_active())
                {
                    Event::fire(new StarringNotification($model));
                }
            });

            GigsStats::created(function ($model) {

                if (User::auth() &&  User::auth()->getKey() != $model->user_id
                    && $model->user->getNotificationByType(UserNotification::GIG_PROFILE_VIEW_NOTIFICATION)
                    && $model->user->getNotificationByType(UserNotification::GIG_PROFILE_VIEW_NOTIFICATION)->is_active())
                {
                    Event::fire(new GigViewNotification($model));
                }
            });

            FavoriteProducts::created(function ($model) {

                if (User::auth()
                    && $model->user->getNotificationByType(UserNotification::GIG_PROFILE_VIEW_NOTIFICATION)
                    && $model->user->getNotificationByType(UserNotification::GIG_PROFILE_VIEW_NOTIFICATION)->is_active()
                )
                {
                    Event::fire(new GigStarringNotification($model));
                }
            });


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public static function getPusherCredentials()
    {
        return [
            'channel_name' =>self::$channel_name,
            'socket_id' => self::$socket_id
        ];
    }
    public static function setChannelName ($channelName)
    {
        self::$channel_name = $channelName;
    }
    public static function setSocket_id ($socketID)
    {
        self::$socket_id = $socketID;
    }
}
