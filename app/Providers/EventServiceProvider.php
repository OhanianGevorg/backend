<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\FollowNotification' => [
            'App\Listeners\FollowNotificationListener',
        ],
        'App\Events\ViewNotification' => [
            'App\Listeners\ViewNotificationListener',
        ],
        'App\Events\StarringNotification' => [
            'App\Listeners\StarringNotificationListener',
        ],
        'App\Events\GigViewNotification' => [
            'App\Listeners\GigViewNotificationListener',
        ],
        'App\Events\GigStarringNotification' => [
            'App\Listeners\GigStarringNotificationListener',
        ],

    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
