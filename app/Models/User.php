<?php

namespace App\Models;

use App\Filters\Artists\ArtistFilter;
use App\Filters\Gigs\GigFilter;
use App\Filters\Search\FollowFilter;
use App\Filters\Statistic\StatisticFilter;
use App\Filters\Venues\VenueFilter;
use App\Filters\Search\UserFilter;
use App\Http\Requests\Filters\Artist\ArtistFilterRequest;
use App\Http\Requests\Filters\Gig\GigFilterRequest;
use App\Http\Requests\Filters\Search\FollowFilterRequest;
use App\Http\Requests\Filters\Statistic\StatisticFilterRequest;
use App\Http\Requests\Filters\Venue\VenueFilterRequest;
use App\Http\Requests\Filters\Search\UserFilterRequest;
use App\Models\Gig\Product;
use App\Models\Gig\GigArtist;
use App\Models\Stats\UsersStats;
use App\Models\Stats\VideosStats;
use App\Models\Users\Notification;
use App\Models\Users\TimeLine;
use App\Models\Users\UserGenres;
use App\Models\Users\UserNotification;
use App\Models\Users\UserPictures;
use App\Models\Users\UserReviews;
use App\Models\Users\UserSongs;
use App\Models\Users\UserVideos;
use App\Models\Users\UserWall;
use App\Models\Users\UserFollows;
use App\Models\Favorites\FavoriteSongs;
use App\Models\Favorites\FavoriteUsers;
use App\Models\Favorites\FavoriteVideos;
use App\Models\Favorites\FavoriteProducts;
use App\Models\Order\Order;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class User extends Authenticatable
{
    /**
     * The model roles definition.
     *
     * @var constants
     */
    const USERS = 1;
    const STORE = 2;
    const ADMIN = 3;
    const PERPAGE = 20;

    private static $date = null;

    public $stage_time = '';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'role_id',
        'api_token',
        'facebook_id',
        'name',
        'surname',
        'store_name',
        'address',
        'country',
        'state',
        'city',
        'latitude',
        'longitude',
        'contact_phone',
        'contact_mobile',
//        'contact_email',
        'web_site',
//        'other_web_site',
        'about',
//        'hear_about',
        'avatar',
        'fb_user_id'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $AuthUserAvatar;

    /**
     * Get authorized User
     *
     * @return User|false
     */
    public static function auth()
    {
        if (Session::has('api_token')) {

            $user = self::where('api_token', Session::get('api_token'))->first();

            if ($user) {
                self::$AuthUserAvatar = asset('pics/' . $user->avatar);
                return $user;
            }
        }

        return false;
    }

    public static function setDate($time)
    {
        self::$date = $time;
    }

    private function getDate()
    {
        if (self::$date) {
            return (new Carbon(self::$date))->toDateTimeString();
        }
        return self::$date;
    }

    public static function microtime_float()
    {
        list($usec, $sec) = explode(" ", microtime());
        return ((float)$usec + (float)$sec);
    }

    /**
     * Get Users with "artist" role
     * @param ArtistFilterRequest $filter
     *
     * @return User|null
     */
    public static function artists(ArtistFilterRequest $filter)
    {
        return (new ArtistFilter())->filter(self::query()->where('role_id', self::USER)->distinct()->select('users.*'), $filter)->orderBy('users.name');
    }


    /**
     * Get Users with "venue" role
     * @param VenueFilterRequest $filter
     *
     * @return User|null
     */
    public static function venues(VenueFilterRequest $filter)
    {
        return (new VenueFilter())->filter(self::query()->where('role_id', self::STORE)->distinct()->select('users.*'), $filter)->orderBy('users.name');
    }

    /**
     * @param UserFilterRequest $filter
     * @return mixed
     */
    public static function users(UserFilterRequest $filter)
    {
        return (new UserFilter())->filter(self::query()->distinct()->select('users.*'), $filter)->orderBy('name');
    }

    public static function follows(FollowFilterRequest $filter, $id)
    {
        return (new FollowFilter())->filter(UserFollows::query(), $filter, $id);
    }

    /**
     * Get Users with "musiclover" role
     *
     * @return User|null
     *
     *
     *
     * /**
     * Get Model related genres
     *
     * @return UserGenres|null
     */
    public function genres()
    {
        return $this->hasMany(UserGenres::class, 'user_id');
    }

    /**
     * Get Model related pictures
     *
     * @return UserPictures|null
     */
    public function pictures()
    {
        return $this->hasMany(UserPictures::class, 'user_id');
    }


    /**
     * Get Model related profile pictures
     *
     * @return UserPictures|null
     */
    public function profile_pictures()
    {
        $order_count = Order::where('user_id', $this->getKey())->where('item_type', UserPictures::PROFILE)->count();
        $pic_count = UserPictures::where('user_id', $this->getKey())->where('type', UserPictures::PROFILE)->count();
        if ($order_count && $order_count == $pic_count) {
            return UserPictures::select('user_pictures.*', 'order.order_number', 'order.item_id', 'order.user_id', 'order.item_type')
                ->where('user_pictures.type', '=', UserPictures::PROFILE)
                ->where('user_pictures.user_id', '=', $this->getKey())
                ->join('order', 'order.item_id', '=', 'user_pictures.id')
                ->distinct()
                ->orderBy('order.order_number');
        } else {
            return $this->hasMany(UserPictures::class, 'user_id')->where('type', UserPictures::PROFILE);
        }
    }


    /**
     * Get Model related gig pictures
     *
     * @return UserPictures|null
     */
    public function gig_pictures()
    {
        $order_count = Order::where('user_id', $this->getKey())->where('item_type', UserPictures::GIG)->count();
        $pic_count = UserPictures::where('user_id', $this->getKey())->where('type', UserPictures::GIG)->count();
        if ($order_count && $order_count == $pic_count) {
            return UserPictures::select('user_pictures.*', 'order.order_number', 'order.item_id', 'order.user_id', 'order.item_type')
                ->where('user_pictures.type', '=', UserPictures::GIG)
                ->where('user_pictures.user_id', '=', $this->getKey())
                ->join('order', 'order.item_id', '=', 'user_pictures.id')
                ->distinct()
                ->orderBy('order.order_number');

        } else {
            return $this->hasMany(UserPictures::class, 'user_id')->where('type', UserPictures::GIG);
        }

    }


    public function user_songs()
    {
        return $this->hasMany(UserSongs::class, 'user_id');
    }

    /**
     * Get Model related songs
     *
     * @return UserSongs|null
     */
    public function songs()
    {
        $order_count = Order::where('user_id', $this->getKey())->where('item_type', Order::SONG)->count();
        $song_count = UserSongs::where('user_id', $this->getKey())->count();
        if ($order_count && $order_count == $song_count) {
            return UserSongs::select('user_songs.*', 'order.item_id', 'order.item_type', 'order.order_number', 'order.user_id')
                ->where('user_songs.user_id', '=', $this->getKey())
                ->join('order', 'user_songs.id', '=', 'order.item_id')
                ->distinct()
                ->orderBy('order.order_number');
        } else {
            return $this->hasMany(UserSongs::class, 'user_id');
        }


    }


    /**
     * Get Model related videos
     *
     * @return UserVideos|null
     */
    public function videos()
    {
        return $this->hasMany(UserVideos::class, 'user_id');
    }


    /**
     * Get Model related reviews
     *
     * @return UserReviews|null
     */
    public function reviews()
    {
        return $this->hasMany(UserReviews::class, 'user_id');
    }


    /**
     * Get Model related Wall
     *
     * @return UserWall|null
     */
    public function wall()
    {
        return $this->hasMany(UserWall::class, 'user_id');
    }


    /**
     * Get Model related Gigs
     *
     * @return UserWall|null
     */
    public function gigs()
    {
        if ($this->is(self::ARTIST)) {
            $gigs = $this->hasMany(GigArtist::class, 'artist_user_id')->getResults()->map(function (GigArtist $relation) {
                return $relation->gig;
            });
            return $gigs->sortBy('start');
        }

        if ($this->is(self::VENUE)) {
            return $this->hasMany(Gig::class, 'venue_user_id')->orderBy('start')->getResults();
        }

    }


    public function gig_artist()
    {


        if ($this->is(self::ARTIST)) {

            return Gig::select('gigs.*')->distinct()
                ->join('gig_artists', 'gig_artists.gig_id', '=', 'gigs.id')
                ->join('users', 'gig_artists.artist_user_id', '=', 'users.id')
                ->where('users.id', '=', $this->getKey())->orderBy('start');
        }

        if ($this->is(self::VENUE)) {
            return Gig::select('gigs.*')->distinct()
                ->join('users', 'gigs.venue_user_id', '=', 'users.id')
                ->where('users.id', '=', $this->getKey())->orderBy('start');
        }
        if ($this->is(self::MUSIC_LOVER)) {
            return Gig::select('gigs.*')->distinct()
                ->join('user_favorite_gigs', 'user_favorite_gigs.gig_id', '=', 'gigs.id')
                ->join('users', 'user_favorite_gigs.user_id', '=', 'users.id')
                ->where('users.id', '=', $this->getKey())->orderBy('start');
        }

    }

    public function userGigsFilter(GigFilterRequest $filter)
    {

        return (new GigFilter())->filter($this->gig_artist(), $filter);
    }

    /**
     * @param User constant $role
     *
     * @return boolean
     */
    public function is($role)
    {
        return $this->role_id == $role;

    }


    /**
     * @param $user_id
     *
     * @return boolean
     */
    public function isOwn($user_id)
    {
        return $this->id == $user_id;
    }


    /**
     *
     * @return boolean
     */
    public function isFavorite()
    {
        $auth_user = self::auth();
        if (!$auth_user) {
            return false;
        }

        $isFavorite = FavoriteUsers::where('user_id', $auth_user->getKey())
            ->where('entity_user_id', $this->getKey())
            ->first();
        return !!$isFavorite;
    }


    /**
     *
     * @return boolean
     */
    public function isFollow()
    {
        $auth_user = self::auth();
        if (!$auth_user) {
            return false;
        }

        $isFollow = UserFollows::where('follower_user_id', $auth_user->getKey())
            ->where('follow_user_id', $this->getKey())
            ->first();
        return !!$isFollow;
    }


    /**
     * Get Model related favorite Users with role artist
     *
     * @return Collection
     */
    public function favorite_artists()
    {
        return $this->hasMany(FavoriteUsers::class, 'user_id')->where('entity_type', 'users')->getResults()->map(function (FavoriteUsers $relation) {
            return $relation->entity_user;
        });

    }


    /**
     * Get Model related favorite Users with role veune
     *
     * @return Collection
     */
    public function favorite_venues()
    {
        return $this->hasMany(FavoriteUsers::class, 'user_id')->where('entity_type', 'store')->getResults()->map(function (FavoriteUsers $relation) {
            return $relation->entity_user;
        });
    }


    /**
     * Get Model related favorite gigs
     *
     * @return Collection
     */
    public function favorite_products()
    {

        return $this->hasMany(FavoriteProducts::class, 'user_id')->getResults()->map(function (FavoriteProducts $relation) {
            return $relation->products;
        });

    }


    /**
     * Get Model related favorite songs
     *
     * @return Collection
     */
    public function favorite_songs()
    {
        return $this->hasMany(FavoriteSongs::class, 'user_id')->getResults()->map(function (FavoriteSongs $relation) {
            return $relation->song;
        });
    }


    /**
     * Get Model related favorite videos
     *
     * @return Collection
     */
    public function favorite_videos()
    {
        return $this->hasMany(FavoriteVideos::class, 'user_id')->getResults()->map(function (FavoriteVideos $relation) {
            return $relation->video;
        });
    }


    /**
     * Get Model related followers
     *
     * @return Collection
     */
    public function followers()
    {
        return $this->hasMany(UserFollows::class, 'follow_user_id')->getResults()->map(function (UserFollows $relation) {
            return $relation->follower;
        });
    }


    /**
     * Get Model related followings
     *
     * @return Collection
     */
    public function followings()
    {
        return $this->hasMany(UserFollows::class, 'follower_user_id')->getResults()->map(function (UserFollows $relation) {
            return $relation->follow;
        });
    }

    public function gigsCount()
    {
        if ($this->getDate()) {
            return $this->gig_artist()
                ->where('gigs.start', '>=', $this->getDate())
                ->count();
        }
        return $this->gig_artist()->count();
    }


    public function upcomingGigs()
    {

        if (self::getDate()) {
            return $this->gig_artist()
                ->where('gigs.start', '>=', (new Carbon(self::getDate()))->toDateTimeString())
                ->get();
        }
        return $this->gig_artist()->get();
    }

    public function has($property)
    {
        $field = null;

        if (in_array($property, $this->fillable)) {
            if ($this->{$property}) {
                $field = $this->{$property};
            }
        }
        return $field;
    }

    public function usersStats()
    {
        return UsersStats::where('owner_id', '=', $this->getKey())->get();
    }

    public function getSongsStats()
    {
        return $this->hasMany(UserSongs::class, 'user_id')->getResults()->map(function (UserSongs $stats) {
            return $stats->songsStats()->get();
        });
    }

    public function getVideosStats()
    {
        return $this->hasMany(UserVideos::class, 'user_id')->getResults()->map(function (UserVideos $stats) {
            return $stats->videosStats()->get();
        });
    }

    public function getCount($type, StatisticFilterRequest $filter)
    {
        return (new StatisticFilter())
            ->filter(UsersStats::where('owner_id', '=', $this->getKey()), $filter)
            ->where('type', '=', $type)
            ->count();
    }

    public function followersQuery(StatisticFilterRequest $filter)
    {
        return (new StatisticFilter())
            ->filter(UserFollows::where('follow_user_id', '=', $this->getKey()), $filter)
            ->count();
    }

    public function notifications()
    {
        return $this->hasMany(UserNotification::class, 'user_id');
    }

    public function getUserNotifications()
    {
        return $this->hasMany(Notification::class, 'notified_user_id');
    }

    public function getActiveNotifications()
    {
        return $this->getUserNotifications()->where('is_online', '=', 'no');
    }

    public function getNotificationByType($type)
    {
        return $this->notifications()->where('notification_type', '=', $type)->first();
    }

    public function timeLine()
    {
        return $this->hasOne(TimeLine::class, 'user_id');
    }

}
