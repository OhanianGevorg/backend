<?php

namespace App\Models\Stats;

use App\Filters\Statistic\StatisticFilter;
use App\Http\Requests\Filters\Statistic\StatisticFilterRequest;
use App\Models\Gig\Gig;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class GigsStats extends Model
{
    //
    protected $table = 'gigs_stats';

    protected $primaryKey = 'id';

    protected $fillable = [
        'owner_id',
        'user_id',
        'type',
        'ip',
        'created_at'
    ];

    public function getOwnerUser()
    {
        return User::where('id','=',$this->user_id)->distinct()->get();
    }

    public function getOwner()
    {
        return Gig::where('id','=',$this->owner_id)->distinct()->get();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public static function gigsStats(StatisticFilterRequest $filter,$id)
    {
        $query =  User::find($id)->gig_artist();

        return (new StatisticFilter())->filter($query->join('gigs_stats','gigs.id','=','gigs_stats.owner_id'),$filter);
    }
}
