<?php

namespace App\Models\Stats;

use App\Filters\Statistic\StatisticFilter;
use App\Http\Requests\Filters\Statistic\StatisticFilterRequest;
use App\Models\User;
use App\Models\Users\UserSongs;
use Illuminate\Database\Eloquent\Model;

class SongsStats extends Model
{
    protected $table = 'songs_stats';

    protected $primaryKey = 'id';

    protected $fillable = [
        'owner_id',
        'user_id',
        'type',
        'ip',
        'created_at'
    ];
    //
    public function getOwnerUser()
    {
        return User::where('id','=',$this->user_id);
    }

    public function getOwner()
    {
        return UserSongs::where('id','=',$this->owner_id)->distinct()->get();
    }

    public static function songsStats(StatisticFilterRequest $filter,$id)
    {
        dd(UserSongs::where('user_songs.user_id','=',$id)->join('songs_stats','user_songs.id','=','songs_stats.owner_id')->count());
        return (new StatisticFilter())->filter(UserSongs::where('user_songs.user_id','=',$id)
            ->join('songs_stats','user_songs.id','=','songs_stats.owner_id'),$filter);
    }
}
