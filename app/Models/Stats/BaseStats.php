<?php

namespace App\Models\Stats;

class BaseStats
{

    private static $action_types = array(
        'impressions',
        'views',
        'saves',
        'shares',
        'plays',
        'downloads'
    );


    public static function action($action)
    {
        if(in_array($action,self::$action_types)){
            return $action;
        }
        return false;
    }
}
