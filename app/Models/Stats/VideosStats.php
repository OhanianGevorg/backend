<?php

namespace App\Models\Stats;

use App\Filters\Statistic\StatisticFilter;
use App\Http\Requests\Filters\Statistic\StatisticFilterRequest;
use App\Models\Users\UserVideos;
use Illuminate\Database\Eloquent\Model;

class VideosStats extends Model
{
    //
    protected $table = 'videos_stats';

    protected $primaryKey = 'id';

    protected $fillable = [
        'owner_id',
        'user_id',
        'type',
        'ip',
        'created_at'
    ];

    public function getOwnerUser()
    {
        return User::where('id','=',$this->user_id);
    }


    public static function videosStats(StatisticFilterRequest $filter,$id)
    {
        return (new StatisticFilter())->filter(UserVideos::join('videos_stats','user_videos.id','=','videos_stats.owner_id')
            ->where('user_videos.user_id','=',$id),$filter);
    }
}
