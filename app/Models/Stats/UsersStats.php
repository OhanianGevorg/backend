<?php

namespace App\Models\Stats;

use App\Filters\Statistic\StatisticFilter;
use App\Http\Requests\Filters\Statistic\StatisticFilterRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class UsersStats extends Model
{
    //
    protected $table = 'users_stats';

    protected $primaryKey = 'id';

    protected $fillable = [
        'owner_id',
        'type',
        'ip',
        'created_at'
    ];

    public function getOwnerUser()
    {
        return User::where('id','=',$this->owner_id);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }


    public function getByType($type) {
        return $this->where('type','=',$type)->first();
    }

    public static function usersStats(StatisticFilterRequest $filter,$id)
    {
        return (new StatisticFilter())->filter(self::query()->where('owner_id','=',$id),$filter)->get();
    }
}
