<?php
namespace App\Models\Favorites;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use App\Models\Gig\Product;

class FavoriteProducts extends Model
{  
    protected $table = 'user_favorite_products';

    protected $fillable = [
        'product_id',
        'user_id',
        'created_at'
    ];

    public function products()
    {

        return $this->belongsTo(Product::class, 'product_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}