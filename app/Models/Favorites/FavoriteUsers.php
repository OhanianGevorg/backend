<?php
namespace App\Models\Favorites;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;


class FavoriteUsers extends Model
{  
    protected $table = 'user_favorite_users';

    protected $fillable = [        
        'user_id',
        'entity_user_id',
        'entity_type'    
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function entity_user()
    {
        return $this->belongsTo(User::class, 'entity_user_id');
    }

}