<?php
namespace App\Models\Favorites;

use Illuminate\Database\Eloquent\Model;
use App\Models\Users\UserSongs;
use App\Models\User;


class FavoriteSongs extends Model
{  
    protected $table = 'user_favorite_songs';

    protected $fillable = [
        'song_id',
        'user_id'      
    ];
   
    public function song()
    {
        return $this->belongsTo(UserSongs::class, 'song_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}