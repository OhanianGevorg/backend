<?php
namespace App\Models\Favorites;

use Illuminate\Database\Eloquent\Model;
use App\Models\Users\UserVideos;
use App\Models\User;


class FavoriteVideos extends Model
{  
    protected $table = 'user_favorite_videos';

    protected $fillable = [
        'video_id',
        'user_id'      
    ];

    public function video()
    {
        return $this->belongsTo(UserVideos::class, 'video_id');
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
   
}