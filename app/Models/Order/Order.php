<?php

namespace App\Models\Order;

use App\Models\User;
use App\Models\Users\UserPictures;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    /**
     * Define Order item types
     *
     * @const int
     */
    const PROFILE = 2;
    const GIG = 1;
    const SONG = 3;


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order';


    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'order_number',
        'user_id',
        'item_id',
        'item_type'
    ];


    /**
     * Get model related user object
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }



    /**
     * Get model related order object
     *
     * @return Order
     */
    public function pics()
    {
        return $this->belongsTo(UserPictures::class, 'item_id');
    }

}
