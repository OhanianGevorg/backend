<?php
namespace App\Models\Gig;

use App\Filters\Gigs\GigFilter;
use App\Filters\Statistic\StatisticFilter;
use App\Http\Requests\Filters\Gig\GigFilterRequest;
use App\Http\Requests\Filters\Statistic\StatisticFilterRequest;
use App\Models\Stats\GigsStats;
use App\Models\Stats\SongsStats;
use App\Models\User;
use App\Models\Favorites\FavoriteProducts;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use MongoDB\Driver\Query;

class Product extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'store_user_id',
        'poster',
        'price',
        'name',
        'description',
        'ticket_seller_site',
        'created_at',
        'updated_at'
    ];


    /**
     * @param GigFilterRequest $filter
     *
     * @return Collection
     */
    public static function getAll(GigFilterRequest $filter)
    {
        return (new GigFilter())->filter(self::query()->distinct()->select('products.*'), $filter)->orderBy('start');
    }


    /**
     * Get Model related types
     *
     * @return Query
     */
    public function types()
    {
        return $this->hasMany(GigType::class, 'gig_id')->getResults()->map(function (GigType $relation) {
            return $relation->type;
        });
    }

    /**
     * Get Model related GigType
     * return Query
     */
    public function gigTypes()
    {
        return $this->hasMany(GigType::class, 'gig_id');
    }

    /**
     * Get Model related Users with role artist
     *
     * @return Query
     */
    public function artists()
    {

        return $this->hasMany(GigArtist::class, 'gig_id')->getResults()->map(function (GigArtist $relation) {
            if ($relation->user) {
                $relation->user->stage_time = $relation->stage_time;
            }
            return $relation->user;
        });
    }


    /**
     * Get Model related Users with role artist
     *
     * @return Query
     */
    public function gig_artists()
    {
        return $this->hasMany(GigArtist::class, 'gig_id');
    }


    /**
     * Get Model related Users with role venue
     *
     * @return Query
     */
    public function venue()
    {
        return $this->belongsTo(User::class, 'venue_user_id');
    }


    /**
     * Get Model related Users
     *
     * @return Query
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    /**
     *
     * @return boolean
     */
    public function isFavorite()
    {
        $auth_user = User::auth();
        if (!$auth_user) {
            return false;
        }

        $isFavorite = FavoriteProducts::where('user_id', $auth_user->getKey())
            ->where('gig_id', $this->getKey())
            ->first();
        return !!$isFavorite;
    }

    public function wall()
    {
        return $this->hasMany(GigWall::class, 'gig_id');
    }

    public function saved_by()
    {
        return $this->hasMany(FavoriteProducts::class, 'gig_id')->getResults()->map(function (FavoriteProducts $relation) {
            return $relation->user;
        });
    }

    public function gigStats()
    {
        return GigsStats::where('owner_id', '=', $this->getKey())->get();
    }

    public function getCount($type, StatisticFilterRequest $filter)
    {
        return (new StatisticFilter())
            ->filter(GigsStats::where('owner_id', '=', $this->getKey()), $filter)
            ->where('type', '=', $type)
            ->count();
    }
}