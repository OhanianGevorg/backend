<?php
namespace App\Models\Gig;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;

class GigArtist extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gig_artists';


    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gig_id',
        'artist_user_id',
        'stage_time'
    ];


    /**
     * The model related User with role artist
     *
     * @return Relation
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'artist_user_id');
    }

    /**
     * The model related Gig
     *
     * @return Relation
     */
    public function gig()
    {
        return $this->belongsTo(Gig::class, 'gig_id');
    }
}