<?php

namespace App\Models\Gig;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Models\User;


class GigWall extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gig_wall';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gig_id',
        'sender_id',
        'message'
    ];
    

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gig()
    {
        return $this->belongsTo(Gig::class, 'gig_id');
    }


    /**
     * Get model related User object
     *
     * @return Relation
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_user_id');
    }
}
