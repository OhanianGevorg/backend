<?php
namespace App\Models\Gig;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class GigType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gig_types';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gig_id',
        'gig_system_type_id'
    ];


    /**
     * The model related gig system types
     *
     * @return Relation
     */
    public function type()
    {
        return $this->belongsTo(GigSystemTypes::class, 'gig_system_type_id');
    }
}