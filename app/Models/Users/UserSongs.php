<?php
namespace App\Models\Users;

use App\Filters\Statistic\StatisticFilter;
use App\Http\Requests\Filters\Statistic\StatisticFilterRequest;
use App\Models\Stats\SongsStats;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

use App\Models\Favorites\FavoriteSongs;

class UserSongs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_songs';


    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'song_name',
        'file_id',
        'file_name',
        'description',
        'permission'
    ];


    /**
     * Get Model related User object with role "artist"
     *
     * @return UserSongs|null
     */
    public function artist()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     *
     * @return boolean
     */
    public function isFavorite()
    {
        $auth_user = User::auth();
        if (!$auth_user) {
            return false;
        }

        $isFavorite = FavoriteSongs::where('user_id', $auth_user->getKey())
            ->where('song_id', $this->getKey())
            ->first();
        return !!$isFavorite;
    }

    public function songsStats()
    {
        return SongsStats::where('owner_id', '=', $this->getKey())->get();
    }

    public function getCount($type, StatisticFilterRequest $filter)
    {

        return (new StatisticFilter())
            ->filter(SongsStats::where('owner_id', '=', $this->getKey()), $filter)
            ->where('type', '=', $type)
            ->count();
    }

}