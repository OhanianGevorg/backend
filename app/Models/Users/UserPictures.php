<?php
namespace App\Models\Users;

use App\Models\Order\Order;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class UserPictures extends Model
{
    /**
     * Define picture types
     *
     * @const int
     */
    const GIG = 1;
    const PROFILE = 2;

    public static $middle_poster =[
        'name'=>'middle',
        'width'=>509,
        'height'=>723
    ];

    public static $small_poster = [
        'name'=>'small',
        'width'=>161,
        'height'=>231
    ];


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_pictures';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id',
        'type',
        'file_id',
        'caption',
        'photo_credit'
    ];


    /**
     * Get model related user object
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function order()
    {
        return $this->hasOne(Order::class,'item_id');
    }
//    public function saveMany()
//    {
//        parent::saveMany();
//        dd(123);
//    }
}