<?php

namespace App\Models\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Self_;

class UserNotification extends Model
{
    //
    protected $table = 'user_notifications';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'event_name',
        'notification_type',
        'is_active',
        'updated_at',
        'created_at'
    ];


    const STARING_NOTIFICATION = 'staring';
    const GIG_STARING_NOTIFICATION = 'gig_staring';
    const FOLLOW_NOTIFICATION = 'follow';
    const PROFILE_VIEW_NOTIFICATION = 'profile_view';
    const GIG_PROFILE_VIEW_NOTIFICATION = 'gig_profile_view';

    private static $notifications_rule = [
        [
            'event_name' => 'StarredEvent',
            'users' => [User::USERS, User::STORE],
            'notification_type' => self::STARING_NOTIFICATION,
            'is_active' => 'yes'
        ],

        [
            'event_name' => 'GigStarredEvent',
            'users' => [User::USERS, User::STORE],
            'notification_type' => self::GIG_STARING_NOTIFICATION,
            'is_active' => 'yes'
        ],

        [
            'event_name' => 'FollowNotification',
            'users' => [User::USERS, User::STORE],
            'notification_type' => self::FOLLOW_NOTIFICATION,
            'is_active' => 'yes'
        ],

        [
            'event_name' => 'UserProfileNotification',
            'users' => [User::USERS, User::STORE],
            'notification_type' => self::PROFILE_VIEW_NOTIFICATION,
            'is_active' => 'yes'
        ],

        [
            'event_name' => 'GigProfileNotification',
            'users' => [User::USERS, User::STORE],
            'notification_type' => self::GIG_PROFILE_VIEW_NOTIFICATION,
            'is_active' => 'yes'
        ]
    ];

    public static function getEventRules($role, $user_id)
    {
        $eventsList = [];
        foreach (self::$notifications_rule as $key => $rule) {

            if (in_array($role, $rule['users'])) {
                $rule['user_id'] = $user_id;
                $eventsList[] = $rule;
            }
        }
        return $eventsList;
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class, 'users_notification_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function is_active()
    {
        return ($this->is_active === 'yes') ? true : false;
    }


    public function is($event_type)
    {
        return $this->notification_type === $event_type;
    }

    public function getNotificationType()
    {
        return $this->notification_type;
    }
}
