<?php
namespace App\Models\Users;

use App\Filters\Statistic\StatisticFilter;
use App\Http\Requests\Filters\Statistic\StatisticFilterRequest;
use App\Models\Stats\VideosStats;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\Relation;

use App\Models\Favorites\FavoriteVideos;

class UserVideos extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_videos';


    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'title',
        'caption',
        'url'
    ];


    /**
     * Get model related user object
     *
     * @return Relation
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


     /**
     *
     * @return boolean
     */
    public function isFavorite()
    {  
        $auth_user = User::auth();
        if(! $auth_user ) {
            return false;
        }

        $isFavorite = FavoriteVideos::where('user_id', $auth_user->getKey())
                                    ->where('video_id', $this->getKey())
                                    ->first();
        return !!$isFavorite;
    }

    public function getYouTubeIdFromURL($url)
    {
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        return  $my_array_of_vars['v'];
    }

    public function videosStats()
    {
        return VideosStats::where('owner_id','=',$this->getKey())->get();
    }

    public function getCount($type,StatisticFilterRequest $filter)
    {
        return (new StatisticFilter())
            ->filter(VideosStats::where('owner_id','=',$this->getKey()),$filter)
            ->where('type','=',$type)
            ->count();
    }
}