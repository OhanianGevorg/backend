<?php
namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Models\User;


class UserSearchSaves extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_search_saves';


    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'date',
        'type',
        'types',
        'location_city',
        'location_radius',
        'location_latitude',
        'location_longitude',
        'min_price',
        'max_price',
        'genres',
        'first_initial'
    ];

    /*
     * constants
     */

    const ARTIST = 1;
    const VENUE = 2;
    const GIG = 3;


    /**
     * Get model related User object
     *
     * @return Relation
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }



}