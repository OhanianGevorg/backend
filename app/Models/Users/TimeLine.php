<?php

namespace App\Models\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class TimeLine extends Model
{
    //
    protected $table = 'time_line';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'user_type',
        'user_name',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function posts()
    {
        return $this->hasMany(Post::class, 'time_line_id');
    }

    public function sharedPosts()
    {
        return $this->hasMany(SharePost::class, 'share_user_id', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getSharedPosts()
    {
        return $this->sharedPosts()->getResults()->map(function (SharePost $relation) {
            return $relation->post;
        });
    }
}
