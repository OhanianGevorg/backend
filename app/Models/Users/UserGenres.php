<?php
namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use App\Models\Genre\Genre;
use App\Models\User;

class UserGenres extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_genres';


    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'genre_id',
        'user_id'
    ];


    /**
     * Get model related user object
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    /**
     * Get model related genre object
     *
     * @return Genre
     */
    public function genre()
    {
        return $this->belongsTo(Genre::class, 'genre_id');
    }
}