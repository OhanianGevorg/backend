<?php

namespace App\Models\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class SharePost extends Model
{
    //
    protected $table = 'share_posts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'owner_id',
        'share_user_id',
        'time_line_id',
        'post_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class,'post_id');
    }

    public function shareUser()
    {
        return $this->belongsTo(User::class,'share_user_id');
    }
}
