<?php

namespace App\Models\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
    protected $table = 'notifications';
    protected $primaryKey = 'id';
    protected $fillable = [
        'users_notification_id',
        'notified_user_id',
        'notifier_user_id',
        'event_type',
        'action_id',
        'is_online',
        'updated_at',
        'created_at'
    ];

    public function user_notification()
    {
        return $this->belongsTo(UserNotification::class,'users_notification_id');
    }

    public function getNotifier()
    {
        return User::where('id','=',$this->notifier_user_id)->first();
    }

}
