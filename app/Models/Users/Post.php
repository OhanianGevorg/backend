<?php

namespace App\Models\Users;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $table = 'posts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'user_id',
        'time_line_id',
        'post_content',
        'post_type',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public function timeLine()
    {
        return $this->belongsTo(TimeLine::class,'time_line_id');
    }

    public function shared()
    {
        return $this->hasMany(SharePost::class,'post_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
