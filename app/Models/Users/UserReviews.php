<?php
namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class UserReviews extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_reviews';


    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'review',
        'reviewed_by',
        'review_date'
    ];
}