<?php
namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class UserFollows extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_follows';


    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'follower_user_id',
        'follow_user_id'
    ];


    /**
     * Get model related user object
     *
     * @return User
     */
    public function follow()
    {
        return $this->belongsTo(User::class, 'follow_user_id');
    }


    /**
     * Get model related genre object
     *
     * @return Genre
     */
    public function follower()
    {
        return $this->belongsTo(User::class, 'follower_user_id');
    }

    public static function isFollowed($follower_id,$following_id){
        return self::where('follower_user_id','=',$follower_id)
            ->where('follow_user_id','=',$following_id)->count();
    }
}