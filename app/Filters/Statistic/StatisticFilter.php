<?php

namespace App\Filters\Statistic;

use App\Filters\AbstractFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class StatisticFilter extends AbstractFilter
{
    public function time(Builder $query, $time)
    {
        return $query->where('created_at','>=', (new Carbon($time))->toDateString());
    }

    public function type(Builder $query, $type)
    {
        dd(456);
    }
}