<?php
namespace App\Filters\Venues;

use App\Filters\AbstractFilter;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use App\Enums\DateIntervals;
use Illuminate\Support\Facades\DB;

class VenueFilter extends AbstractFilter
{

    private $now = null;

    private $count = 0;

    /**
     * @param Builder $query
     */
    private function getGig(Builder $query)
    {
        if ($this->count === 0) {
            $query->join('gigs', 'users.id', '=', 'gigs.venue_user_id');
            $this->count++;
        }
    }

    /**
     * @param null $timezone
     */
    private function setNow($timezone = null)
    {
        if ($timezone) {
            $this->now = Carbon::now()->tz($timezone);
        }

    }

    /**
     * @param Builder $query
     * @param string $name
     */
    public function name(Builder $query, $name)
    {
        $query->where('users.name', 'Like', '%'. $name . '%');
    }


    /**
     * @param Builder $query
     * @param string $firstInitial
     */
    public function firstInitial(Builder $query, $firstInitial)
    {
        $query->where(DB::raw('UCASE(LEFT(users.name, 1))'), '=', DB::raw('UCASE("' . $firstInitial . '")'));
    }


    /**
     * @param Builder $query
     * @param array $genres
     */
    public function genres(Builder $query, $genres)
    {
        $query->join('user_genres', 'user_genres.user_id', '=', 'users.id');
        $query->join('genres', 'user_genres.genre_id', '=', 'genres.id');
        $query->whereIn('genres.id', explode(',', $genres));
    }


    /**
     * @param Builder $query
     * @param string $date
     */
    public function date(Builder $query, $date)
    {
        if (!$this->now) {
            $this->now = Carbon::now();
        }

        $this->getGig($query);

        switch ($date) {
            case DateIntervals::TODAY:
                $query->where('gigs.start', '>=', $this->now->startOfDay()->toDateTimeString())->where('gigs.start', '<=', $this->now->startOfDay()->addHours(17)->addMinute(0)->toDateTimeString());
                $query->where('gigs.end', '<', $this->now->startOfDay()->addHours(17)->addMinute(0)->toDateTimeString());
                break;
            case DateIntervals::TONIGHT:
                $query->where('gigs.start', '>=', $this->now->startOfDay()->addHours(17)->addMinute(1)->toDateTimeString());
                $query->where('gigs.end', '<=', $this->now->endOfDay()->toDateTimeString());
                break;
            case DateIntervals::WEEKEND:
                $query->where('gigs.start', '>=', $this->now->startOfWeek()->addDay(5)->toDateTimeString());
                $query->where('gigs.end', '<=', $this->now->endOfWeek()->toDateTimeString());
                break;

            case DateIntervals::WEEK:
                $query->where('gigs.start', '>=', $this->now->toDateTimeString());
                $query->where('gigs.end', '<=', $this->now->addWeek()->toDateTimeString());
                break;

            case DateIntervals::MONTH:
                $query->where('gigs.start', '>=', $this->now->toDateTimeString());
                $query->where('gigs.end', '<=', $this->now->addMonth()->toDateTimeString());
                break;

            default:
                $date = new Carbon($date);
                $query->where('gigs.start', '>=', $date->hour(00)->minute(00)->toDateTimeString());
                $query->where('gigs.end', '<=', $date->hour(23)->minute(59)->toDateTimeString());
                break;
        }
    }


    /**
     * @param Builder $query
     * @param string $type
     */
    public function type(Builder $query, $type)
    {
        switch ($type) {
            case 'friends_favorites':
                break;

            case 'my_favorites':
                $user = User::auth();
                if ($user) {
                    $query->join('user_favorite_users', 'users.id', '=', 'user_favorite_users.entity_user_id')
                        ->where('user_favorite_users.user_id', '=', $user->getKey());
                }
                break;

        }
    }


    /**
     * @param Builder $query
     * @param $location
     */
    public function location(Builder $query, $location)
    {
        $city = array_get($location, 'city');
        $radius = array_get($location, 'radius');
        $latitude = array_get($location, 'latitude');
        $longitude = array_get($location, 'longitude');
        if ((!empty($radius) && is_numeric($radius)) && !empty($latitude) && !empty($longitude)) {
            $query->where(DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( `latitude` ) ) * cos( radians( `longitude` ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( `latitude` ) ) ) )'), '<=', $radius);
        } elseif (!empty($city)) {
            $query->where('users.city', $city);
        }
    }

    public function maxPrice($query, $maxPrice)
    {
        if ($maxPrice > 0) {
            $this->getGig($query);
            $query->where('gigs.price', '<=', $maxPrice);
        }

    }

    public function minPrice($query, $minPrice)
    {
        $this->getGig($query);
        $query->where('gigs.price', '>=', $minPrice);
    }

    public function types(Builder $query, $types)
    {
        $this->getGig($query);
        $query->join('gig_types', 'gigs.id', '=', 'gig_types.gig_id');
        $query->whereIn('gig_types.gig_system_type_id', explode(',', $types));
    }

    public function timezone(Builder $query, $timezone)
    {
        if ($timezone) {
            $this->setNow($timezone);
        }
    }
}