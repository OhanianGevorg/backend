<?php
namespace App\Filters\Gigs;

use App\Enums\DateIntervals;
use App\Filters\AbstractFilter;
use App\Models\User;
use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Builder;

class GigFilter extends AbstractFilter
{
    private $isUsersTableJoined = false;

    private $now = null;

    /**
     * @param null $timezone
     */
    private function setNow($timezone = null)
    {

        if ($timezone) {
            $this->now = (new Carbon($timezone))->toDateTimeString();
        }

    }

    /**
     * @param Bulder $query
     * @param $param
     */
    public function ages(Builder $query, $param)
    {
       $query->where('gigs.ages', '=', $param);
    }

    /**
     * @param Builder $query
     * @param  $types
     */


    public function types(Builder $query, $types)
    {
        $query->join('gig_types', 'gigs.id', '=', 'gig_types.gig_id')->whereIn('gig_types.gig_system_type_id', explode(',', $types));
    }


    /**
     * @param Builder $query
     * @param string $date
     */
    public function date(Builder $query, $date)
    {
        ($this->now) ? $this->now = (new Carbon($this->now)) : $this->now = Carbon::now();
        switch ($date) {
            case DateIntervals::TODAY:

                $query->where('gigs.start', '>=', $this->now->startOfDay()->toDateTimeString())->where('gigs.start', '<=', $this->now->startOfDay()->addHours(17)->addMinute(0)->toDateTimeString());
                $query->where('gigs.end', '<', $this->now->startOfDay()->addHours(17)->addMinute(0)->toDateTimeString());
                break;
            case DateIntervals::TONIGHT:
                $query->where('gigs.start', '>=', $this->now->startOfDay()->addHours(17)->addMinute(1)->toDateTimeString());
                $query->where('gigs.end', '<=', $this->now->endOfDay()->toDateTimeString());
                break;

            case DateIntervals::WEEKEND:
                $query->where('gigs.start', '>=', $this->now->startOfWeek()->addDay(5)->toDateTimeString());
                $query->where('gigs.end', '<=', $this->now->endOfWeek()->toDateTimeString());
                break;

            case DateIntervals::WEEK:
                $query->where('gigs.start', '>=', $this->now->toDateTimeString());
                $query->where('gigs.end', '<=', $this->now->addWeek()->toDateTimeString());
                break;

            case DateIntervals::MONTH:
                $query->where('gigs.start', '>=', $this->now->toDateTimeString());
                $query->where('gigs.end', '<=', $this->now->addMonth()->toDateTimeString());
                break;

            default:
                $range = explode(',', $date);
                $rangeDate = [
                    'endDate' => max($range),
                    'startDate' => min($range)
                ];
                $query->where('gigs.start', '>=', (new Carbon($rangeDate['startDate']))->toDateTimeString());
                $query->where('gigs.end', '<=', (new Carbon($rangeDate['endDate']))->toDateTimeString());
                break;
        }
    }


    /**
     * @param Builder $query
     * @param int $maxPrice
     */
    public function maxPrice(Builder $query, $maxPrice)
    {
        if ($maxPrice > 0) {
            $query->where('gigs.price', '<=', $maxPrice);
        }
    }


    /**
     * @param Builder $query
     * @param int $minPrice
     */
    public function minPrice(Builder $query, $minPrice)
    {
        $query->where('gigs.price', '>=', $minPrice);
    }


    /**
     * @param Builder $query
     * @param array $genres
     */
    public function genres(Builder $query, $genres)
    {
        $query->join('gig_artists', 'gigs.id', '=', 'gig_artists.gig_id')
            ->join('user_genres', 'gig_artists.artist_user_id', '=', 'user_genres.user_id')
            ->whereIn('user_genres.genre_id', explode(',', $genres))->distinct();
    }


    /**
     * @param Builder $query
     * @param $location
     */
    public function location(Builder $query, $location)
    {
        $this->joinUsersTable($query);
        $city = array_get($location, 'city');
        $radius = array_get($location, 'radius');
        $latitude = array_get($location, 'latitude');
        $longitude = array_get($location, 'longitude');

        if ((!empty($radius) && is_numeric($radius)) && !empty($latitude) && !empty($longitude)) {
            $query->where(DB::raw('( 6371 * acos( cos( radians(' . $latitude . ') ) * cos( radians( `latitude` ) ) * cos( radians( `longitude` ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( `latitude` ) ) ) )'), '<=', $radius);
        } elseif (!empty($city)) {
            $query->where('users.city', $city);
        }
    }

    /**
     * @param Builder $query
     * @param $favorite
     */
    public function favorite(Builder $query, $param)
    {
        $user = User::auth();
        if ($user && $param == 'my_saved_gigs') {
            $query->join('user_favorite_gigs', 'user_favorite_gigs.gig_id', '=', 'gigs.id')
                ->where('user_favorite_gigs.user_id', '=', $user->getKey());
        }
    }

    /**
     * @param Builder $query
     * @param $rangeDate
     */
    public function rangeDate(Builder $query, $rangeDate)
    {
        switch ($rangeDate) {
            case($rangeDate['startDate'] && $rangeDate['endDate']):
                $query->where('gigs.start', '>=', (new Carbon($rangeDate['startDate']))->toDateTimeString())->where('gigs.end', '<=', (new Carbon($rangeDate['endDate']))->toDateTimeString());
                break;
            case ($rangeDate['startDate'] && !$rangeDate['endDate']):
                $query->where('gigs.start', '>=', (new Carbon($rangeDate['startDate']))->toDateTimeString());
                break;
            case ($rangeDate['endDate'] && !$rangeDate['startDate']):
                $query->getQuery()->orders = null;
                $query->where('gigs.end', '<', $rangeDate['endDate'])->orderBy('start', 'DESC');
                break;
            default:
                $query->where('gigs.start', '<>', null);
        }
    }

    public function timezone(Builder $query, $timezone)
    {
        if ($timezone) {
            $this->setNow($timezone);
        }
    }


    /**
     * @param Builder $query
     */
    private function joinUsersTable(Builder &$query)
    {
        if (!$this->isUsersTableJoined) {
            $query->join('users', 'gigs.venue_user_id', '=', 'users.id');
            $this->isUsersTableJoined = true;
        }
    }
}