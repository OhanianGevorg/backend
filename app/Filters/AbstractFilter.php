<?php
namespace App\Filters;

use App\Http\Requests\FilterRequest;
use Illuminate\Database\Eloquent\Builder;

abstract class AbstractFilter
{
    /**
     * @param Builder $query
     * @param FilterRequest $filter
     * @return Builder
     */
    public function filter(Builder $query, FilterRequest $filter,$param =null)
    {
        foreach($filter->filters() as $name => $value) {
            if(empty($value)) {
                continue;
            }

            if(is_array($value) && !array_filter($value)&&$name!=='rangeDate') {
                continue;
            }

            if($param){
                $this->{$name}($query, $value,$param);
            }else{
                $this->{$name}($query, $value);
            }

        }

        return $query;
    }
}