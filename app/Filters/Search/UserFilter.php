<?php

namespace App\Filters\Search;

use App\Models\Gig\Gig;
use App\Models\User;
use DB;
use App\Enums\DateIntervals;
use App\Filters\AbstractFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Request;

class UserFilter extends AbstractFilter
{
    private $now = null;

    /**
     * @param Builder $query
     * @param string $name
     */
    public function name(Builder $query, $name)
    {
        $query->where('users.name', 'Like',  $name . '%')->orWhere('users.city', '=',  $name  )->orWhere('users.name', 'Like',  '%'.' '.$name . '%');

    }


}