<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 9/26/16
 * Time: 7:39 PM
 */

namespace App\Filters\Search;


use App\Filters\AbstractFilter;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use DB;

class FollowFilter extends AbstractFilter
{

    private function isJoined()
    {

    }

    public function type(Builder $query,$type,$param)
    {
        if(!$param){
            return;
        }
        switch ($type){
            case('followers'):
                $query->where('follow_user_id','=',$param);
                $query->join('users','users.id','=','user_follows.follower_user_id');
                break;
            case ('followings'):
                $query->where('follower_user_id','=',$param);
                $query->join('users','users.id','=','user_follows.follow_user_id');
                break;
            default:
                $query->where('follow_user_id','=',$param);
                $query->join('users','users.id','=','user_follows.follower_user_id');
                break;
        }
    }

    public function name(Builder $query, $name,$param)
    {
        $query->where('users.name', 'Like', '%' . $name . '%');

    }
}