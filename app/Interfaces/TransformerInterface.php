<?php
namespace App\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface TransformerInterface
{
    /**
     * @param Model $model
     * @return array
     */
    public static function transform($model);
}