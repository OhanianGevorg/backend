<?php
namespace App\Traits;

trait PrepareCollection
{
    /**
     * @param array $rawCollection
     * @param array $injector
     *
     * @return array
     */
    public static function prepareCollection(array $rawCollection, array $injector = [])
    {
        $col = [];
        foreach ($rawCollection as $raw) {
            $col[] = self::prepare($raw, $injector);
        }

        return $col;
    }
}