<?php
namespace App\Traits;

use Traversable;

trait TransformCollection
{
    /**
     * @param Traversable|null $collection
     *
     * @return array
     */
    public static function transformCollection(Traversable $collection = null, $params = null)
    {
        if (!$collection) {
            return [];
        }

        $results = [];
        foreach ($collection as $item) {
            if(!$params){
                $results[] = self::transform($item);
            }else{
                $results[] = self::transform($item,$params);
            }

        }

        return $results;
    }
}