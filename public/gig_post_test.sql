/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : tonightsgig

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-06-11 04:23:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for artist_pics
-- ----------------------------
DROP TABLE IF EXISTS `artist_pics`;
CREATE TABLE `artist_pics` (
  `pic_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` int(11) NOT NULL,
  `pic_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pic_type` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`pic_id`),
  KEY `artist_pics_artist_id_index` (`artist_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of artist_pics
-- ----------------------------
INSERT INTO `artist_pics` VALUES ('1', '2', 'picture1.png', '1', '2016-06-09 17:08:51', '2016-06-09 17:08:56', '1');
INSERT INTO `artist_pics` VALUES ('2', '1', 'picture2.jpg', '1', '2016-06-09 17:09:02', '2016-06-09 17:09:06', '2');
INSERT INTO `artist_pics` VALUES ('3', '3', 'picture5.jpg', '1', null, null, '3');
INSERT INTO `artist_pics` VALUES ('4', '4', 'picture3.jpg', '1', null, null, '4');
INSERT INTO `artist_pics` VALUES ('5', '5', 'picture4.jpg', '1', null, null, '5');

-- ----------------------------
-- Table structure for artist_songs
-- ----------------------------
DROP TABLE IF EXISTS `artist_songs`;
CREATE TABLE `artist_songs` (
  `song_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `song_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_index` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`song_id`),
  KEY `artist_songs_artist_id_index` (`artist_id`),
  KEY `artist_songs_album_id_index` (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of artist_songs
-- ----------------------------
INSERT INTO `artist_songs` VALUES ('1', '2', '1', 'hgg', 'dragons.mp3', 'Monsters', '4', '2016-06-10 13:05:34', '2016-06-16 13:05:39', '1');
INSERT INTO `artist_songs` VALUES ('2', '1', '1', 'mkmk', 'coldplay.mp3', 'Weekend Seeb', '2', '2016-06-03 13:05:42', '2016-06-08 13:05:46', '2');
INSERT INTO `artist_songs` VALUES ('3', '3', '1', 'asd', 'audiomachine.mp3', 'Age of Dragon', '3', '2016-06-10 23:46:02', '2016-06-10 23:46:08', '3');
INSERT INTO `artist_songs` VALUES ('4', '2', '1', 'asd', 'dragons2.mp3', 'Radioactive', '1', '2016-06-09 23:46:13', '2016-06-10 23:46:18', '4');
INSERT INTO `artist_songs` VALUES ('5', '1', '1', 'asd', 'coldplay2.mp3', 'Paradise', '5', '2016-06-10 23:48:19', '2016-06-10 23:48:24', '5');
INSERT INTO `artist_songs` VALUES ('6', '5', '1', 'ad', 'alanwalker.mp3', 'Faded', '6', '2016-06-09 03:12:19', '2016-06-10 00:23:56', '6');
INSERT INTO `artist_songs` VALUES ('7', '5', '1', 'asd', 'alanwalker2.mp3', 'Until i Die', '7', '2016-06-09 03:13:28', '2016-06-11 03:13:32', '7');
INSERT INTO `artist_songs` VALUES ('8', '2', '1', 'asd', 'dragons3.mp3', 'Its Time', '8', '2016-06-11 03:16:24', '2016-06-11 03:16:34', '8');
INSERT INTO `artist_songs` VALUES ('9', '4', '1', 'sdsd', 'scorpions.mp3', 'Humanity', '9', '2016-06-11 03:20:02', '2016-06-11 03:20:09', '9');
INSERT INTO `artist_songs` VALUES ('10', '4', '1', 'sdsd', 'scorpions2.mp3', 'Lorelei', '10', '2016-06-09 03:21:07', '2016-06-11 03:21:12', '10');

-- ----------------------------
-- Table structure for artists
-- ----------------------------
DROP TABLE IF EXISTS `artists`;
CREATE TABLE `artists` (
  `artist_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `biography` text COLLATE utf8_unicode_ci NOT NULL,
  `dicography` text COLLATE utf8_unicode_ci NOT NULL,
  `tech_info` text COLLATE utf8_unicode_ci NOT NULL,
  `web_site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other_web_site` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`artist_id`),
  KEY `artists_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of artists
-- ----------------------------
INSERT INTO `artists` VALUES ('1', '1', '', '', '', 'dfsdf', '', 'dfsd', '2016-05-24 11:32:38', '2016-05-24 11:32:38', 'Coldplay');
INSERT INTO `artists` VALUES ('2', '2', '', '', '', '', '', 'dfsdfds', '2016-05-24 11:32:46', '2016-05-24 11:32:46', 'Dragons');
INSERT INTO `artists` VALUES ('3', '3', '', '', '', '', '', 'dsfsdf', '2016-05-24 11:33:03', '2016-05-24 11:33:03', 'AudioMachine');
INSERT INTO `artists` VALUES ('4', '4', '', '', '', '', '', 'Yerevan', '2016-06-10 00:15:38', '2016-06-11 00:15:46', 'Scorpions');
INSERT INTO `artists` VALUES ('5', '5', '', '', '', '', '', 'wwww', '2016-06-15 00:15:52', '2016-06-11 00:15:58', 'Alan Walker');

-- ----------------------------
-- Table structure for gig_artists
-- ----------------------------
DROP TABLE IF EXISTS `gig_artists`;
CREATE TABLE `gig_artists` (
  `gig_artist_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` int(11) NOT NULL,
  `gig_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gig_artist_id`),
  KEY `gig_artists_artist_id_index` (`artist_id`),
  KEY `gig_artists_gig_id_index` (`gig_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of gig_artists
-- ----------------------------
INSERT INTO `gig_artists` VALUES ('1', '1', '2', '2016-05-24 11:35:02', '2016-05-24 11:35:02');
INSERT INTO `gig_artists` VALUES ('2', '2', '1', '2016-05-24 11:35:02', '2016-05-24 11:35:02');
INSERT INTO `gig_artists` VALUES ('3', '3', '3', '2016-05-24 11:35:02', '2016-05-24 11:35:02');
INSERT INTO `gig_artists` VALUES ('4', '4', '1', '2016-05-24 12:02:45', '2016-05-24 12:02:45');
INSERT INTO `gig_artists` VALUES ('5', '5', '1', '2016-05-24 12:02:45', '2016-05-24 12:02:45');
INSERT INTO `gig_artists` VALUES ('6', '6', '2', '2016-05-24 12:02:45', '2016-05-24 12:02:45');

-- ----------------------------
-- Table structure for gigs
-- ----------------------------
DROP TABLE IF EXISTS `gigs`;
CREATE TABLE `gigs` (
  `gig_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `poster_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `start_time` datetime NOT NULL,
  `finish_time` datetime NOT NULL,
  `price` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`gig_id`),
  KEY `gigs_venue_id_index` (`venue_id`),
  KEY `gigs_poster_id_index` (`poster_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of gigs
-- ----------------------------
INSERT INTO `gigs` VALUES ('1', '1', '1', '', 'PUB', '2016-06-05', '2016-06-05 10:30:00', '2016-06-05 11:30:00', '100.00', '2016-05-24 11:35:02', '2016-05-24 11:35:02');
INSERT INTO `gigs` VALUES ('2', '2', '2', '', 'MAB', '2016-06-05', '2016-06-05 10:30:00', '2016-06-05 11:30:00', '100.00', '2016-05-24 12:02:45', '2016-05-24 12:02:45');
INSERT INTO `gigs` VALUES ('3', '3', '3', '', 'ZAB', '2016-06-09', '2016-06-07 15:01:47', '2016-06-21 15:01:49', '100.00', '2016-06-07 15:01:52', '2016-06-01 15:01:56');

-- ----------------------------
-- Table structure for posters
-- ----------------------------
DROP TABLE IF EXISTS `posters`;
CREATE TABLE `posters` (
  `poster_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gig_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`poster_id`),
  KEY `posters_gig_id_index` (`gig_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of posters
-- ----------------------------
INSERT INTO `posters` VALUES ('1', 'picture1.png', '1', '2016-05-24 11:35:02', '2016-05-24 11:35:02');
INSERT INTO `posters` VALUES ('2', 'picture2.jpg', '2', '2016-05-24 12:02:45', '2016-05-24 12:02:45');
INSERT INTO `posters` VALUES ('3', 'picture4.jpg', '3', '2016-06-10 19:28:57', '2016-06-03 19:29:09');
