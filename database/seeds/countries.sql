/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : tonightgigs

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2016-06-11 02:34:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `countries`
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `SortName` varchar(3) NOT NULL,
  `Name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO `countries` VALUES ('1', 'AF', 'Afghanistan');
INSERT INTO `countries` VALUES ('2', 'AL', 'Albania');
INSERT INTO `countries` VALUES ('3', 'DZ', 'Algeria');
INSERT INTO `countries` VALUES ('4', 'AS', 'American Samoa');
INSERT INTO `countries` VALUES ('5', 'AD', 'Andorra');
INSERT INTO `countries` VALUES ('6', 'AO', 'Angola');
INSERT INTO `countries` VALUES ('7', 'AI', 'Anguilla');
INSERT INTO `countries` VALUES ('8', 'AQ', 'Antarctica');
INSERT INTO `countries` VALUES ('9', 'AG', 'Antigua And Barbuda');
INSERT INTO `countries` VALUES ('10', 'AR', 'Argentina');
INSERT INTO `countries` VALUES ('11', 'AM', 'Armenia');
INSERT INTO `countries` VALUES ('12', 'AW', 'Aruba');
INSERT INTO `countries` VALUES ('13', 'AU', 'Australia');
INSERT INTO `countries` VALUES ('14', 'AT', 'Austria');
INSERT INTO `countries` VALUES ('15', 'AZ', 'Azerbaijan');
INSERT INTO `countries` VALUES ('16', 'BS', 'Bahamas The');
INSERT INTO `countries` VALUES ('17', 'BH', 'Bahrain');
INSERT INTO `countries` VALUES ('18', 'BD', 'Bangladesh');
INSERT INTO `countries` VALUES ('19', 'BB', 'Barbados');
INSERT INTO `countries` VALUES ('20', 'BY', 'Belarus');
INSERT INTO `countries` VALUES ('21', 'BE', 'Belgium');
INSERT INTO `countries` VALUES ('22', 'BZ', 'Belize');
INSERT INTO `countries` VALUES ('23', 'BJ', 'Benin');
INSERT INTO `countries` VALUES ('24', 'BM', 'Bermuda');
INSERT INTO `countries` VALUES ('25', 'BT', 'Bhutan');
INSERT INTO `countries` VALUES ('26', 'BO', 'Bolivia');
INSERT INTO `countries` VALUES ('27', 'BA', 'Bosnia and Herzegovina');
INSERT INTO `countries` VALUES ('28', 'BW', 'Botswana');
INSERT INTO `countries` VALUES ('29', 'BV', 'Bouvet Island');
INSERT INTO `countries` VALUES ('30', 'BR', 'Brazil');
INSERT INTO `countries` VALUES ('31', 'IO', 'British Indian Ocean Territory');
INSERT INTO `countries` VALUES ('32', 'BN', 'Brunei');
INSERT INTO `countries` VALUES ('33', 'BG', 'Bulgaria');
INSERT INTO `countries` VALUES ('34', 'BF', 'Burkina Faso');
INSERT INTO `countries` VALUES ('35', 'BI', 'Burundi');
INSERT INTO `countries` VALUES ('36', 'KH', 'Cambodia');
INSERT INTO `countries` VALUES ('37', 'CM', 'Cameroon');
INSERT INTO `countries` VALUES ('38', 'CA', 'Canada');
INSERT INTO `countries` VALUES ('39', 'CV', 'Cape Verde');
INSERT INTO `countries` VALUES ('40', 'KY', 'Cayman Islands');
INSERT INTO `countries` VALUES ('41', 'CF', 'Central African Republic');
INSERT INTO `countries` VALUES ('42', 'TD', 'Chad');
INSERT INTO `countries` VALUES ('43', 'CL', 'Chile');
INSERT INTO `countries` VALUES ('44', 'CN', 'China');
INSERT INTO `countries` VALUES ('45', 'CX', 'Christmas Island');
INSERT INTO `countries` VALUES ('46', 'CC', 'Cocos (Keeling) Islands');
INSERT INTO `countries` VALUES ('47', 'CO', 'Colombia');
INSERT INTO `countries` VALUES ('48', 'KM', 'Comoros');
INSERT INTO `countries` VALUES ('49', 'CG', 'Congo');
INSERT INTO `countries` VALUES ('50', 'CD', 'Congo The Democratic Republic Of The');
INSERT INTO `countries` VALUES ('51', 'CK', 'Cook Islands');
INSERT INTO `countries` VALUES ('52', 'CR', 'Costa Rica');
INSERT INTO `countries` VALUES ('53', 'CI', 'Cote D\'Ivoire (Ivory Coast)');
INSERT INTO `countries` VALUES ('54', 'HR', 'Croatia (Hrvatska)');
INSERT INTO `countries` VALUES ('55', 'CU', 'Cuba');
INSERT INTO `countries` VALUES ('56', 'CY', 'Cyprus');
INSERT INTO `countries` VALUES ('57', 'CZ', 'Czech Republic');
INSERT INTO `countries` VALUES ('58', 'DK', 'Denmark');
INSERT INTO `countries` VALUES ('59', 'DJ', 'Djibouti');
INSERT INTO `countries` VALUES ('60', 'DM', 'Dominica');
INSERT INTO `countries` VALUES ('61', 'DO', 'Dominican Republic');
INSERT INTO `countries` VALUES ('62', 'TP', 'East Timor');
INSERT INTO `countries` VALUES ('63', 'EC', 'Ecuador');
INSERT INTO `countries` VALUES ('64', 'EG', 'Egypt');
INSERT INTO `countries` VALUES ('65', 'SV', 'El Salvador');
INSERT INTO `countries` VALUES ('66', 'GQ', 'Equatorial Guinea');
INSERT INTO `countries` VALUES ('67', 'ER', 'Eritrea');
INSERT INTO `countries` VALUES ('68', 'EE', 'Estonia');
INSERT INTO `countries` VALUES ('69', 'ET', 'Ethiopia');
INSERT INTO `countries` VALUES ('70', 'XA', 'External Territories of Australia');
INSERT INTO `countries` VALUES ('71', 'FK', 'Falkland Islands');
INSERT INTO `countries` VALUES ('72', 'FO', 'Faroe Islands');
INSERT INTO `countries` VALUES ('73', 'FJ', 'Fiji Islands');
INSERT INTO `countries` VALUES ('74', 'FI', 'Finland');
INSERT INTO `countries` VALUES ('75', 'FR', 'France');
INSERT INTO `countries` VALUES ('76', 'GF', 'French Guiana');
INSERT INTO `countries` VALUES ('77', 'PF', 'French Polynesia');
INSERT INTO `countries` VALUES ('78', 'TF', 'French Southern Territories');
INSERT INTO `countries` VALUES ('79', 'GA', 'Gabon');
INSERT INTO `countries` VALUES ('80', 'GM', 'Gambia The');
INSERT INTO `countries` VALUES ('81', 'GE', 'Georgia');
INSERT INTO `countries` VALUES ('82', 'DE', 'Germany');
INSERT INTO `countries` VALUES ('83', 'GH', 'Ghana');
INSERT INTO `countries` VALUES ('84', 'GI', 'Gibraltar');
INSERT INTO `countries` VALUES ('85', 'GR', 'Greece');
INSERT INTO `countries` VALUES ('86', 'GL', 'Greenland');
INSERT INTO `countries` VALUES ('87', 'GD', 'Grenada');
INSERT INTO `countries` VALUES ('88', 'GP', 'Guadeloupe');
INSERT INTO `countries` VALUES ('89', 'GU', 'Guam');
INSERT INTO `countries` VALUES ('90', 'GT', 'Guatemala');
INSERT INTO `countries` VALUES ('91', 'XU', 'Guernsey and Alderney');
INSERT INTO `countries` VALUES ('92', 'GN', 'Guinea');
INSERT INTO `countries` VALUES ('93', 'GW', 'Guinea-Bissau');
INSERT INTO `countries` VALUES ('94', 'GY', 'Guyana');
INSERT INTO `countries` VALUES ('95', 'HT', 'Haiti');
INSERT INTO `countries` VALUES ('96', 'HM', 'Heard and McDonald Islands');
INSERT INTO `countries` VALUES ('97', 'HN', 'Honduras');
INSERT INTO `countries` VALUES ('98', 'HK', 'Hong Kong S.A.R.');
INSERT INTO `countries` VALUES ('99', 'HU', 'Hungary');
INSERT INTO `countries` VALUES ('100', 'IS', 'Iceland');
INSERT INTO `countries` VALUES ('101', 'IN', 'India');
INSERT INTO `countries` VALUES ('102', 'ID', 'Indonesia');
INSERT INTO `countries` VALUES ('103', 'IR', 'Iran');
INSERT INTO `countries` VALUES ('104', 'IQ', 'Iraq');
INSERT INTO `countries` VALUES ('105', 'IE', 'Ireland');
INSERT INTO `countries` VALUES ('106', 'IL', 'Israel');
INSERT INTO `countries` VALUES ('107', 'IT', 'Italy');
INSERT INTO `countries` VALUES ('108', 'JM', 'Jamaica');
INSERT INTO `countries` VALUES ('109', 'JP', 'Japan');
INSERT INTO `countries` VALUES ('110', 'XJ', 'Jersey');
INSERT INTO `countries` VALUES ('111', 'JO', 'Jordan');
INSERT INTO `countries` VALUES ('112', 'KZ', 'Kazakhstan');
INSERT INTO `countries` VALUES ('113', 'KE', 'Kenya');
INSERT INTO `countries` VALUES ('114', 'KI', 'Kiribati');
INSERT INTO `countries` VALUES ('115', 'KP', 'Korea North');
INSERT INTO `countries` VALUES ('116', 'KR', 'Korea South');
INSERT INTO `countries` VALUES ('117', 'KW', 'Kuwait');
INSERT INTO `countries` VALUES ('118', 'KG', 'Kyrgyzstan');
INSERT INTO `countries` VALUES ('119', 'LA', 'Laos');
INSERT INTO `countries` VALUES ('120', 'LV', 'Latvia');
INSERT INTO `countries` VALUES ('121', 'LB', 'Lebanon');
INSERT INTO `countries` VALUES ('122', 'LS', 'Lesotho');
INSERT INTO `countries` VALUES ('123', 'LR', 'Liberia');
INSERT INTO `countries` VALUES ('124', 'LY', 'Libya');
INSERT INTO `countries` VALUES ('125', 'LI', 'Liechtenstein');
INSERT INTO `countries` VALUES ('126', 'LT', 'Lithuania');
INSERT INTO `countries` VALUES ('127', 'LU', 'Luxembourg');
INSERT INTO `countries` VALUES ('128', 'MO', 'Macau S.A.R.');
INSERT INTO `countries` VALUES ('129', 'MK', 'Macedonia');
INSERT INTO `countries` VALUES ('130', 'MG', 'Madagascar');
INSERT INTO `countries` VALUES ('131', 'MW', 'Malawi');
INSERT INTO `countries` VALUES ('132', 'MY', 'Malaysia');
INSERT INTO `countries` VALUES ('133', 'MV', 'Maldives');
INSERT INTO `countries` VALUES ('134', 'ML', 'Mali');
INSERT INTO `countries` VALUES ('135', 'MT', 'Malta');
INSERT INTO `countries` VALUES ('136', 'XM', 'Man (Isle of)');
INSERT INTO `countries` VALUES ('137', 'MH', 'Marshall Islands');
INSERT INTO `countries` VALUES ('138', 'MQ', 'Martinique');
INSERT INTO `countries` VALUES ('139', 'MR', 'Mauritania');
INSERT INTO `countries` VALUES ('140', 'MU', 'Mauritius');
INSERT INTO `countries` VALUES ('141', 'YT', 'Mayotte');
INSERT INTO `countries` VALUES ('142', 'MX', 'Mexico');
INSERT INTO `countries` VALUES ('143', 'FM', 'Micronesia');
INSERT INTO `countries` VALUES ('144', 'MD', 'Moldova');
INSERT INTO `countries` VALUES ('145', 'MC', 'Monaco');
INSERT INTO `countries` VALUES ('146', 'MN', 'Mongolia');
INSERT INTO `countries` VALUES ('147', 'MS', 'Montserrat');
INSERT INTO `countries` VALUES ('148', 'MA', 'Morocco');
INSERT INTO `countries` VALUES ('149', 'MZ', 'Mozambique');
INSERT INTO `countries` VALUES ('150', 'MM', 'Myanmar');
INSERT INTO `countries` VALUES ('151', 'NA', 'Namibia');
INSERT INTO `countries` VALUES ('152', 'NR', 'Nauru');
INSERT INTO `countries` VALUES ('153', 'NP', 'Nepal');
INSERT INTO `countries` VALUES ('154', 'AN', 'Netherlands Antilles');
INSERT INTO `countries` VALUES ('155', 'NL', 'Netherlands The');
INSERT INTO `countries` VALUES ('156', 'NC', 'New Caledonia');
INSERT INTO `countries` VALUES ('157', 'NZ', 'New Zealand');
INSERT INTO `countries` VALUES ('158', 'NI', 'Nicaragua');
INSERT INTO `countries` VALUES ('159', 'NE', 'Niger');
INSERT INTO `countries` VALUES ('160', 'NG', 'Nigeria');
INSERT INTO `countries` VALUES ('161', 'NU', 'Niue');
INSERT INTO `countries` VALUES ('162', 'NF', 'Norfolk Island');
INSERT INTO `countries` VALUES ('163', 'MP', 'Northern Mariana Islands');
INSERT INTO `countries` VALUES ('164', 'NO', 'Norway');
INSERT INTO `countries` VALUES ('165', 'OM', 'Oman');
INSERT INTO `countries` VALUES ('166', 'PK', 'Pakistan');
INSERT INTO `countries` VALUES ('167', 'PW', 'Palau');
INSERT INTO `countries` VALUES ('168', 'PS', 'Palestinian Territory Occupied');
INSERT INTO `countries` VALUES ('169', 'PA', 'Panama');
INSERT INTO `countries` VALUES ('170', 'PG', 'Papua new Guinea');
INSERT INTO `countries` VALUES ('171', 'PY', 'Paraguay');
INSERT INTO `countries` VALUES ('172', 'PE', 'Peru');
INSERT INTO `countries` VALUES ('173', 'PH', 'Philippines');
INSERT INTO `countries` VALUES ('174', 'PN', 'Pitcairn Island');
INSERT INTO `countries` VALUES ('175', 'PL', 'Poland');
INSERT INTO `countries` VALUES ('176', 'PT', 'Portugal');
INSERT INTO `countries` VALUES ('177', 'PR', 'Puerto Rico');
INSERT INTO `countries` VALUES ('178', 'QA', 'Qatar');
INSERT INTO `countries` VALUES ('179', 'RE', 'Reunion');
INSERT INTO `countries` VALUES ('180', 'RO', 'Romania');
INSERT INTO `countries` VALUES ('181', 'RU', 'Russia');
INSERT INTO `countries` VALUES ('182', 'RW', 'Rwanda');
INSERT INTO `countries` VALUES ('183', 'SH', 'Saint Helena');
INSERT INTO `countries` VALUES ('184', 'KN', 'Saint Kitts And Nevis');
INSERT INTO `countries` VALUES ('185', 'LC', 'Saint Lucia');
INSERT INTO `countries` VALUES ('186', 'PM', 'Saint Pierre and Miquelon');
INSERT INTO `countries` VALUES ('187', 'VC', 'Saint Vincent And The Grenadines');
INSERT INTO `countries` VALUES ('188', 'WS', 'Samoa');
INSERT INTO `countries` VALUES ('189', 'SM', 'San Marino');
INSERT INTO `countries` VALUES ('190', 'ST', 'Sao Tome and Principe');
INSERT INTO `countries` VALUES ('191', 'SA', 'Saudi Arabia');
INSERT INTO `countries` VALUES ('192', 'SN', 'Senegal');
INSERT INTO `countries` VALUES ('193', 'RS', 'Serbia');
INSERT INTO `countries` VALUES ('194', 'SC', 'Seychelles');
INSERT INTO `countries` VALUES ('195', 'SL', 'Sierra Leone');
INSERT INTO `countries` VALUES ('196', 'SG', 'Singapore');
INSERT INTO `countries` VALUES ('197', 'SK', 'Slovakia');
INSERT INTO `countries` VALUES ('198', 'SI', 'Slovenia');
INSERT INTO `countries` VALUES ('199', 'XG', 'Smaller Territories of the UK');
INSERT INTO `countries` VALUES ('200', 'SB', 'Solomon Islands');
INSERT INTO `countries` VALUES ('201', 'SO', 'Somalia');
INSERT INTO `countries` VALUES ('202', 'ZA', 'South Africa');
INSERT INTO `countries` VALUES ('203', 'GS', 'South Georgia');
INSERT INTO `countries` VALUES ('204', 'SS', 'South Sudan');
INSERT INTO `countries` VALUES ('205', 'ES', 'Spain');
INSERT INTO `countries` VALUES ('206', 'LK', 'Sri Lanka');
INSERT INTO `countries` VALUES ('207', 'SD', 'Sudan');
INSERT INTO `countries` VALUES ('208', 'SR', 'Suriname');
INSERT INTO `countries` VALUES ('209', 'SJ', 'Svalbard And Jan Mayen Islands');
INSERT INTO `countries` VALUES ('210', 'SZ', 'Swaziland');
INSERT INTO `countries` VALUES ('211', 'SE', 'Sweden');
INSERT INTO `countries` VALUES ('212', 'CH', 'Switzerland');
INSERT INTO `countries` VALUES ('213', 'SY', 'Syria');
INSERT INTO `countries` VALUES ('214', 'TW', 'Taiwan');
INSERT INTO `countries` VALUES ('215', 'TJ', 'Tajikistan');
INSERT INTO `countries` VALUES ('216', 'TZ', 'Tanzania');
INSERT INTO `countries` VALUES ('217', 'TH', 'Thailand');
INSERT INTO `countries` VALUES ('218', 'TG', 'Togo');
INSERT INTO `countries` VALUES ('219', 'TK', 'Tokelau');
INSERT INTO `countries` VALUES ('220', 'TO', 'Tonga');
INSERT INTO `countries` VALUES ('221', 'TT', 'Trinidad And Tobago');
INSERT INTO `countries` VALUES ('222', 'TN', 'Tunisia');
INSERT INTO `countries` VALUES ('223', 'TR', 'Turkey');
INSERT INTO `countries` VALUES ('224', 'TM', 'Turkmenistan');
INSERT INTO `countries` VALUES ('225', 'TC', 'Turks And Caicos Islands');
INSERT INTO `countries` VALUES ('226', 'TV', 'Tuvalu');
INSERT INTO `countries` VALUES ('227', 'UG', 'Uganda');
INSERT INTO `countries` VALUES ('228', 'UA', 'Ukraine');
INSERT INTO `countries` VALUES ('229', 'AE', 'United Arab Emirates');
INSERT INTO `countries` VALUES ('230', 'GB', 'United Kingdom');
INSERT INTO `countries` VALUES ('231', 'US', 'United States');
INSERT INTO `countries` VALUES ('232', 'UM', 'United States Minor Outlying Islands');
INSERT INTO `countries` VALUES ('233', 'UY', 'Uruguay');
INSERT INTO `countries` VALUES ('234', 'UZ', 'Uzbekistan');
INSERT INTO `countries` VALUES ('235', 'VU', 'Vanuatu');
INSERT INTO `countries` VALUES ('236', 'VA', 'Vatican City State (Holy See)');
INSERT INTO `countries` VALUES ('237', 'VE', 'Venezuela');
INSERT INTO `countries` VALUES ('238', 'VN', 'Vietnam');
INSERT INTO `countries` VALUES ('239', 'VG', 'Virgin Islands (British)');
INSERT INTO `countries` VALUES ('240', 'VI', 'Virgin Islands (US)');
INSERT INTO `countries` VALUES ('241', 'WF', 'Wallis And Futuna Islands');
INSERT INTO `countries` VALUES ('242', 'EH', 'Western Sahara');
INSERT INTO `countries` VALUES ('243', 'YE', 'Yemen');
INSERT INTO `countries` VALUES ('244', 'YU', 'Yugoslavia');
INSERT INTO `countries` VALUES ('245', 'ZM', 'Zambia');
INSERT INTO `countries` VALUES ('246', 'ZW', 'Zimbabwe');
