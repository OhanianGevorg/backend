<?php

use App\Models\Artist\ArtistDataModel;
use App\Models\Artists\ArtistSongModel;
use App\Models\Genre\GenreModel;
use App\Models\Gig\GigModel;
use App\Models\Users\ArtistModel;
use App\Models\Users\VenueModel;
use App\Models\Venue\VenueDataModel;
use App\Models\GigTypes;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
//        $this->call(VenuesSeeder::class);
//        $this->call(ArtistSeeder::class);
//        $this->call(GigSeeder::class);
//        $this->call(GenreSeeder::class);
//        $this->call(GigTypeSeeder::class);
    }
}

class UserSeeder extends Seeder{
    public function run()
    {
        $iter =0;
        factory(\App\Models\User::class,10)->create()->each(function ($user) use($iter){
            $user->email = 'asd'.$iter.'@gmail.com';
            $iter++;
        });
    }
}
//class VenuesSeeder extends Seeder
//{
//    /**
//     * Run the database seeds.
//     * @return void
//     */
//    public function run()
//    {
//        VenueModel::getQuery()->delete();
//        VenueDataModel::truncate();
//        factory(VenueModel::class, 5)->create()->each(function ($venue) {
//            $venue->data()->save(factory(VenueDataModel::class)->create());
//        });
//    }
//}
//
//class ArtistSeeder extends Seeder
//{
//    /**
//     * Run the database seeds.
//     * @return void
//     */
//    public function run()
//    {
//        ArtistModel::getQuery()->delete();
//        ArtistDataModel::truncate();
//        factory(ArtistModel::class, 5)->create()->each(function ($venue) {
//            $venue->data()->save(factory(ArtistDataModel::class)->create());
//        });
//    }
//}
//
//class GigSeeder extends Seeder
//{
//    public function run()
//    {
//        GigModel::truncate();
//
//        $venues = VenueModel::all();
//        $artists = ArtistModel::all();
//
//        factory(GigModel::class, 5)->create()->each(function ($gig) use ($venues, $artists) {
//            $gig->venue_id = $venues->random()->getKey();
//            $gig->save();
//
//            $artists->random(3)->each(function ($artist) use ($gig) {
//                \App\Models\Gig\GigArtistModel::create([
//                    'gig_id' => $gig->getKey(),
//                    'artist_id' => $artist->getKey(),
//                ]);
//            });
//        });
//    }
//}
//
//class GenreSeeder extends Seeder
//{
//    public function run()
//    {
//        GenreModel::truncate();
//
//        GenreModel::insert([
//            ['name' => 'rock'],
//            ['name' => 'pop'],
//            ['name' => 'folk'],
//            ['name' => 'jazz'],
//            ['name' => 'country'],
//            ['name' => 'metal'],
//            ['name' => 'classic'],
//        ]);
//    }
//}
//
//class GigTypeSeeder extends Seeder
//{
//    public function run()
//    {
//        GigTypes::truncate();
//
//        GigTypes::insert([
//            ['name' => 'Originals'],
//            ['name' => 'Covers'],
//            ['name' => 'Open Mics'],
//            ['name' => 'Album Launches'],
//            ['name' => 'Fundraisers'],
//            ['name' => 'Festivals'],
//            ['name' => 'Comedy'],
//            ['name' => 'Special Events'],
//        ]);
//    }
//}