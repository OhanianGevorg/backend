<?php
//$factory->define(App\Models\Users\VenueModel::class, function (Faker\Generator $faker) {
//    return [
//        'email' => $faker->safeEmail,
//        'role_id' => \App\Models\Users\UserModel::VENUE,
//        'password' => bcrypt(str_random(10)),
//        'remember_token' => str_random(10),
//        'site_name' => $faker->unique()->word
//    ];
//});
//
//$factory->define(App\Models\Venue\VenueDataModel::class, function (Faker\Generator $faker) {
//    return [
//        'name' => $faker->company,
//        'about' => str_random(100),
//        'web_site' => str_random(100),
//        'latitude' => $faker->randomFloat(),
//        'longitude' => $faker->randomFloat(),
//        'capacity' => $faker->randomNumber(),
//    ];
//});
//
//$factory->define(App\Models\Users\ArtistModel::class, function (Faker\Generator $faker) {
//    return [
//        'email' => $faker->safeEmail,
//        'role_id' => \App\Models\Users\UserModel::ARTIST,
//        'password' => bcrypt(str_random(10)),
//        'remember_token' => str_random(10),
//        'site_name' => $faker->unique()->word
//    ];
//});
//
//$factory->define(App\Models\Artist\ArtistDataModel::class, function (Faker\Generator $faker) {
//    return [
//        'name' => $faker->name
//    ];
//});
//
//$factory->define(App\Models\Gig\GigModel::class, function (Faker\Generator $faker) {
//    $startTime = $faker->dateTime;
//
//    return [
//        'price' => $faker->randomNumber(),
//        'description' => str_random(100),
//        'name' => $faker->word,
//        'ticket_seller_site' => $faker->url,
//        'start' => $startTime,
//        'end' => $startTime->add(new DateInterval('PT1H')),
//    ];
//});
//
//$factory->define(App\Models\GigTypes::class, function (Faker\Generator $faker) {
//    return [
//        'name' => $faker->name
//    ];
//});
//
//$factory->define(App\Models\Artists\ArtistSongModel::class, function (Faker\Generator $faker) {
//    return [
//        'file_id' => 'alanwalker.mp3',
//        'file_name' => $faker->word,
//        'song_name' => $faker->word
//    ];
//});
$factory->define(App\Models\User::class,function (Faker\Generator $faker){
    return [
        'role_id'=>1,
        'email'=>'asd@gmail.com',
        'password'=>bcrypt('asdASD123'),
        'name'=>'userName',
        'surname'=>'userSurname'
    ];
});