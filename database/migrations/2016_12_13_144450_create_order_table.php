<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('order',function (Blueprint $table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_number');
            $table->integer('user_id');
            $table->integer('item_id');
            $table->integer('item_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('order');

    }
}
