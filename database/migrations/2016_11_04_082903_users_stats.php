<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersStats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('users_stats',function (Blueprint $table){

                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('owner_id');
                $table->string('type')->nullable(false);
                $table->string('ip');
                $table->timestamps();
                $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('users_stats');
    }
}
