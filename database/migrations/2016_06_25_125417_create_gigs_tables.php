<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGigsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
//            $table->integer('user_id')->index();
            $table->integer('store_user_id')->index();
            $table->string('poster')->index();
            $table->double('price')->nullable();
            $table->string('name')->nullable();
            $table->string('description')->nullable();
            $table->string('ticket_seller_site')->nullable();
//            $table->string('ages')->nullable();
//            $table->dateTime('start');
//            $table->dateTime('end')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_system_types', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('products_types', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->index();
            $table->integer('product_system_type_id')->index();
            $table->timestamps();
        });

        Schema::create('sold_products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->index();
            $table->integer('user_id')->index();
            $table->timestamp('stage_time');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_wall', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id');
            $table->integer('sender_user_id');
            $table->string('message');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('product_system_types');
        Schema::dropIfExists('products_types');
        Schema::dropIfExists('sold_products');
        Schema::dropIfExists('product_wall');
    }
}
