<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoriteTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_favorite_products', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->index();
            $table->integer('user_id')->index();
            $table->timestamps();
        });

        Schema::create('user_favorite_users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->enum('entity_type', ['user', 'store']);
            $table->integer('entity_user_id')->index();
            $table->integer('user_id')->index();
            $table->timestamps();
        });

        Schema::create('user_favorite_songs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('song_id')->index();
            $table->integer('user_id')->index();
            $table->timestamps();
        });

        Schema::create('user_favorite_videos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('video_id')->index();
            $table->integer('user_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_favorite_products');
        Schema::dropIfExists('user_favorite_users');
        Schema::dropIfExists('user_favorite_songs');
        Schema::dropIfExists('user_favorite_videos');
    }
}
