<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSongsPermissionEnum extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_songs', function (Blueprint $table) {
            //
            $table->enum('permission',['deny','allow']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('user_songs', function (Blueprint $table) {
            //
            $table->dropColumn('permission');
        });
    }
}
