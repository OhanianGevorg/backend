<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('role_id')->index();
            $table->string('email')->unique();
            $table->string('password');
            $table->string('api_token', 60)->nullable();
            $table->string('facebook_id')->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('store_name')->nullable();
            $table->string('address')->nullable();
            $table->string('country')->nullabl();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->string('contact_phone')->nullable();
            $table->string('contact_mobile')->nullable();
//            $table->string('contact_email')->nullable();
            $table->string('web_site')->nullable();
//            $table->string('other_web_site')->nullable();
            $table->text('about')->nullable();
//            $table->text('hear_about')->nullable();
            $table->string('avatar')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('store_types', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('store_id')->index();
            $table->integer('type_id')->index();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('user_pictures', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->enum('type', [1, 2]);
            $table->string('file_id');
//            $table->string('caption')->nullable();
//            $table->string('photo_credit')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('user_follows', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('follow_user_id')->index();
            $table->integer('follower_user_id')->index();
            $table->timestamps();

        });

        Schema::create('user_reviews', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->text('review');
            $table->string('reviewed_by')->nullable();
            $table->date('review_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('user_songs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->string('file_id');
            $table->string('file_name')->nullable();
            $table->string('song_name')->nullable();
            $table->string('description')->nullable();
            $table->string('sort_index')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('user_videos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->string('title')->nullable();
            $table->string('caption')->nullable();
            $table->string('url');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('user_wall', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('sender_user_id');
            $table->string('message');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('user_search_saves', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->enum('type', [1, 2, 3]);
            $table->string('name');
            $table->string('date');
            $table->string('first_initial');
            $table->string('types');
            $table->string('location_city');
            $table->string('location_radius');
            $table->string('location_latitude');
            $table->string('location_longitude');
            $table->integer('min_price');
            $table->integer('max_price');
            $table->string('genres');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
        Schema::dropIfExists('store_types');
        Schema::dropIfExists('user_pictures');
        Schema::dropIfExists('user_reviews');
        Schema::dropIfExists('user_wall');
        Schema::dropIfExists('user_videos');
        Schema::dropIfExists('user_songs');
    }
}
